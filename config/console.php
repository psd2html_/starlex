<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php'));

$config = [
    'id'                  => 'basic-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'app\commands',
    'language'  => 'ru',

    'aliases'    => [
        '@uploads' => 'uploads/',
    ],

    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'keyPrefix' => 'console_',
        ],
        'log'   => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'    => require(__DIR__ . '/db-local.php'),

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
    ],
    'params'     => $params,


    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
        'migrate' => [
            'class'          => 'yii\console\controllers\MigrateController',
            'migrationPath'  => '@app/migrations',
            'migrationTable' => '{{%migration}}',
            'templateFile'   => '@app/migrations/views/migration.php',
        ],
    ],

];

if (YII_ENV_DEV){
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
