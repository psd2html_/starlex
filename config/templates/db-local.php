<?php

return [
    'class'    => 'yii\db\Connection',
    'dsn'      => 'mysql:host=localhost;dbname=starlex_db',
    'username' => 'root',
    'password' => '',
    'charset'  => 'utf8',
];