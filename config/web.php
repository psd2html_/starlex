<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php'));

Yii::setAlias('@web', realpath(dirname(__FILE__) . '/../../web/'));

$config = [
    'id'        => 'basic',
    'name'      => 'Starlex',
    'basePath'  => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language'  => 'ru',
    'homeUrl'   => '/',

    'aliases' => [
        '@uploads' => 'uploads/',
    ],

    'components'      => [
        'request'      => [
            // for API
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'cache'        => [
            'class'     => 'yii\caching\FileCache',
            'keyPrefix' => 'web_',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer'       => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => YII_ENV_DEV,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'      => 'yii\log\FileTarget',
                    'logFile'    => '@app/runtime/logs/eauth.log',
                    'categories' => ['nodge\eauth\*'],
//                    'logVars'    => [],
                ],
            ],
        ],
        'db'           => require(__DIR__ . '/db-local.php'),
        'user'         => [
            'identityClass'   => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl'        => ['user/sign-in/login'],
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            //'enableStrictParsing' => true,
            'rules'           => require(__DIR__ . '/rules.php'),
        ],
        'assetManager' => [
            'bundles'         => [
                'yii\bootstrap\BootstrapAsset' => [//'css' => [],
                ],
            ],
            'appendTimestamp' => YII_DEBUG,
        ],
        'authManager'  => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'eauth'        => require('eauth.php'),
        'i18n'         => [
            'translations' => [
                'eauth' => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
                'app'   => [
                    'sourceLanguage' => 'ru',
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@app/messages',
                    'fileMap'        => [
                        'app' => 'app.php',
                    ],
//					'on missingTranslation' => ['\backend\modules\i18n\Module', 'missingTranslation'],
                ],
            ],
        ],

        'authClientCollection' => [
            'class'   => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class'        => 'yii\authclient\clients\VKontakte',
                    'scope'        => ['email'],

                    // DEV-Artwebit
                    'clientId'     => '6121750',
                    'clientSecret' => '1DsLh3xgyXoiqxzBMDxc',

                ],
                'google'    => [
                    'class'        => 'yii\authclient\clients\Google',
                    'scope'        => ['email'],

                    // DEV-Artwebit
                    'clientId'     => '643412400890-bo67pjuojhs94ebjacfq53i0b759hmki.apps.googleusercontent.com',
                    'clientSecret' => 'JMMRa-xCQnLueqzMQgA7nVXE',
                ],

                'facebook' => [
                    'class'        => 'yii\authclient\clients\Facebook',
                    'scope'        => ['email'],

                    // DEV-Artwebit
                    'clientId'     => '1377028569077599',
                    'clientSecret' => '9e9e0ae402ca8ac8746d185d82f197f6',
                ],
            ],
        ],
    ],
    'modules'         => [
        'api'  => [
            'class' => 'app\modules\api\Module',
        ],
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
    ],
    'as globalAccess' => [
        'class' => 'app\behaviors\GlobalAccessBehavior',
        'rules' => [
            [
                'controllers' => ['user/sign-in'],
                'allow'       => true,
                'roles'       => ['?'],
            ],
            [
                'controllers' => ['user/sign-in'],
                'allow'       => true,
                'roles'       => ['@'],
                'actions'     => ['logout'],
            ],
            [
                'controllers' => ['user/sign-in'],
                'allow'       => false,
                'roles'       => ['@'],
            ],
            [
                'controllers' => ['api/default'],
                'allow'       => false,
                'roles'       => ['?'],
            ],
            [
                'controllers' => ['api/*'],
                'allow'       => true,
                'roles'       => ['?'],
            ],
            [
                'allow' => true,
                'roles' => ['@'],
            ],
            [
                'allow' => false,
                'roles' => ['?'],
            ],
        ],
    ],
    'params'          => $params,
];

if (YII_DEBUG){
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

if (YII_ENV_DEV){
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
        'generators' => [
            'crud' => [
                'class'     => 'app\extension\gii\crud\Generator',
                'templates' => [
                    'Starlex templates' => '@app/extension/gii/crud/default',
                ],
            ],
        ],
    ];
}

return $config;
