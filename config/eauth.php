<?php


return [
    'class'        => 'nodge\eauth\EAuth',
    'popup'        => true,
    // Use the popup window instead of redirecting.
    'cache'        => false,
    // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
    'cacheExpire'  => 0,
    // Cache lifetime. Defaults to 0 - means unlimited.
    'httpClient'   => [
        // uncomment this to use streams in safe_mode
        //'useStreamsFallback' => true,
    ],

    // You can save access_token to your database by using custom token storage in your config:
//    'tokenStorage' => [
//        'class' => '@app\eauth\DatabaseTokenStorage',
//    ],



    'services' => [ // You can change the providers and their classes.
//        'google' => array(
//            'class' => 'nodge\eauth\services\GoogleOpenIDService',
//            //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
//        ),
//        'yandex' => array(
//            'class' => 'nodge\eauth\services\YandexOpenIDService',
//            //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
//        ),
//        'twitter' => array(
//            // register your app here: https://dev.twitter.com/apps/new
//            'class' => 'nodge\eauth\services\TwitterOAuth1Service',
//            'key' => '...',
//            'secret' => '...',
//        ),
        'google' => [
            // register your app here: https://code.google.com/apis/console/
            'class' => 'app\components\eauth\services\extended\GoogleOAuth2Service',
            'title' => 'Google (OAuth)',

            // LIVE
            //'clientId' => '671213306528-4d0cmsapdrv7b66gan175n906nkd9avq.apps.googleusercontent.com',
            //'clientSecret' => 'qwzr1JVzobBsWs_u9I0JjOq2',

            // DEV Artwebit
            //'clientId' => '643412400890-bo67pjuojhs94ebjacfq53i0b759hmki.apps.googleusercontent.com',
            //'clientSecret' => 'JMMRa-xCQnLueqzMQgA7nVXE',

            'clientId' => '755789287248-6aqha1tjh1mrai99apj52hueba4jom5r.apps.googleusercontent.com',
            //'clientSecret' => 'JMMRa-xCQnLueqzMQgA7nVXE',

        ],
//        'yandex_oauth' => array(
//            // register your app here: https://oauth.yandex.ru/client/my
//            'class' => 'nodge\eauth\services\YandexOAuth2Service',
//            'clientId' => '...',
//            'clientSecret' => '...',
//            'title' => 'Yandex (OAuth)',
//        ),
    /*
     https://www.facebook.com/
логин:  starlexacc@gmail.com
пароль:  Starlex123456
     */
        'facebook'     => [
            // register your app here: https://developers.facebook.com/apps/
            'class' => 'app\components\eauth\services\extended\FacebookOAuth2Service',

            // LIVE
            //'clientId' => '271450073335924',
            //'clientSecret' => '565f1bcbb4b67f1a2a04e65847f781ec',

            // DEV - Artwebit
            //'clientId' => '1986949234906764',
            //'clientSecret' => 'c59a974d19f973c985da5d4be8381605',

            // новые
            'clientId' => '1818934464816463',
            'clientSecret' => '9f4331be2c08efefb577a2595de4fba8',
        ],
//        'yahoo' => array(
//            'class' => 'nodge\eauth\services\YahooOpenIDService',
//            //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
//        ),
//        'linkedin' => array(
//            // register your app here: https://www.linkedin.com/secure/developer
//            'class' => 'nodge\eauth\services\LinkedinOAuth1Service',
//            'key' => '...',
//            'secret' => '...',
//            'title' => 'LinkedIn (OAuth1)',
//        ),
//        'linkedin_oauth2' => array(
//            // register your app here: https://www.linkedin.com/secure/developer
//            'class' => 'nodge\eauth\services\LinkedinOAuth2Service',
//            'clientId' => '...',
//            'clientSecret' => '...',
//            'title' => 'LinkedIn (OAuth2)',
//        ),
//        'github' => array(
//            // register your app here: https://github.com/settings/applications
//            'class' => 'nodge\eauth\services\GitHubOAuth2Service',
//            'clientId' => '...',
//            'clientSecret' => '...',
//        ),
//        'live' => array(
//            // register your app here: https://account.live.com/developers/applications/index
//            'class' => 'nodge\eauth\services\LiveOAuth2Service',
//            'clientId' => '...',
//            'clientSecret' => '...',
//        ),
//        'steam' => array(
//            'class' => 'nodge\eauth\services\SteamOpenIDService',
//            //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
//        ),
        'vkontakte' => [
            // register your app here: https://vk.com/editapp?act=create&site=1
            'class' => 'app\components\eauth\services\extended\VKontakteOAuth2Service',

            // DEV - Artwebit
            //'clientId' => '6121750',
            //'clientSecret' => '1DsLh3xgyXoiqxzBMDxc',

            // LIVE - Amazon
            'clientId' => '6157594',
            'clientSecret' => 'gvWNvEef3LDrx2StV2LA',
        ],


//        'mailru' => array(
//            // register your app here: http://api.mail.ru/sites/my/add
//            'class' => 'nodge\eauth\services\MailruOAuth2Service',
//            'clientId' => '...',
//            'clientSecret' => '...',
//        ),
//        'odnoklassniki' => array(
//            // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
//            // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
//            'class' => 'nodge\eauth\services\OdnoklassnikiOAuth2Service',
//            'clientId' => '1252680192',
//            'clientSecret' => '626B55F547C739C8CEE68F73',
//            'clientPublic' => 'CBADGLKLEBABABABA',
//            'title' => 'Odnoklas.',
//        ),
    ],
];