<?php

return [
    // Admin
    '<_a:(login|logout|signup|email-confirm|request-password-reset|password-reset)>' => 'user/sign-in/<_a>',

    // API
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/user',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/game',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/tariff',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/order',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/auth',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/theme',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/theme-language',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/language',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/lesson',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/lesson-language',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/lesson-word',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/lesson-words-language',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/lesson-image',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/lesson-media',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/completed-user-lesson',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/word',
        'pluralize'  => false,
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'api/v1/word-translate',
        'pluralize'  => false,
    ],

    //'login/<service:google|facebook|etc>' => 'site/login',

    // EXAMPLE
////    [
////        'pattern'   => '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>',
////        'route'     => '<module>/<controller>/<action>',
////    ],
////    [
////        'pattern'   => '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>',
////        'route'     => '<module>/<controller>/<action>',
////    ],
////    [
////        'pattern'   => '<controller:\w+>/<action:[\w-]+>/<id:\d+>',
////        'route'     => '<controller>/<action>',
////    ],
////    [
////        'pattern'   => '<controller:\w+>/<action:[\w-]+>',
////        'route'     => '<controller>/<action>',
////    ],
////    [
////        'pattern'   => '<controller:\w+>/<id:\d+>',
////        'route'     => '<controller>/view',
////    ],
];