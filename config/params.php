<?php

return [
    'adminEmail'                    => 'admin@gmail.com',
    'supportEmail'                  => 'support@gmail.com',
    'user.passwordResetTokenExpire' => 3600, // время «жизни» токена сброса пароля

    // Imagine ext.
    'thumbnails' => [
        'width' => 100,
        'height' => 100
    ],
    'social' => [
        'facebook' => [
            //'clientId'     => '1896343330659634',
            //'clientSecret' => '3d78c42ae7c063f9854f8e8f185ea25b',
            'clientId'     => '1818934464816463',
            'clientSecret' => '9f4331be2c08efefb577a2595de4fba8',
        ],
        'google' => [
            'clientId'     =>  '755789287248-6aqha1tjh1mrai99apj52hueba4jom5r.apps.googleusercontent.com'
            //'clientId'     => '1090756755635-t2l0r9640cvvrfr3mnsf987uci399kvj.apps.googleusercontent.com',
            //'clientSecret' => '',
        ],
        'vk' => [
            //'clientId'     => '6663315',
            //'clientSecret' => 'QsozHlNNR8MWaGMzJmph',
            'clientId'     => '6489872',
            'clientSecret' => '6wgoKPuixdCY9vqcQjfR',
        ],
    ]
];