<?php
/**
 * Created by PhpStorm.
 * User: jintropin
 * Date: 16.08.2018
 * Time: 12:05
 */
namespace app\components;

use Yii;
use VK\Client\VKApiClient;

class SocialSignup {

    public $errors = [];
    public $profile = [];
    public $attributes;

    public function getProfile($provider, $accessToken) {
        switch ($provider) {
            case "facebook":
                $client_id = Yii::$app->params['social']['facebook']['clientId'];

                $client_secret = Yii::$app->params['social']['facebook']['clientSecret'];

                $fb = new \Facebook\Facebook([
                    'app_id' => $client_id,
                    'app_secret' => $client_secret,
                    'default_graph_version' => 'v3.1',
                ]);

                $request = $fb->request(
                   'GET',
                   '/me?fields=name,link,email'
                );

                $request->setAccessToken($accessToken);

                try {
                   $response = $fb->getClient()->sendRequest($request);
                } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                   // When Graph returns an error
                   //echo 'Graph returned an error: ' . $e->getMessage();
                   //exit;
                } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                   // When validation fails or other local issues
                   //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                   //exit;
                }

                $info = $response->getGraphNode();

                $this->profile['id'] = $info['id'];
                $this->profile['name'] = $info['name'];
                $this->profile['email'] = $info['email'];

                $this->attributes['client_id']      = $info['id'];
                $this->attributes['username']       = $info['name'];
                $this->attributes['email']    = $info['email'];

                break;
            case "google":
                $client_id = Yii::$app->params['social']['google']['clientId'];

                $client = new \Google_Client([
                    'client_id' => $client_id
                ]);

                $client->setAccessToken($accessToken);

                $oauth2 = new \Google_Service_Oauth2($client);

                $info = $oauth2->userinfo->get();

                $this->profile['id'] = $info['id'];
                $this->profile['name'] = $info['name'];
                $this->profile['email'] = $info['email'];

                $this->attributes['id'] = $info['id'];
                $this->attributes['name'] = $info['name'];
                $this->attributes['email'] = $info['email'];
                if (!empty($info['link'])) {
                    $this->attributes['url'] = $info['link'];
                }
                if (!empty($info['gender'])) {
                    $this->attributes['gender'] = $info['gender'] == 'male' ? 'M' : 'F';
                }
                if (!empty($info['picture'])) {
                    $this->attributes['photo'] = $info['picture'];
                }
                $this->attributes['birthday']       = isset($info['birthday']) ? ($info['birthday']) : '';
                $this->attributes['client_id']      = isset($info['id']) ? ($info['id']) : '';
                $this->attributes['username']       = isset($info['name']) ? ($info['name']) : '';
                $this->attributes['social_page']    = isset($info['link']) ? ($info['link']) : '';
                $this->attributes['avatar']         = isset($info['picture']) ? ($info['picture']) : '';
                $this->attributes['sex']            = $info['gender'] == 'male' ? 1 : 2; // 1-мужской; 2-женский

                break;
            case "vk":
                $client_id = Yii::$app->params['social']['vk']['clientId'];

                $client_secret = Yii::$app->params['social']['vk']['clientSecret'];

                $vk = new VKApiClient();

                $response = $vk->users()->get($accessToken, array(
                    'fields'    => array('photo','sex', 'nickname'),
                ));

                $info = $response[0];

                $this->profile['id'] = $info['id'];
                $this->profile['name'] = $info['first_name'] . ' ' . $info['last_name'];

                $this->attributes['client_id'] = $info['id'];
                $this->attributes['username'] = $info['first_name'] . ' ' . $info['last_name'];
                $this->attributes['social_page'] = 'http://vk.com/id' . $info['id'];
                $this->attributes['avatar']         = isset($info['photo']) ? ($info['photo']) : '';
                if (!empty($info['nickname'])) {
                    $this->attributes['username'] = $info['nickname'];
                }
                $this->attributes['gender'] = $info['sex'] == 1 ? 'F' : 'M';

                break;
            default:
                $this->errors[] = ["field"=>"provider","message"=>"провайдер задан не верно"];
        }
    }

}