<?php
/**
 * An example of extending the provider class.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii2-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

namespace app\components\eauth\services\extended;

class FacebookOAuth2Service extends \nodge\eauth\services\FacebookOAuth2Service
{

    protected $scopes = [
        self::SCOPE_EMAIL,
//        self::SCOPE_USER_BIRTHDAY,
//        self::SCOPE_USER_HOMETOWN,
//        self::SCOPE_USER_LOCATION,
//        self::SCOPE_USER_PHOTOS,
    ];

    /**
     * http://developers.facebook.com/docs/reference/api/user/
     *
     * @see FacebookOAuth2Service::fetchAttributes()
     */
    protected function fetchAttributes()
    {
        $this->attributes = $this->makeSignedRequest('me', [
            'query' => [
                'fields' => join(',', [
                    'id',
                    'name',
                    'link',
                    'email',
//                    'verified',
//                    'first_name',
//                    'last_name',
//                    'gender',
//                    'birthday',
//                    'hometown',
//                    'location',
//                    'locale',
//                    'timezone',
//                    'updated_time',
                ])
            ]
        ]);

//        $this->attributes['photo_url'] = $this->baseApiUrl.$this->getId().'/picture?width=100&height=100';

        // подстраиваем под наши атрибуты
        $this->attributes['client_id']      = $this->getAttribute('id');
        $this->attributes['username']       = $this->getAttribute('name');
        $this->attributes['social_page']    = $this->getAttribute('link');
        $this->attributes['email']    = $this->getAttribute('email');
//        $this->attributes['avatar']         = $this->getAttribute('photo_url');
//        $this->attributes['sex']            = $this->getAttribute('gender') == 'male' ? 1 : 2; // 1-мужской; 2-женский;

        return true;
    }
}
