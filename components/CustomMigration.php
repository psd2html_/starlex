<?php

namespace app\components;

use yii\db\Migration;

class CustomMigration extends Migration
{
    const CASCADE   = 'CASCADE';
    const RESTRICT  = 'RESTRICT';
    const SET_NULL  = 'SET NULL';
    const NO_ACTION = 'NO ACTION';

    /**
     * @var null|string
     */
    protected $tableOptions = null;

    /**
     * @var \yii\rbac\DbManager
     */
    protected $authManager = null;

    /**
     * @var string
     */
    protected $pkPrefix = 'PK';

    /**
     * @var string
     */
    protected $idxPrefix = 'IDX';

    /**
     * @var string
     */
    protected $uniPrefix = 'UNI';

    /**
     * @var string
     */
    protected $fkPrefix = 'FK';

    protected $characterSet = 'utf8';
    protected $collate      = 'utf8_general_ci';
    protected $engine       = 'InnoDB';


    /**
     * CustomMigration constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        if ($this->db->driverName === 'mysql'){
            $this->tableOptions = "CHARACTER SET {$this->characterSet} COLLATE {$this->collate} ENGINE={$this->engine}";
        }
        $this->authManager = \Yii::$app->getAuthManager();
    }

    /**
     * @param string $name
     * @param string $table
     * @param string $columns
     */
    public function addPrimaryKey($name, $table, $columns)
    {
        if (is_null($name)){
            $name = $this->getPkName($table, $columns);
        }
        parent::addPrimaryKey($name, $table, $columns);
    }

    /**
     * @param $table
     * @param $columns
     *
     * @return string
     */
    public function getPkName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(sprintf('%s_%s', $this->pkPrefix, $table . '_' . implode('', (array)$columns)));
    }

    /**
     * @param $columns
     *
     * @return string
     */
    public function includePrimaryKey($columns)
    {
        $columns = is_array($columns) ? implode(',', $columns) : $columns;

        return ('PRIMARY KEY (' . $columns . ')');
    }

    /**
     * @param string     $table
     * @param string     $column
     * @param string     $name
     * @param bool|false $unique
     *
     * @internal param string $columns
     */
    public function createIndex($table, $column, $name = null, $unique = false)
    {
        if ($name === null){
            $name = $this->getIndexName($table, $column);
        }

        parent::createIndex($name, $table, $column, $unique);
    }

    public function createIndexByName($name, $table, $columns, $unique = false)
    {
        parent::createIndex($name, $table, $columns, $unique);
    }

    public function getIndexName($table, $column)
    {
        return self::getName('idx_', $table, $column);
    }

    private static function getName($prefix, $table, $column)
    {
        return $prefix . $table . '_' . $column;
    }

    public function dropIndex($table, $column)
    {
        $name = $this->getIndexName($table, $column);

        parent::dropIndex($name, $table);
    }

    public function dropIndexByName($name, $table)
    {
        parent::dropIndex($name, $table);
    }

    /**
     * @param $table
     * @param $columns
     *
     * @return string
     */
    public function getIdxName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(sprintf('%s_%s', $this->idxPrefix, $table . '_' . implode('', (array)$columns)));
    }

    /**
     * @param string       $name
     * @param string       $table
     * @param string       $columns
     * @param array|string $refTable
     * @param string       $refColumns
     * @param null         $delete
     * @param null         $update
     */
    public function addFK($table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        $name = $this->getFkName($table, $columns);

        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    public function getFkName($table, $column)
    {
        return self::getName('fk_', $table, $column);
    }

    /**
     * @param string       $name
     * @param string       $table
     * @param string       $columns
     * @param array|string $refTable
     * @param string       $refColumns
     * @param null         $delete
     * @param null         $update
     */
    public function dropFK($table, $columns)
    {
        $name = $this->getFkName($table, $columns);

        parent::dropForeignKey($name, $table);
    }

    public function tableExists($name)
    {
        return (bool)$this->db->createCommand("SHOW TABLES LIKE '$name'")
            ->queryOne();
    }

    /**
     * Включает или отключает проверку внешних ключей
     *
     * @param bool $bool
     *
     * @throws \yii\db\Exception
     */
    public function checkingFK(bool $bool)
    {
        $bool = (int)$bool;
        $command = "SET FOREIGN_KEY_CHECKS=$bool";
        echo "    > $command\n";

        $this->execute($command);
    }

    public function resetAutoIncrement($table)
    {
        $this->execute("ALTER TABLE `$table` AUTO_INCREMENT=0");
    }
}
