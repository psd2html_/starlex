<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\UploadedFile;
use Imagine\Image\Box; // ext. "yiisoft/yii2-imagine"
use yii\imagine\Image; // ext. "yiisoft/yii2-imagine"

/**
 * Класс для работы с картинками
 * Функции: сохранение картинок с миниатюрами и получение их
 *
 * Class ImageBehavior
 *
 * @property string $imageThumbUrl
 * @property string $baseImagePath
 */
class ImageBehavior extends Behavior
{
    public $imagePath       = ''; // папка где находятся картинки. Example: "images/lesson"
    public $imageThumbsPath = ''; // папка для Миниатюр
    public $imageField      = ''; // поле таблицы куда сохраняется картинка. Example: "image"

    // Если `true` то в параметре subfolder прописывается поле таблицы в качестве подпапки - динамическое свойство модели
    // Если `false` то в параметре subfolder указывается имя подпапки
    public $subfolderField  = false;

    public $subfolder       = ''; // имя поля (см. параметр `subfolderField`) или название подпапки (например 'subfolder/')

    public $thumb_width     = '50';
    public $thumb_height    = '50';


    public function init()
    {
        $this->thumb_width = Yii::$app->params['thumbnails']['width'];
        $this->thumb_height = Yii::$app->params['thumbnails']['height'];
    }


    /**
     * Тумбнейл картинки
     *
     * @param $subfolder - подпапка - ID сущности (урока, игры)
     * @return string
     */
    public function getImageThumbUrl()
    {
        // путь к миниатюре
        return $this->getBaseImagePath() . '/' . $this->imageThumbsPath . $this->thumb_width .'_'. $this->thumb_height . '_' . $this->owner->{$this->imageField};
    }


    /**
     * Базовый путь к папке с катинками
     *
     * @return string
     */
    public function getBaseImagePath()
    {
        //$_subfolder = ($this->subfolderField) ? ($this->owner->{$this->subfolder}) : ($this->subfolder);
        //$return = Yii::getAlias('@uploads'). '/' . $this->imagePath .'/'. $_subfolder;
        $return = Yii::getAlias('@uploads'). '/' . $this->imagePath;
        return $return;
    }


    /**
     * Сохранение картинок
     *
     * @param $model
     * @param $field
     * @param $file
     */
    public function attachImage($file = '')
    {
        // путь куда будут сохраняться картинки
        $dir = $this->getBaseImagePath() . '/';
        $model = $this->owner;
        $field = $this->imageField;

        if (empty($file))
            $file = UploadedFile::getInstance($model, $field);

        if ($file && $file->tempName)
        {
            $model->{$field} = $file;
            if ($model->validate([$field]))
            {
                // create directory
                $this->createDirectory($dir);

                $fileName = substr(md5($model->$field->baseName), 0, 15) . '.' . $model->$field->extension;

                $model->$field->saveAs($dir . $fileName);
                $model->$field = $fileName;
                $model->save();

                // Thumbnail
                $this->attachThumbnail($fileName, $file);
            }
        }
    }


    /**
     * Сохранить миниатюру
     *
     * @param $fileName - имя сгенерированного оригинального файла
     * @param $file - файл
     */
    public function attachThumbnail($fileName, $file)
    {
        $model = $this->owner;
        $field = $this->imageField;

        $model->{$field} = $file;
        $fileNameThumbs = $this->thumb_width .'_'. $this->thumb_height . '_' . substr(md5($model->$field->baseName), 0, 15) .'.'. $model->$field->extension;

        $dir = $this->getBaseImagePath() . '/';
        $this->createDirectory($dir . $this->imageThumbsPath);

        // Для ресайза фотки до 800x800px по большей стороне надо обращаться к функции Box() или widen,
        // так как в обертках доступны только 5 простых функций: crop, frame, getImagine, setImagine, text, thumbnail, watermark
        $photo = Image::getImagine()->open($dir . $fileName);
        $photo->thumbnail(new Box(800, 800))->save($dir . $fileName, ['quality' => 100]);
        //$imagineObj = new Imagine();
        //$imageObj = $imagineObj->open(\Yii::$app->basePath . $dir . $fileName);
        //$imageObj->resize($imageObj->getSize()->widen(400))->save(\Yii::$app->basePath . $dir . $fileName);
        Image::thumbnail($dir . $fileName, $this->thumb_width, $this->thumb_height)
            ->save(Yii::getAlias($dir . $this->imageThumbsPath . $fileNameThumbs), ['quality' => 100]);
    }


    /**
     * Удаляем обычную картинку
     */
    public function deleteAttach()
    {
        $model = $this->owner;
        $field = $this->imageField;

        $current_image = !empty($model->$field) ? ($model->$field) : $model->OldAttributes[$field];

        // Удаляем оригинальную картинку
        $dir = $this->getBaseImagePath() . '/';
        if (!empty($current_image) and file_exists( $dir . $current_image))
            unlink($dir . $current_image); //удаляем файл

        $model->$field = '';

        // Удаляем миниатюру
        $this->deleteAtachThumbnail();
    }


    /**
     * Удаляем миниатюру
     */
    public function deleteAtachThumbnail()
    {
        $model = $this->owner;
        $field = $this->imageField;

        $current_image = !empty($model->$field) ? ($model->$field) : $model->OldAttributes[$field];

        // удаляем thumbnail
        $dir = $this->getBaseImagePath() .'/'. $this->imageThumbsPath;
        if (!empty($current_image) and file_exists( $dir . $this->thumb_width .'_'. $this->thumb_height . '_' . $current_image))
            unlink($dir . $this->thumb_width .'_'. $this->thumb_height . '_' . $current_image);

        $model->$field = '';
    }


    /**
     * Создать папку
     *
     * @param $path
     */
    public function createDirectory($path)
    {
        if (file_exists($path)) {
            //echo "The directory {$path} exists";
        } else {
            mkdir($path, 0775, true);
            //echo "The directory {$path} was successfully created.";
        }
    }


    /**
     * удаляем папку с картинками
     *
     * @param $dir
     */
    public function removeDirectory($dir)
    {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? $this->removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }
}