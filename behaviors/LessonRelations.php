<?php

namespace app\behaviors;


use app\exceptions\LessonRelationSaverException;
use app\helpers\FlashHelper;
use app\models\Language;
use app\models\LessonTranslate;
use app\models\LessonWord;
use app\models\Word;
use app\models\WordGroup;
use yii\base\Behavior;
use yii\db\ActiveRecord;


/**
 * Class LessonRelationSaver
 *
 * @property \app\models\Lesson $owner
 *
 * @package app\behaviors
 */
class LessonRelations extends Behavior
{
    private $ruLangId;
    private $saveErrors;
    private $wordGroupIsNew;
    /** @var  \app\models\Lesson */
    private $lesson;
    private $lessonIsNew;
    /** @var  WordGroup */
    private $wordGroup;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT  => 'saveRelations',
            ActiveRecord::EVENT_AFTER_UPDATE  => 'saveRelations',
            ActiveRecord::EVENT_BEFORE_DELETE => 'deleteGroups',
        ];
    }

    public function deleteGroups()
    {
        // Remove word groups
        foreach ($this->owner->wordGroupsRelation as $wordGroup){
            $wordGroup->delete();
        }
    }

    public function saveRelations()
    {
        $this->lesson = $this->owner;
        try {

            if (!$ruLang = Language::findOne(['locale' => 'ru'])){
                $this->saveErrors('Не найден русский язык');
            }

            $this->ruLangId = $ruLang->id;

            $this->lessonIsNew = $this->lesson->isNewRecord;

            // Сохранение переводов
            $this->saveLessonTranslates();

            // Сохранение слов
            if (!$this->lesson->wordGroups){
                $this->saveErrors('Заполните группу слов');
            } else {
                foreach ($this->lesson->wordGroups as $wordGroupId => $group){
                    if (preg_match('/new_\d+/', $wordGroupId)){
                        $wordGroupId = false;
                    }

                    $this->saveWordGroup($wordGroupId, $group);

                    if ($this->wordGroupIsNew){
                        $newLessonWord = new LessonWord();
                        $newLessonWord->lesson_id = $this->lesson->id;
                        $newLessonWord->word_group_id = $this->wordGroup->id;
                        if (!$newLessonWord->save()){
                            $this->saveErrors($newLessonWord->getFirstErrors());
                        }
                    }

                    $this->saveWords($group['words']);
                }
            }
        } catch (LessonRelationSaverException $tryError){
            //FlashHelper::setAlert($tryError);

            $this->owner->addError('wordGroups', $tryError);
        }

        return true;
    }

    /**
     * @param array|string $message
     *
     * @throws LessonRelationSaverException
     */
    private function saveErrors($message)
    {
        $this->saveErrors = is_array($message) ? implode(PHP_EOL, $message) : $message;

        throw new LessonRelationSaverException($this->saveErrors);
    }

    private function saveLessonTranslates()
    {
        if ($this->lesson->names){
            foreach ($this->lesson->names as $languageId => $translate){
                $lessonTranslate = LessonTranslate::findOne([
                    'lang_id'   => $languageId,
                    'lesson_id' => $this->lesson->id,
                ]);

                if (!$lessonTranslate){
                    $lessonTranslate = new LessonTranslate();
                    $lessonTranslate->lang_id = $languageId;
                    $lessonTranslate->lesson_id = $this->lesson->id;
                }

                if ($translate){
                    $lessonTranslate->translate = $translate;
                }

                if (!$lessonTranslate->save()){
                    $this->saveErrors($lessonTranslate->getFirstErrors());
                };
            }
        } else {
            $this->saveErrors('Заполните переводы лекций');
        }
    }

    private function saveWordGroup($wordGroupId, $groupData)
    {
        $this->wordGroup = null;
        $this->wordGroupIsNew = false;
        if (!$this->lessonIsNew && $wordGroupId !== false){
            $this->wordGroup = WordGroup::findOne($wordGroupId);
        }

        if ($this->wordGroup === null){
            $this->wordGroupIsNew = true;
            $this->wordGroup = new WordGroup();
            $this->wordGroup->name = $groupData['words'][$this->ruLangId];
        }

        $this->wordGroup->description = $groupData['description'];

        if (!$this->wordGroup->save()){
            $this->saveErrors($this->wordGroup->getFirstErrors());
        }
    }

    private function saveWords($words)
    {
        foreach ($words as $languageId => $word){
            if ($language = Language::findOne($languageId)){
                $newWord = null;
                if (!$this->lessonIsNew){
                    $newWord = Word::findOne([
                        'word_group_id' => $this->wordGroup->id,
                        'lang_id'       => $language->id,
                    ]);
                }

                if (!$newWord){
                    $newWord = new Word();
                    $newWord->word_group_id = $this->wordGroup->id;
                    $newWord->lang_id = $language->id;
                    $newWord->status = $newWord::STATUS_ENABLED;
                }

                $newWord->name = $word["name"];
                $newWord->article = $word["article"];
                if (!$newWord->save()){
                    $this->saveErrors($newWord->getFirstErrors());
                }
            }
        }
    }
}