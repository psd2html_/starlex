<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\UploadedFile;

/**
 * Class MediaFileBehavior
 *
 * @package app\behaviors
 *
 * @property string $baseFilePath
 */
class MediaFileBehavior extends Behavior
{
    public $filePath = ''; // media/lessons
    public $fileField = 'file';

    // Если `true` то в параметре subfolder прописывается поле таблицы в качестве подпапки - динамическое свойство модели
    // Если `false` то в параметре subfolder указывается имя подпапки
    public $subfolderField  = false;

    public $subfolder       = ''; // имя поля (см. параметр `subfolderField`) или название подпапки (например 'subfolder/')


    /**
     * Базовый путь к папке
     *
     * @return string
     */
    public function getBaseFilePath()
    {
        $_subfolder = ($this->subfolderField) ? ($this->owner->{$this->subfolder}) : ($this->subfolder);
        $path = Yii::getAlias('@uploads'). '/' . $this->filePath;

        return $path . ($_subfolder ? '/'. $_subfolder : '');
    }


    /**
     * Сохранить файл
     *
     * @param string $file
     */
    public function attachFile($file = '')
    {
        if (empty($file))
            $file = UploadedFile::getInstance($this->owner, $this->fileField);

        if ($file && $file->tempName)
        {
            $dir = $this->getBaseFilePath() . '/';

            // create directory
            $this->createDirectory($dir);

            $file->saveAs($dir . $file->name);
            $this->owner->{$this->fileField} = $file->name;
            //$this->owner->save();
        }
    }


    /**
     * Удалить файл
     */
    public function deleteFile()
    {
        $model = $this->owner;
        $field = $this->fileField;

        $dir = $this->getBaseFilePath() . '/';
        $current_file = !empty($model->$field) ? ($model->$field) : $model->OldAttributes[$field];

        if (!empty($current_file) and file_exists( $dir . $current_file )) {
            unlink($dir . $current_file);
        }

        $model->{$this->fileField} = '';
    }


    /**
     * Создать папку
     *
     * @param $path
     */
    public function createDirectory($path)
    {
        if (file_exists($path)) {
            //echo "The directory {$path} exists";
        } else {
            mkdir($path, 0775, true);
            //echo "The directory {$path} was successfully created.";
        }
    }


    /**
     * удаляем папку с картинками
     *
     * @param $dir
     */
    public function removeDirectory($dir)
    {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? $this->removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }

}