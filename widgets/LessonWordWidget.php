<?php


namespace app\widgets;


use yii\base\Exception;
use yii\base\Widget;

/**
 * Class LessonWordWidget
 *
 * @package app\widgets
 */
class LessonWordWidget extends Widget
{
    public $isEmpty = false;
    public $form;
    public $model;

    public function run()
    {
        if (isset($this->form, $this->model)){
            if ( $this->isEmpty ){
                return $this->render('lesson-word-widget/new-hidden', [
                    'form'  => $this->form,
                    'model' => $this->model,
                    'languages' => \app\models\Language::collection(),
                ]);
            } else {
                return $this->render('lesson-word-widget/widget', [
                    'form'       => $this->form,
                    'model'      => $this->model,
                ]);
            }
        } else {
            $error = !$this->form ? 'form' : (!$this->model ? 'model' : '');

            throw new Exception("Пустое свойство: '$error'");
        }
    }
}