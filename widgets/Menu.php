<?php
namespace app\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Menu
 *
 * @package backend\components\widget
 */
class Menu extends \yii\widgets\Menu
{
    /**
     * @var string
     */
    public $linkTemplate = "<a href=\"{url}\">{icon} {label} <span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span></a>";
    /**
     * @var string
     */
    public $labelTemplate = "<a href=\"{url}\">{label} <span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span></a>";


    /**
     * @var string
     */
    public $badgeTag = 'span';
    /**
     * @var string
     */
    public $badgeClass = 'label pull-right';
    /**
     * @var string
     */
    public $badgeBgClass;

    /**
     * @var string
     */
    public $parentRightIcon = "<span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span>";

    public $submenuTemplate = "\n<ul class=\"treeview-menu\" {show}>\n{items}\n</ul>\n";
    public $activateParents = true;

    /**
     * Recursively renders the menu items (without the container tag).
     *
     * @param array $items the menu items to be rendered recursively
     *
     * @return string the rendering result
     */
    protected function renderItems($items)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item){
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];
            if ($item['active']){
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null){
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null){
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($class)){
                if (empty($options['class'])){
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }
            $menu = $this->renderItem($item);
            if (!empty($item['items'])){
                $menu .= strtr($this->submenuTemplate, [
                    '{show}'  => $item['active'] ? "style='display: block'" : '',
                    '{items}' => $this->renderItems($item['items']),
                ]);
            }
            $lines[] = Html::tag($tag, $menu, $options);
        }

        return implode("\n", $lines);
    }

    protected function renderItem($item)
    {
        $item['badgeOptions'] = isset($item['badgeOptions']) ? $item['badgeOptions'] : [];

        if (!ArrayHelper::getValue($item, 'badgeOptions.class')){
            $bg = isset($item['badgeBgClass']) ? $item['badgeBgClass'] : $this->badgeBgClass;
            $item['badgeOptions']['class'] = $this->badgeClass . ' ' . $bg;
        }

        if (isset($item['items'])){
            if (!isset($item['right-icon'])){
                $item['right-icon'] = $this->parentRightIcon;
            }
        } else {
            $this->linkTemplate = "<a href=\"{url}\">\n{icon}\n{label}\n{right-icon}{badge}</a>";
            $this->labelTemplate = "{icon}\n{label}\n{badge}";
        }

        if (isset($item['url'])){
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
            $replace = [
                '{badge}'      => isset($item['badge'])
                    ? Html::tag('span', Html::tag('small', $item['badge'], $item['badgeOptions']), ['class' => 'pull-right-container'])
                    : '',
                '{url}'        => Url::to($item['url']),
                '{label}'      => $item['label'],
                '{right-icon}' => isset($item['right-icon']) ? $item['right-icon'] : '',
            ];

            if (!empty($item['icon'])){
                $replace += [
                    '{icon}' => "<i class=\"{$item['icon']}\"></i> ",
                ];
            }

            return strtr($template, $replace);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            $replace = [
                '{badge}'      => isset($item['badge']) ? Html::tag('small', $item['badge'], $item['badgeOptions'])
                    : '',
                '{label}'      => $item['label'],
                '{right-icon}' => isset($item['right-icon']) ? $item['right-icon'] : '',
                '{icon}'       => empty($item['icon']) ? null : '<i class="' . $item['icon'] . '"></i> ',
            ];

            return strtr($template, $replace);
        }
    }

    protected function normalizeItems($items, &$active)
    {
        foreach ($items as $i => $item){
            if (isset($item['visible']) && !$item['visible']){
                unset($items[$i]);
                continue;
            }
            if (!isset($item['label'])){
                $item['label'] = '';
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $items[$i]['label'] = $encodeLabel ? Html::encode($item['label']) : $item['label'];
            $items[$i]['icon'] = isset($item['icon']) ? $item['icon'] : '';
            $hasActiveChild = false;
            if (isset($item['items'])){
                $items[$i]['items'] = $this->normalizeItems($item['items'], $hasActiveChild);
                if (empty($items[$i]['items']) && $this->hideEmptyItems){
                    unset($items[$i]['items']);
                    if (!isset($item['url'])){
                        unset($items[$i]);
                        continue;
                    }
                }
            }
            if (!isset($item['active'])){
                if ($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item)){
                    $active = $items[$i]['active'] = true;
                } else {
                    $items[$i]['active'] = false;
                }
            } elseif ($item['active']) {
                $active = true;
            }
        }

        return array_values($items);
    }
    /**
     * Checks whether a menu item is active.
     * This is done by checking if [[route]] and [[params]] match that specified in the `url` option of the menu item.
     * When the `url` option of a menu item is specified in terms of an array, its first element is treated
     * as the route for the item and the rest of the elements are the associated parameters.
     * Only when its route and parameters match [[route]] and [[params]], respectively, will a menu item
     * be considered active.
     *
     * @param array $item the menu item to be checked
     *
     * @return boolean whether the menu item is active
     */
    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])){
            $route = $item['url'][0];
            if ($route[0] !== '/' && Yii::$app->controller){
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }
            $arrayRoute = explode('/', ltrim($route, '/'));
            $arrayThisRoute = explode('/', $this->route);
            if ($arrayRoute[0] !== $arrayThisRoute[0]){
                return false;
            }
            if (isset($arrayRoute[1]) && $arrayRoute[1] !== $arrayThisRoute[1]){
                return false;
            }
            if (isset($arrayRoute[2]) && $arrayRoute[2] !== $arrayThisRoute[2]){
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1){
                foreach (array_splice($item['url'], 1) as $name => $value){
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)){
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }
}