<?php
/** @var $model app\models\Lesson */
/** @var $languages array */
?>

<div class="col-md-3 new-group-hidden">
    <div class="new-word-border">
        <h4>NEW </h4>
        <?php foreach ($languages as $languageId => $langName): ?>
            <?= $form->field($model, "wordGroups[new][words][$languageId][name]")
                ->textInput([
                    'required' => 'required',
                ])
                ->label($langName); ?>
            <?= $form->field($model, "wordGroups[new][words][$languageId][article]")
                ->textInput([
                    //'required' => 'required',
                ])
                ->label('Артикль'); ?>
            <?= $form->field($model, "wordGroups[new][article]")
                ->textInput()->label('Артикль')?>
        <?php endforeach; ?>
        <?= $form->field($model, "wordGroups[new][description]")
            ->textInput()->label('Описание')?>
        <span class="remove-word">&times;</span>
    </div>
</div>