<?php

/** @var $countWords integer */
/** @var $model app\models\Lesson */

?>
<?php if (!$model->isNewRecord): ?>
    <?php foreach ($model->lessonWords as $key => $lessonWord): ?>
        <?php
        /** @var $lessonWord app\models\LessonWord */

        $groupId = $lessonWord->word_group_id;
        ?>
        <div class="col-md-3 word-group">
            <div class="new-word-border">
                <h4>Группа ID: <?= $groupId; ?></h4>
                <?php foreach ($lessonWord->words as $id => $word): ?>
                    <?= $form->field($model, "wordGroups[$groupId][words][$word->lang_id][article]")
                        ->textInput([
                            //'required' => 'required',
                            'value'    => $word->article,
                        ])
                        ->label('Артикль'); ?>
                    <?= $form->field($model, "wordGroups[$groupId][words][$word->lang_id][name]")
                        ->textInput([
                            'required' => 'required',
                            'value'    => $word->name,
                        ])
                        ->label($word->lang->name); ?>
                <?php endforeach; ?>
                <?= $form->field($model, "wordGroups[$groupId][description]")
                    ->textInput([
                        'value' => $lessonWord->wordGroup->description
                    ])->label('Описание')?>
                <span class="remove-word" data-group-id="<?= $groupId; ?>">&times;</span>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
