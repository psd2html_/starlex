<?php

namespace app\widgets;

use app\models\Language;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Nav;

class TranslateMenuWidget extends Widget
{
    public $url;

    public function run()
    {
        $Languages = Language::find()
            ->asArray()
            ->all();

        $menuItems = [];
        foreach ($Languages as $key => $value){
            $menuItems[] = [
                'label' => $value['name'],
                'url'   => [$this->url, 'lang' => $value['locale'], 'lang_id' => $value['id']],
            ];
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items'   => array_filter([
                !Yii::$app->user->isGuest ? [
                    'label' => Yii::t('app', 'Translate'),
                    'items' => $menuItems,
                ] : false,
            ]),
        ]);
    }
}

