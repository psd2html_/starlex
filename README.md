Project: 'Starlex'
==================
#### PHP: >= 7.1
#### Framework: >= Yii 2.0.13
#### Database: MySQL >= 5.7

INSTALLATION
------------
#### Порядок установки: 
1. Клонировать проект из репозитория
1. Установить/запустить Composer
1. Установить менеджер ассетов
    ```bash
    composer global require "fxp/composer-asset-plugin:~1.3"
    ```
1. Настроить доступ в БД в файле config/db-local.php
1. Настроить параметры в файле config/params-local.php
1. Выполнить все миграции


#### Clone project
~~~
git clone https://[YOUR_ACCOUNT_ON_BITBUCKET]@bitbucket.org/psd2html_/starlex.git
~~~

#### Composer. In project folder
~~~
php composer.phar install
~~~

#### Migration
~~~
php yii migrate --migrationPath=@yii/rbac/migrations/
php yii migration/up
~~~

DIRECTORY STRUCTURE
-------------------

      application/assets/             contains assets definition      
      application/commands/           contains console commands (controllers)
	  application/config/             contains application configurations      
      application/controllers/        contains web controller classes      
      application/data/               contains different files      
      application/mail/               contains view files for e-mails
      application/migrations/         contains migrations files
      application/models/             contains model classes
      application/modules/            contains modules
      application/modules/api         contains API      
      application/runtime/            contains files generated during runtime
	  application/tests/              contains various tests for the basic application
	  application/vendor/             contains dependent 3rd-party packages
	  application/views/              contains view files for the Web application
	  application/web/                contains the entry script and Web resources
      application/web/assets          contains web assets
      application/web/css             contains styles folder
      application/web/js              contains java-script folder
	  application/web/image           contains images	  
	  application/widgets             contains widgets	  
	  
Apache config
-------------------
~~~
<VirtualHost *:%httpport%>
    DocumentRoot    "%hostdir%/[PATH_TO_WEB]/web/"
    ServerName      "%host%"
    ...
</VirtualHost>
~~~

ACCESS - DEV
-------------------
#### Dev Server
http://starlex.co

#### SSH
~~~
host: ssh -i "Starlex.pem" ubuntu@ec2-13-59-41-80.us-east-2.compute.amazonaws.com
Ключ "Starlex.pem"
~~~

#### PhpMyAdmin
~~~
http://backend.starlex.co/phpmyadmin
login: root
passw: 721e6712te
~~~

#### DB
~~~
db_name: starlex_db
db_login: root
db_pass: 721e6712te
~~~

#### Account:
~~~
login: admin@starlex.co
pass: admin!admin
~~~

###Ссылки на другие документы
[Администрирование](docs/Administration.md)
