<?php

namespace app\commands;

use app\modules\api\helpers\RouteControllersHelper;
use yii\console\Controller;

class RouteController extends Controller
{
    public function actionDefault()
    {
        $config = \yii\helpers\ArrayHelper::merge(
            require(__DIR__ . '/../config/web.php'),
            require(__DIR__ . '/../config/web-local.php'
            ));

        $app = new \yii\web\Application($config);
        $module = $app->getModule('api');

        $routeHelper = new RouteControllersHelper([], $module);

        $routes = [];
        foreach ($routeHelper->getApiControllers() as $controller){
            $routes[] = $controller->getRoute();
        }
    }
}
