<?php

namespace app\commands;

use yii\console\Controller;

/**
 * Commends for server, via ssh-connection
 *
 * Class ServerController
 *
 * @package app\commands
 */
class ServerController extends Controller
{
    /**
     * GIT PULL in the server
     *
     * В корне папки проекта должен лежать ssh-ключ Starlex.pem.
     * > php yii server/git-pull
     */
    public function actionGitPull()
    {
        $cmd = 'ssh -i "Starlex.pem" ubuntu@ec2-13-59-41-80.us-east-2.compute.amazonaws.com \'{c}\'';
        $cmd = strtr($cmd, [
            '{c}' => 'sh git-pull.sh',
        ]);
        echo '> '.$cmd . PHP_EOL;
        echo shell_exec($cmd);
    }
}
