<?php

namespace app\commands;

use Faker\Factory;
use yii\console\Controller;

/**
 * Debug commands for development
 *
 * Class ServerController
 *
 * @package app\commands
 */
class TestController extends Controller
{
    public function actionIndex()
    {
//        return;
        if (YII_ENV_PROD){
            return;
        }
        /////////////////////

        $userDataSql = <<<SQL
INSERT IGNORE INTO `user` (`username`, `email`, `password_hash`, `status`, `auth_key`, `created_at`, `updated_at`, `rating`, `type`) VALUES ('{@username@}', '{@email@}', '$2y$13\$cVp3jabZerSEObs.HTQ8iu8PrhfGJBBOnVBMZKZHKTLBSFaEXAcHW', 1, 'uthQCSCbTvG-oaX6Ng9ZYhsNcScXBKBg', '1503924264', '1503924264', 0, 0)
SQL;


        for ($i = 1; $i < 1000; $i++){
            $faker = Factory::create();

            $sql = strtr($userDataSql, [
                '{@username@}' => $faker->userName,
                '{@email@}'    => $faker->email,
            ]);

            \Yii::$app->db->createCommand($sql)
                ->execute();
        }
    }

    public function actionGet()
    {
        $timeStart = microtime(true);
        $arrayResults = [
            'min' => [
                'first'  => 0,
                'second' => 0,
            ],
            'max' => [
                'first'  => 0,
                'second' => 0,
            ],
        ];

        $i = 0;
//        Console::startProgress(0, 1000);
        do {
            $testing = $this->testQuery();

            if ($i === 0){
                $arrayResults['min']['first'] = $arrayResults['max']['first'] = $testing['first'];
                $arrayResults['min']['second'] = $arrayResults['max']['second'] = $testing['second'];
            }

            if ($testing['first'] < $arrayResults['min']['first']){
                $arrayResults['min']['first'] = $testing['first'];
            }
            if ($testing['first'] > $arrayResults['max']['first']){
                $arrayResults['max']['first'] = $testing['first'];
            }

            if ($testing['second'] < $arrayResults['min']['second']){
                $arrayResults['min']['second'] = $testing['second'];
            }
            if ($testing['second'] > $arrayResults['max']['second']){
                $arrayResults['max']['second'] = $testing['second'];
            }
//            Console::updateProgress($i, 1000);
        } while ($i++ < 1);
//        Console::endProgress();

        var_dump($arrayResults);

        echo (microtime(true) - $timeStart) . PHP_EOL;
    }

    private function testQuery()
    {
        $firstSql = <<<SQL
SELECT id
FROM (SELECT
        id,
        email
      FROM user
      GROUP BY email) AS id
SQL;


        $secondSql = <<<SQL
SELECT U.id
FROM user AS U
  JOIN (SELECT
               id,
               email
             FROM user
             GROUP BY email) AS filtered_user ON filtered_user.id = U.id;
SQL;

        /// FIRST
        $timeStart = microtime(true);

        \Yii::$app->db->createCommand($firstSql)
            ->queryScalar();

        $timeFirst = microtime(true) - $timeStart;
//        Console::output($timeFirst);

        /// SECOND

        $timeStart = microtime(true);

        \Yii::$app->db->createCommand($secondSql)
            ->queryScalar();

        $timeSecond = microtime(true) - $timeStart;

//        Console::output($timeSecond);

        return [
            'first'  => $timeFirst,
            'second' => $timeSecond,
        ];
    }

    public function actionMulti()
    {
        $descriptorspec = [
            0 => ["pipe", "r"],  // stdin is a pipe that the child will read from
            1 => ["pipe", "w"],  // stdout is a pipe that the child will write to
            2 => ["file", "error-output.txt", "a"] // stderr is a file to write to
        ];
        for ( $i = 1; $i <= 8; $i++ ) {
            proc_open('php ' . realpath(__DIR__ . '/../') . '/yii test', $descriptorspec, $pipes);
        }
    }
}
