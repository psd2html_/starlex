<?php


namespace app\commands;

use app\models\LessonTranslate;
use yii\console\Controller;
use yii\helpers\Console;

class DataController extends Controller
{
    private $arr = [
        'Урок 1' => 'ПЕРВЫЙ УРОК',
        'Урок 2' => 'ВТОРОЙ УРОК',
        'Урок 3' => 'ТРЕТИЙ УРОК',
        'Урок 4' => 'ЧЕТВЕРТЫЙ УРОК',
        'Урок 5' => 'ПЯТЫЙ УРОК',
        'Урок 6' => 'ШЕСТОЙ УРОК'
    ];

    public function actionCheckCollocationWords()
    {

    }

    public function actionRenameLesson()
    {
        $models = LessonTranslate::find()->where(['lang_id' => 1])->all();

        foreach ($models as $model) {
            $model->translate = $this->arr[$model->translate];
            $model->save();
        }

        Console::output('ok');
    }
}
