<?php

namespace app\commands;

use app\components\CustomMigration;
use app\models\Language;
use app\models\Lesson;
use app\models\LessonImage;
use app\models\LessonMedia;
use app\models\LessonTranslate;
use app\models\LessonWord;
use app\models\Theme;
use app\models\ThemeTranslate;
use app\models\Word;
use app\models\WordGroup;
use yii\console\Controller;
use yii\console\Exception;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\FileHelper;

/**
 * Class DataController
 *
 * @package app\commands
 */
class DemoController extends Controller
{
    const LANG_RU  = 'ru';
    const LANG_USA = 'en';
    const LANG_UK  = 'uk';

    const LANGUAGES = [
        self::LANG_RU  => 'Русский',
        self::LANG_USA => 'English (USA)',
        self::LANG_UK  => 'English (UK)',
    ];

    const DEMO_DATA = [
        'themes' => [
            [
                'names'   => [
                    self::LANG_RU  => 'Первые Слова',
                    self::LANG_USA => 'English (USA)',
                    self::LANG_UK  => 'English (UK)',
                ],
                'parent'  => false,
                'lessons' => [
                    [
                        'names' => [
                            self::LANG_RU  => 'Урок 1',
                            self::LANG_USA => 'Lesson 1 (USA)',
                            self::LANG_UK  => 'Lesson 1 (UK)',
                        ],
                        'words' => [
                            [
                                'items'       => [
                                    self::LANG_RU  => 'имя',
                                    self::LANG_USA => 'first name',
                                    self::LANG_UK  => 'name',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'фамилия',
                                    self::LANG_USA => 'last name',
                                    self::LANG_UK  => 'surname',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'привет',
                                    self::LANG_USA => 'hello',
                                    self::LANG_UK  => 'hello',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'приятно познакомиться',
                                    self::LANG_USA => 'nice to meet you',
                                    self::LANG_UK  => 'nice to meet you',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'пока',
                                    self::LANG_USA => 'bye',
                                    self::LANG_UK  => 'bye',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'спасибо',
                                    self::LANG_USA => 'thank you',
                                    self::LANG_UK  => 'thank you',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'простите',
                                    self::LANG_USA => 'sorry',
                                    self::LANG_UK  => 'sorry',
                                ],
                                'description' => null,
                            ],
                        ],
                    ],
                    [
                        'names' => [
                            self::LANG_RU  => 'Урок 2',
                            self::LANG_USA => 'Lesson 2 (USA)',
                            self::LANG_UK  => 'Lesson 2 (UK)',
                        ],
                        'words' => [
                            [
                                'items'       => [
                                    self::LANG_RU  => 'адрес',
                                    self::LANG_USA => 'address',
                                    self::LANG_UK  => 'address',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'электронная почта',
                                    self::LANG_USA => 'e-mail',
                                    self::LANG_UK  => 'e-mail',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'собачка',
                                    self::LANG_USA => 'at',
                                    self::LANG_UK  => 'at',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'точка',
                                    self::LANG_USA => 'dot',
                                    self::LANG_UK  => 'dot',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'дефис',
                                    self::LANG_USA => 'hyphen',
                                    self::LANG_UK  => 'hyphen',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'нижнее тире',
                                    self::LANG_USA => 'underscore',
                                    self::LANG_UK  => 'underscore',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'возраст',
                                    self::LANG_USA => 'age',
                                    self::LANG_UK  => 'age',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'работа',
                                    self::LANG_USA => 'job',
                                    self::LANG_UK  => 'job',
                                ],
                                'description' => null,
                            ],
                        ],
                    ],
                    [
                        'names' => [
                            self::LANG_RU  => 'Урок 3',
                            self::LANG_USA => 'Lesson 3 (USA)',
                            self::LANG_UK  => 'Lesson 3 (UK)',
                        ],
                        'words' => [
                            [
                                'items'       => [
                                    self::LANG_RU  => 'я',
                                    self::LANG_USA => 'I',
                                    self::LANG_UK  => 'I',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'ты',
                                    self::LANG_USA => 'you',
                                    self::LANG_UK  => 'you',
                                ],
                                'description' => 'you(ты)',
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'он',
                                    self::LANG_USA => 'he',
                                    self::LANG_UK  => 'he',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'она',
                                    self::LANG_USA => 'she',
                                    self::LANG_UK  => 'she',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'оно',
                                    self::LANG_USA => 'it',
                                    self::LANG_UK  => 'it',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'мы',
                                    self::LANG_USA => 'we',
                                    self::LANG_UK  => 'we',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'вы',
                                    self::LANG_USA => 'you',
                                    self::LANG_UK  => 'you',
                                ],
                                'description' => 'you(вы)',
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'Вы',
                                    self::LANG_USA => 'you',
                                    self::LANG_UK  => 'you',
                                ],
                                'description' => 'you(Уважительно)',
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'они',
                                    self::LANG_USA => 'they',
                                    self::LANG_UK  => 'they',
                                ],
                                'description' => null,
                            ],
                            [
                                'items'       => [
                                    self::LANG_RU  => 'они',
                                    self::LANG_USA => 'they',
                                    self::LANG_UK  => 'they',
                                ],
                                'description' => null,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'names'   => [
                    self::LANG_RU  => 'Знакомимся',
                    self::LANG_USA => "Let's Get to Know Each Other (USA)",
                    self::LANG_UK  => "Let's Get to Know Each Other (UK)",
                ],
                'parent'  => 'Первые Слова',
                'lessons' => [],
            ],
        ],
    ];

    /** @var  Transaction */
    private $transaction;
    private $languages;
    /** @var  Theme */
    private $newTheme;
    /** @var  Lesson */
    private $newLesson;
    /** @var  WordGroup */
    private $newWordGroup;

//    const WORDS_BY_LANG_ID = [
//        // ru
//        1 => [
//            // Урок 1
//            1 => [
//                1 => 'имя',
//                2 => 'фамилия',
//                3 => 'привет',
//                4 => 'приятно познакомиться',
//                5 => 'пока',
//                6 => 'спасибо',
//                7 => 'простите',
//            ],
//            // Урок 2
//            2 => [
//                8  => 'адрес',
//                9  => 'электронная почта',
//                10 => 'собачка',
//                11 => 'точка',
//                12 => 'дефис',
//                13 => 'нижнее тире',
//                14 => 'возраст',
//                15 => 'работа',
//            ],
//            // Урок 3
//            3 => [
//                16 => 'я',
//                17 => 'ты',
//                18 => 'он',
//                19 => 'она',
//                20 => 'оно',
//                21 => 'мы',
//                22 => 'вы',
//                23 => 'Вы',
//                24 => 'они',
//            ],
//        ],
//
//        // USA
//        2 => [
//            // Урок 1
//            1 => [
//                25 => 'first name',
//                26 => 'last name',
//                27 => 'hello',
//                28 => 'nice to meet you',
//                29 => 'bye',
//                30 => 'thank you',
//                31 => 'sorry',
//            ],
//            // Урок 2
//            2 => [
//                32 => 'address',
//                33 => 'e-mail',
//                34 => 'at',
//                35 => 'dot',
//                36 => 'hyphen',
//                37 => 'underscore',
//                38 => 'age',
//                39 => 'job',
//            ],
//            // Урок 3
//            3 => [
//                40 => 'I',
//                41 => 'you',
//                42 => 'he',
//                43 => 'she',
//                44 => 'it',
//                45 => 'we',
//                46 => 'you',
//                47 => 'you',
//                48 => 'they',
//            ],
//        ],
//
//        // UK
//        3 => [
//            // Урок 1
//            1 => [
//                49 => 'name',
//                50 => 'surname',
//                51 => 'hello',
//                52 => 'nice to meet you',
//                53 => 'bye',
//                54 => 'thank you',
//                55 => 'sorry',
//            ],
//            // Урок 2
//            2 => [
//                56 => 'address',
//                57 => 'e-mail',
//                58 => 'at',
//                59 => 'dot',
//                60 => 'hyphen',
//                61 => 'underscore',
//                62 => 'age',
//                63 => 'job',
//            ],
//            // Урок 3
//            3 => [
//                64 => 'I',
//                65 => 'you',
//                66 => 'he',
//                67 => 'she',
//                68 => 'it',
//                69 => 'we',
//                70 => 'you',
//                71 => 'you',
//                72 => 'they',
//            ],
//        ],
//    ];

    public function actionSet()
    {
        $this->transaction = \Yii::$app->db->beginTransaction();

        $this->cleaningTables();

        try {
            // Языки
            foreach (self::LANGUAGES as $langLocale => $langName){
                $newLang = new Language([
                    'locale' => $langLocale,
                    'name'   => $langName,
                ]);
                $newLang->save();
            };

            $this->languages = ArrayHelper::map(Language::find()
                ->asArray()
                ->all(), 'locale', 'id');

//        $previousTheme = null;
            foreach (self::DEMO_DATA['themes'] as $theme){
                $parentId = null;

                if ($theme['parent']){
                    $parentTheme = Theme::find()
                        ->joinWith('themeTranslateByLanguage')
                        ->where([
                            ThemeTranslate::tableName() . '.translate' => $theme['parent'],
                        ])
                        ->one();
                    if ($parentTheme){
                        $parentId = $parentTheme->id;
                    }
                }

                $this->newTheme = new Theme([
                    'parent_id' => $parentId,
                ]);
                if ($this->newTheme->save()){
                    // Theme translates
                    foreach ($theme['names'] as $themeNameLocale => $themeName){
                        $this->newTheme->link('themeTranslate', new ThemeTranslate([
                            'language_id' => @$this->languages[$themeNameLocale],
                            'translate'   => $themeName,
                        ]));
                    }

                    foreach ($theme['lessons'] as $lesson){
                        $this->setNewLesson($lesson);
                    }

                }
            }

        } catch (\Exception $tryError){
            $this->error($tryError);
        }


        $this->transaction->commit();
        Console::output('=== FINISH ===');


        $this->setMediaAndImages();
    }

    private function cleaningTables()
    {
        $migrate = new CustomMigration();
        /** @var $migrate CustomMigration */
        Console::output('= START tables cleaning =');

        // отключаем проверку внешних ключей
        $migrate->checkingFK(false);

        // чистим таблицы
        $migrate->truncateTable('repetition');
        $migrate->resetAutoIncrement('repetition');

        $migrate->truncateTable('theme_translate');
        $migrate->resetAutoIncrement('theme_translate');

        $migrate->truncateTable('theme');
        $migrate->resetAutoIncrement('theme');

        $migrate->truncateTable('language');
        $migrate->resetAutoIncrement('language');

        $migrate->truncateTable('word_group');
        $migrate->resetAutoIncrement('word_group');

        $migrate->truncateTable('word');
        $migrate->resetAutoIncrement('word');

        $migrate->truncateTable('lesson_image');
        $migrate->resetAutoIncrement('lesson_image');

        $migrate->truncateTable('lesson_media');
        $migrate->resetAutoIncrement('lesson_media');

        $migrate->truncateTable('lesson_translate');
        $migrate->resetAutoIncrement('lesson_translate');

        $migrate->truncateTable('lesson_word');
        $migrate->resetAutoIncrement('lesson_word');

        $migrate->truncateTable('lesson_word_bad_translate');
        $migrate->resetAutoIncrement('lesson_word_bad_translate');

        $migrate->truncateTable('lesson');
        $migrate->resetAutoIncrement('lesson');

        $migrate->truncateTable('completed_user_lesson');
        $migrate->resetAutoIncrement('completed_user_lesson');

        $migrate->truncateTable('completed_user_theme');
        $migrate->resetAutoIncrement('completed_user_theme');

        $migrate->truncateTable('completed_user_word');
        $migrate->resetAutoIncrement('completed_user_word');

        // включаем проверку внешних ключей
        $migrate->checkingFK(true);

        Console::output('= FINISH tables cleaning =');


        $this->cleanDemoDataFiles();
    }

    private function cleanDemoDataFiles()
    {
        // Удаление файлов демо данных
        Console::output('= START delete demo data files =');

        $uploadDir = realpath(\Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads');

        FileHelper::removeDirectory($uploadDir . DIRECTORY_SEPARATOR . 'images');
        FileHelper::removeDirectory($uploadDir . DIRECTORY_SEPARATOR . 'media');

        Console::output('= FINISH delete demo data files =');
    }

    private function setNewLesson($lesson)
    {
        $this->newLesson = new Lesson([
            'theme_id' => $this->newTheme->id,
        ]);

        if (!$this->newLesson->save()){
            var_dump($this->newLesson->getErrors());
            exit;
        }

        foreach ($lesson['names'] as $lessonTranslateLocale => $lessonTranslate){
            $this->newLesson->link('lessonTranslates', new LessonTranslate([
                'lang_id'   => $this->languages[$lessonTranslateLocale],
                'translate' => $lessonTranslate,
            ]));
        }

        foreach ($lesson['words'] as $wordGroup){
            $this->setNewWords($wordGroup);

            $this->setNewLessonWord();
        }
    }

    private function setNewWords($wordGroup)
    {
        $this->newWordGroup = new WordGroup([
            'name'        => $wordGroup['items'][self::LANG_UK],
            'description' => $wordGroup['description'],
        ]);
        $this->newWordGroup->save();

        foreach ($wordGroup['items'] as $wordLocale => $wordName){
            $newWord = new Word([
                'lang_id'       => $this->languages[$wordLocale],
                'name'          => $wordName,
                'word_group_id' => $this->newWordGroup->id,
            ]);
            $newWord->save();
        }
    }

    private function setNewLessonWord()
    {
        $newLessonWord = new LessonWord([
            'lesson_id'     => $this->newLesson->id,
            'word_group_id' => $this->newWordGroup->id,
        ]);
        $newLessonWord->save();
    }

    /**
     * @param mixed $message
     *
     * @throws Exception
     */
    private function error($message)
    {
        throw new Exception($message);
    }

    private function setMediaAndImages()
    {
        // Копируем картинки и аудио файлы
        $rootDir = \Yii::getAlias('@app');
        $demoDir = realpath($rootDir . DIRECTORY_SEPARATOR . 'data');
        $demoFileName = 'demo.zip';
        $demoFilePath = $demoDir . DIRECTORY_SEPARATOR . $demoFileName;
        $uploadDir = realpath($rootDir . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads');

        if (!file_exists($demoFilePath) || !unzip($demoFilePath, $uploadDir)){
            $this->transaction->rollBack();

            throw new Exception("Bad FILE demo data \"$demoFilePath\"");
        }

        $mediaFiles = $this->getDemoFiles("$rootDir/web/uploads/media/lessons");
        $imageFiles = $this->getDemoFiles("$rootDir/web/uploads/images/lessons");

        $getLessonWord = function ($wordName, $lessonId){
            return LessonWord::find()
                ->joinWith('wordGroup')
                ->where([
                    'AND',
                    [
                        'OR',
                        [
                            '=',
                            WordGroup::tableName() . '.name',
                            $wordName,
                        ],
                        [
                            '=',
                            WordGroup::tableName() . '.description',
                            $wordName,
                        ],
                    ],
                    [
                        '=',
                        LessonWord::tableName() . '.lesson_id',
                        $lessonId,
                    ],
                ])
                ->asArray()
                ->one();
        };

        foreach ($mediaFiles as $lessonId => $mediaFilesNames){
            foreach ($mediaFilesNames as $mediaFilesName){
                $wordName = substr($mediaFilesName, strpos($mediaFilesName, '---') + 3);
                $wordName = substr($wordName, 0, strpos($wordName, '.'));

                $langLocale = substr($mediaFilesName, 0, strpos($mediaFilesName, '---'));

                $languageId = @$this->languages[$langLocale];
                if ( $languageId === null ){
                    $this->transaction->rollBack();

                    throw new \yii\db\Exception("Неправильная локаль '$langLocale'");
                }

                if ($lessonWord = $getLessonWord($wordName, $lessonId)){
                    $newLessonMedia = new LessonMedia([
                        'media'          => $mediaFilesName,
                        'lesson_word_id' => $lessonWord['id'],
                        'language_id'    => $languageId,
                    ]);
                    if (!$newLessonMedia->save()){
                        $this->transaction->rollBack();

                        throw new \yii\db\Exception(implode(PHP_EOL, $newLessonMedia->errors));
                    }
                }
            }
        }
        foreach ($imageFiles as $lessonId => $imageFilesNames){
            foreach ($imageFilesNames as $imageFilesName){
                $wordName = substr($imageFilesName, strpos($imageFilesName, '---') + 3);
                $wordName = substr($wordName, 0, strpos($wordName, '.'));

                $langLocale = substr($imageFilesName, 0, strpos($imageFilesName, '---'));

                $languageId = @$this->languages[$langLocale];
                if ( $languageId === null ){
                    $this->transaction->rollBack();

                    throw new \yii\db\Exception("Неправильная локаль '$langLocale'");
                }

                if ($lessonWord = $getLessonWord($wordName, $lessonId)){
                    $newLessonImage = new LessonImage([
                        'image'          => $imageFilesName,
                        'lesson_word_id' => $lessonWord['id'],
                        'language_id'    => $languageId,
                    ]);
                    if (!$newLessonImage->save()){
                        $this->transaction->rollBack();

                        throw new \yii\db\Exception(implode(PHP_EOL, $newLessonImage->errors));
                    }
                }
            }
        }
    }

    private function getDemoFiles($dir)
    {
        if (!file_exists($dir)){
            throw new Exception("Bad DIR demo data \"$dir\"");
        }
        $files = scandir($dir);

        $array_records = [];
        foreach ($files as $lessonId){
            if (($lessonId !== '..') AND ($lessonId !== '.')){
                $media = scandir($dir . '/' . $lessonId);

                foreach ($media as $file){

                    if (($file !== '..') AND ($file !== '.')){

                        if (PHP_OS === 'WINNT'){
                            $file = mb_convert_encoding($file, 'UTF-8', 'CP1251');
                        }

                        $array_records[$lessonId][] = $file;
                    }
                }
            }
        }

        return $array_records;
    }

    public function actionTest()
    {
        $this->cleanDemoDataFiles();

        $migrate = new CustomMigration();
        $migrate->truncateTable('lesson_image');
        $migrate->resetAutoIncrement('lesson_image');
        $migrate->truncateTable('lesson_media');
        $migrate->resetAutoIncrement('lesson_media');

        $this->setMediaAndImages();
    }

    public function actionClean()
    {
        $this->cleaningTables();
    }
}