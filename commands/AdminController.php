<?php


namespace app\commands;


use yii\console\Controller;

class AdminController extends Controller
{
    public function actionAddUserAdmin()
    {
        $sql = <<<SQL
INSERT INTO `user` (`username`, `email`, `password_hash`, `status`, `auth_key`, `created_at`, `updated_at`, `rating`, `type`) VALUES ('admin', 'admin@starlex.co', '$2y$13\$cVp3jabZerSEObs.HTQ8iu8PrhfGJBBOnVBMZKZHKTLBSFaEXAcHW', 1, 'uthQCSCbTvG-oaX6Ng9ZYhsNcScXBKBg', '1503924264', '1503924264', 0, 0)
SQL;
        \Yii::$app->db->createCommand($sql)
            ->execute();
    }
}