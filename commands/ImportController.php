<?php

namespace app\commands;

use app\components\CustomMigration;
use app\models\Language;
use app\models\Lesson;
use app\models\LessonImage;
use app\models\LessonMedia;
use app\models\LessonTranslate;
use app\models\LessonWord;
use app\models\Theme;
use app\models\ThemeTranslate;
use app\models\Word;
use app\models\WordGroup;
use yii\console\Controller;
use yii\console\Exception;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Class DataController
 *
 * @package app\commands
 */
class ImportController extends Controller
{
    const LANG_RU  = 'ru';
    const LANG_USA = 'en';
    const LANG_UK  = 'uk';

    const LANGUAGES = [
        self::LANG_RU  => 'Русский',
        self::LANG_USA => 'English (USA)',
        self::LANG_UK  => 'English (UK)',
    ];


    /** @var  Transaction */
    private $transaction;
    private $languages;
    /** @var  Theme */
    private $newTheme;
    /** @var  Lesson */
    private $newLesson;
    /** @var  WordGroup */
    private $newWordGroup;

    public function actionImages(){
        $migrate = new CustomMigration();
        /** @var $migrate CustomMigration */
        Console::output('= START tables images cleaning =');

        // отключаем проверку внешних ключей
        $migrate->checkingFK(false);

        $migrate->truncateTable('lesson_image');
        $migrate->resetAutoIncrement('lesson_image');

        // включаем проверку внешних ключей
        $migrate->checkingFK(true);

        Console::output('= FINISH tables images cleaning =');
        $this->setImages();
    }

    public function actionExcel()
    {

        $rootDir = \Yii::getAlias('@app');
        //$dir = realpath($rootDir . DIRECTORY_SEPARATOR . 'data');
        $dir = realpath($rootDir . DIRECTORY_SEPARATOR . 'web'. DIRECTORY_SEPARATOR . 'uploads'. DIRECTORY_SEPARATOR . 'import');
        $fileName = '_import.xls';
        $filePath = $dir . DIRECTORY_SEPARATOR . $fileName;

        $reader = IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(false);
        $spreadsheet = $reader->load($dir . DIRECTORY_SEPARATOR . 'import.xlsx');

        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {

            foreach ($worksheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
                $keys = [];
                $i = 1;
                if($row->getRowIndex() != 1) {
                    foreach ($cellIterator as $key => $cell) {
                        if (in_array($cell->getColumn(),
                                ["A", "B", "C", "D", "E", "F"])) { // Колонки которые будем импортировать
                            $keys['keys'][$worksheet->getCellByColumnAndRow($i++, 1)->getValue()] = $cell->getValue();
                        }
                    }
                    $keys['color'] = $worksheet->getStyle($row->getRowIndex())->getFill()->getStartColor()->getRGB();
                    $data[] = $keys;
                }
            }
        }

        /*$data = \app\components\Excel::widget([
            'mode' => 'import',
            'fileName' => $filePath,
            'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel.
            'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric.
            'getOnlySheet' => 'Лист 1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
        ]);*/

        $count_data = count($data);
        if($count_data>0){
            $themes = array();
            $theme = array();
            $subtheme = array();
            $lessons = array();
            $lesson = array();
            $parent = false;
            $is_theme = false;
            $is_subtheme = false;
            $is_lesson = false;

            $i = 0;
            foreach ($data as $row) {
                $i++;
                //parent null theme
                if (!empty($row["keys"]["Русский"]) && !empty($row["keys"]["English (USA)"]) && !empty($row["keys"]["English (UK)"])) {
                    if ($row["color"] == "FFC000") { // оранжевый: блок (был FF9900)
                        $parent = false;
                        if ($is_subtheme) {
                            $subtheme['lessons'][] = $lesson;
                            $themes[] = $subtheme;
                        }
                        $is_subtheme = false;
                        $is_lesson = false;

                        $theme = [
                            'names' => [
                                self::LANG_RU => $row["keys"]["Русский"],
                                self::LANG_USA => $row["keys"]["English (USA)"],
                                self::LANG_UK => $row["keys"]["English (UK)"],
                            ],
                            'parent' => $parent
                        ];
                        $parent = $row["keys"]["Русский"];
                        $themes[] = $theme;
                    } else if ($row["color"] == "FFFF00") { // желтый: тема
                        if ($is_subtheme) {
                            $subtheme['lessons'][] = $lesson;
                            $themes[] = $subtheme;
                            $is_lesson = false;
                        }
                        $subtheme = [
                            'names' => [
                                self::LANG_RU => $row["keys"]["Русский"],
                                self::LANG_USA => $row["keys"]["English (USA)"],
                                self::LANG_UK => $row["keys"]["English (UK)"],
                            ],
                            'parent' => $parent
                        ];
                        $is_subtheme = true;
                    } else if ($row["color"] == "92D050") { // серый: урок в теме
                        if ($is_lesson) {
                            $subtheme['lessons'][] = $lesson;
                        }
                        $lesson = [
                            'names' => [
                                self::LANG_RU => $row["keys"]["Русский"],
                                self::LANG_USA => $row["keys"]["English (USA)"],
                                self::LANG_UK => $row["keys"]["English (UK)"],
                            ]
                        ];
                        $is_lesson = true;

                    } else if ($row["color"] == "000000") { // белый
                        $lesson['words'][] = [
                            'sync'=>$row["keys"]["ThemeId"],
                            'items' => [
                                self::LANG_RU => [
                                    'article'=> null,
                                    'name' => $row["keys"]["Русский"],
                                ],
                                self::LANG_USA => [
                                    'article'=> $row["keys"]["Article"],
                                    'name' => $row["keys"]["English (USA)"],
                                ],
                                self::LANG_UK => [
                                    'article'=> $row["keys"]["Article"],
                                    'name' => $row["keys"]["English (UK)"],
                                ],
                            ],
                            'description' => null,
                        ];
                    }

                    if ($count_data == $i) {
                        $subtheme['lessons'][] = $lesson;
                        $themes[] = $subtheme;
                    }
                }
            }
        }

        $this->transaction = \Yii::$app->db->beginTransaction();

        $this->cleaningTables();

        try {
            // Языки
            foreach (self::LANGUAGES as $langLocale => $langName){
                $newLang = new Language([
                    'locale' => $langLocale,
                    'name'   => $langName,
                ]);
                $newLang->save();
            };

            $this->languages = ArrayHelper::map(Language::find()
                ->asArray()
                ->all(), 'locale', 'id');

            foreach ($themes as $theme){
                $parentId = null;

                if ($theme['parent']){
                    $parentTheme = Theme::find()
                        ->joinWith('themeTranslateByLanguage')
                        ->where([
                            ThemeTranslate::tableName() . '.translate' => $theme['parent'],
                        ])
                        ->one();
                    if ($parentTheme){
                        $parentId = $parentTheme->id;
                    }
                }

                $this->newTheme = new Theme([
                    'parent_id' => $parentId,
                ]);
                if ($this->newTheme->save()){
                    // Theme translates
                    foreach ($theme['names'] as $themeNameLocale => $themeName){
                        $this->newTheme->link('themeTranslate', new ThemeTranslate([
                            'language_id' => @$this->languages[$themeNameLocale],
                            'translate'   => $themeName,
                        ]));
                    }
                    if(isset($theme['lessons']) && count($theme['lessons'])>0){
                        foreach ($theme['lessons'] as $lesson){
                            $this->setNewLesson($lesson);
                        }
                    }


                }else{
                    var_dump($this->newTheme->errors());
                    die();
                }
            }

        } catch (\Exception $tryError){
            $this->error($tryError);
        }


        $this->transaction->commit();
        Console::output('=== FINISH ===');


        $this->setMediaAndImages();
    }

    public function actionSet()
    {
        $this->transaction = \Yii::$app->db->beginTransaction();

        $this->cleaningTables();

        try {
            // Языки
            foreach (self::LANGUAGES as $langLocale => $langName){
                $newLang = new Language([
                    'locale' => $langLocale,
                    'name'   => $langName,
                ]);
                $newLang->save();
            };

            $this->languages = ArrayHelper::map(Language::find()
                ->asArray()
                ->all(), 'locale', 'id');

//        $previousTheme = null;
            //var_dump(self::DEMO_DATA);
            foreach (self::DEMO_DATA['themes'] as $theme){
                $parentId = null;

                if ($theme['parent']){
                    $parentTheme = Theme::find()
                        ->joinWith('themeTranslateByLanguage')
                        ->where([
                            ThemeTranslate::tableName() . '.translate' => $theme['parent'],
                        ])
                        ->one();
                    if ($parentTheme){
                        $parentId = $parentTheme->id;
                    }
                }

                $this->newTheme = new Theme([
                    'parent_id' => $parentId,
                ]);
                if ($this->newTheme->save()){
                    // Theme translates
                    foreach ($theme['names'] as $themeNameLocale => $themeName){
                        $this->newTheme->link('themeTranslate', new ThemeTranslate([
                            'language_id' => @$this->languages[$themeNameLocale],
                            'translate'   => $themeName,
                        ]));
                    }

                    foreach ($theme['lessons'] as $lesson){
                        $this->setNewLesson($lesson);
                    }

                }else{
                    var_dump($this->newTheme->errors());
                    die();
                }
            }

        } catch (\Exception $tryError){
            $this->error($tryError);
        }


        $this->transaction->commit();
        Console::output('=== FINISH ===');


        $this->setMediaAndImages();
    }

    private function cleaningTables()
    {
        $migrate = new CustomMigration();
        /** @var $migrate CustomMigration */
        Console::output('= START tables cleaning =');

        // отключаем проверку внешних ключей
        $migrate->checkingFK(false);

        // чистим таблицы
        $migrate->truncateTable('repetition');
        $migrate->resetAutoIncrement('repetition');

        $migrate->truncateTable('theme_translate');
        $migrate->resetAutoIncrement('theme_translate');

        $migrate->truncateTable('theme');
        $migrate->resetAutoIncrement('theme');

        $migrate->truncateTable('language');
        $migrate->resetAutoIncrement('language');

        $migrate->truncateTable('word_group');
        $migrate->resetAutoIncrement('word_group');

        $migrate->truncateTable('word');
        $migrate->resetAutoIncrement('word');

        $migrate->truncateTable('lesson_image');
        $migrate->resetAutoIncrement('lesson_image');

        $migrate->truncateTable('lesson_media');
        $migrate->resetAutoIncrement('lesson_media');

        $migrate->truncateTable('lesson_translate');
        $migrate->resetAutoIncrement('lesson_translate');

        $migrate->truncateTable('lesson_word');
        $migrate->resetAutoIncrement('lesson_word');

        $migrate->truncateTable('lesson_word_bad_translate');
        $migrate->resetAutoIncrement('lesson_word_bad_translate');

        $migrate->truncateTable('lesson');
        $migrate->resetAutoIncrement('lesson');

        $migrate->truncateTable('completed_user_lesson');
        $migrate->resetAutoIncrement('completed_user_lesson');

        $migrate->truncateTable('completed_user_theme');
        $migrate->resetAutoIncrement('completed_user_theme');

        $migrate->truncateTable('completed_user_word');
        $migrate->resetAutoIncrement('completed_user_word');

        // включаем проверку внешних ключей
        $migrate->checkingFK(true);

        Console::output('= FINISH tables cleaning =');


        //$this->cleanDemoDataFiles();
    }

    private function cleanDemoDataFiles()
    {
        // Удаление файлов демо данных
        Console::output('= START delete demo data files =');

        $uploadDir = realpath(\Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads');

        FileHelper::removeDirectory($uploadDir . DIRECTORY_SEPARATOR . 'images');
        FileHelper::removeDirectory($uploadDir . DIRECTORY_SEPARATOR . 'media');

        Console::output('= FINISH delete demo data files =');
    }

    private function setNewLesson($lesson)
    {
        $this->newLesson = new Lesson([
            'names' => $lesson['names'],
            'theme_id' => $this->newTheme->id,
        ]);

        if (!$this->newLesson->save()){
            var_dump($this->newLesson->errors);
            exit;
        }

        foreach ($lesson['names'] as $lessonTranslateLocale => $lessonTranslate){
            $this->newLesson->link('lessonTranslates', new LessonTranslate([
                'lang_id'   => $this->languages[$lessonTranslateLocale],
                'translate' => $lessonTranslate,
            ]));
        }

        foreach ($lesson['words'] as $wordGroup){
            $this->setNewWords($wordGroup);

            $this->setNewLessonWord($wordGroup["sync"]);
        }
    }

    private function setNewWords($wordGroup)
    {
        $this->newWordGroup = new WordGroup([
            'name'        => $wordGroup['items'][self::LANG_UK]["name"],
            'description' => $wordGroup['description'],
        ]);
        $this->newWordGroup->save();

        foreach ($wordGroup['items'] as $wordLocale => $wordName){
            $newWord = new Word([
                'lang_id'       => $this->languages[$wordLocale],
                'article'          => $wordName["article"],
                'name'          => $wordName["name"],
                'word_group_id' => $this->newWordGroup->id,
                'is_word_collocation' => false
            ]);
            $newWord->save();
        }
    }

    private function setNewLessonWord($sync)
    {
        $newLessonWord = new LessonWord([
            'lesson_id'     => $this->newLesson->id,
            'word_group_id' => $this->newWordGroup->id,
            'sync_id' => $sync
        ]);
        $newLessonWord->save();
    }

    /**
     * @param mixed $message
     *
     * @throws Exception
     */
    private function error($message)
    {
        throw new Exception($message);
    }

    private function setImages()
    {
        // Копируем картинки и аудио файлы
        $rootDir = \Yii::getAlias('@app');
        $audioDir = realpath($rootDir . DIRECTORY_SEPARATOR . 'web'. DIRECTORY_SEPARATOR . 'uploads'. DIRECTORY_SEPARATOR . 'import');
        $ukFileName = 'images.zip';
        $ukFilePath = $audioDir . DIRECTORY_SEPARATOR . $ukFileName;
        $uploadUkDir = realpath($rootDir . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'words');

        /*if (!file_exists($ukFilePath) || !$this->unzip($ukFilePath, $uploadUkDir)){
            $this->transaction->rollBack();

            throw new Exception("Bad FILE image data \"$ukFilePath\"");
        }*/

        $imageFiles = $this->getDemoImageFiles("$rootDir/web/uploads/images/words/");

        $getLessonWord = function ($sync){
            return LessonWord::find()
                ->where([
                    'sync_id' => ''.$sync.''
                ])
                ->asArray()
                ->one();
        };

        foreach ($imageFiles as $lang => $imageFilesName){
            $sync = substr($imageFilesName, 0, strpos($imageFilesName, '.'));

            if ($lessonWord = $getLessonWord(''.$sync.'')){
                $newLessonImage = new LessonImage([
                    'image'          => $imageFilesName,
                    'lesson_word_id' => $lessonWord['id'],
                ]);

                if (!$newLessonImage->save()){
                    $this->transaction->rollBack();
                    var_dump($newLessonImage->errors);
                    //throw new \yii\db\Exception(implode(PHP_EOL, $newLessonMedia->errors));
                }
            }
        }

    }

    private function setMediaAndImages()
    {
        // Копируем картинки и аудио файлы
        $rootDir = \Yii::getAlias('@app');
        $demoDir = realpath($rootDir . DIRECTORY_SEPARATOR . 'data');
        $audioDir = realpath($rootDir . DIRECTORY_SEPARATOR . 'web'. DIRECTORY_SEPARATOR . 'uploads'. DIRECTORY_SEPARATOR . 'import');
        $demoFileName = 'demo.zip';
        $demoFilePath = $demoDir . DIRECTORY_SEPARATOR . $demoFileName;
        $uploadDir = realpath($rootDir . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads');

       /* if (!file_exists($demoFilePath) || !$this->unzip($demoFilePath, $uploadDir)){
            $this->transaction->rollBack();

            throw new Exception("Bad FILE demo data \"$demoFilePath\"");
        }*/

        $ukFileName = 'audio.zip';
        $ukFilePath = $audioDir . DIRECTORY_SEPARATOR . $ukFileName;
        $uploadUkDir = realpath($rootDir . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads');

        /*if (!file_exists($ukFilePath) || !$this->unzip($ukFilePath, $uploadUkDir)){
            $this->transaction->rollBack();

            throw new Exception("Bad FILE demo data \"$ukFilePath\"");
        }*/

        $mediaFiles = $this->getDemoMediaFiles("$rootDir/web/uploads/media/words/");
        //$imageFiles = $this->getDemoFiles("$rootDir/web/uploads/images/lessons");

        $getLessonWord = function ($sync){
            return LessonWord::find()
                ->where([
                    'sync_id' => ''.$sync.''
                ])
                ->asArray()
                ->one();
        };

        foreach ($mediaFiles as $lang => $mediaFilesNames){
            foreach ($mediaFilesNames as $mediaFilesName){
                //$wordName = substr($mediaFilesName, strpos($mediaFilesName, '---') + 3);
                $sync = substr($mediaFilesName, 0, strpos($mediaFilesName, '.'));

                //$langLocale = substr($mediaFilesName, 0, strpos($mediaFilesName, '---'));

                $languageId = @$this->languages[$lang];
                if ( $languageId === null ){
                    $this->transaction->rollBack();

                    throw new \yii\db\Exception("Неправильная локаль '$lang'");
                }

                if ($lessonWord = $getLessonWord(''.$sync.'')){
                    $newLessonMedia = new LessonMedia([
                        'media'          => $mediaFilesName,
                        'lesson_word_id' => $lessonWord['id'],
                        'language_id'    => $languageId,
                    ]);
                    if (!$newLessonMedia->save()){
                        $this->transaction->rollBack();
                        var_dump($newLessonMedia->errors);
                        //throw new \yii\db\Exception(implode(PHP_EOL, $newLessonMedia->errors));
                    }
                }
            }
        }

        /*foreach ($imageFiles as $lessonId => $imageFilesNames){
            foreach ($imageFilesNames as $imageFilesName){
                $wordName = substr($imageFilesName, strpos($imageFilesName, '---') + 3);
                $wordName = substr($wordName, 0, strpos($wordName, '.'));

                $langLocale = substr($imageFilesName, 0, strpos($imageFilesName, '---'));

                $languageId = @$this->languages[$langLocale];
                if ( $languageId === null ){
                    $this->transaction->rollBack();

                    throw new \yii\db\Exception("Неправильная локаль '$langLocale'");
                }

                if ($lessonWord = $getLessonWord($wordName, $lessonId)){
                    $newLessonImage = new LessonImage([
                        'image'          => $imageFilesName,
                        'lesson_word_id' => $lessonWord['id'],
                        'language_id'    => $languageId,
                    ]);
                    if (!$newLessonImage->save()){
                        $this->transaction->rollBack();

                        throw new \yii\db\Exception(implode(PHP_EOL, $newLessonImage->errors));
                    }
                }
            }
        }*/
    }

    private function unzip($zipfile, $toDir)
    {
        $zip = zip_open($zipfile);
        while ($zip_entry = zip_read($zip)){
            zip_entry_open($zip, $zip_entry);
            if (substr(zip_entry_name($zip_entry), -1) == '/'){
                $zdir = $toDir . DIRECTORY_SEPARATOR . substr(zip_entry_name($zip_entry), 0, -1);
                //var_dump( $zdir );
                if (file_exists($zdir)){
                    //trigger_error('Directory "<b>' . $zdir . '</b>" exists', E_USER_ERROR);

                    continue;
                }
                mkdir($zdir);
            } else {
                $name = $toDir . DIRECTORY_SEPARATOR . zip_entry_name($zip_entry);
                if (PHP_OS === 'WINNT'){
                    $name = mb_convert_encoding($name, 'CP1251', mb_detect_encoding($name));
                }

                if (file_exists($name)){
                    //trigger_error('File "<b>' . $name . '</b>" exists', E_USER_ERROR);

                    continue;
                }
                $fopen = fopen($name, "w");

                fwrite($fopen, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)), zip_entry_filesize($zip_entry));
            }
            zip_entry_close($zip_entry);
        }
        zip_close($zip);

        return true;
    }

    private function getDemoImageFiles($dir)
    {
        if (!file_exists($dir)){
            throw new Exception("Bad DIR demo data \"$dir\"");
        }
        //$files = scandir($dir);

        $array_records = [];
        $media = scandir($dir);

        foreach ($media as $file){

            if (($file !== '..') AND ($file !== '.')){

                if (PHP_OS === 'WINNT'){
                    $file = mb_convert_encoding($file, 'UTF-8', 'CP1251');
                }

                $array_records[] = $file;
            }
        }

        return $array_records;
    }

    private function getDemoMediaFiles($dir)
    {
        if (!file_exists($dir)){
            throw new Exception("Bad DIR demo data \"$dir\"");
        }
        $files = scandir($dir);

        $array_records = [];
        foreach ($files as $lang){
            if (($lang !== '..') AND ($lang !== '.')){
                $media = scandir($dir . '/' . $lang);

                foreach ($media as $file){

                    if (($file !== '..') AND ($file !== '.')){

                        if (PHP_OS === 'WINNT'){
                            $file = mb_convert_encoding($file, 'UTF-8', 'CP1251');
                        }

                        $array_records[$lang][] = $file;
                    }
                }
            }
        }

        return $array_records;
    }

    private function getDemoFiles($dir)
    {
        if (!file_exists($dir)){
            throw new Exception("Bad DIR demo data \"$dir\"");
        }
        $files = scandir($dir);

        $array_records = [];
        foreach ($files as $lessonId){
            if (($lessonId !== '..') AND ($lessonId !== '.')){
                $media = scandir($dir . '/' . $lessonId);

                foreach ($media as $file){

                    if (($file !== '..') AND ($file !== '.')){

                        if (PHP_OS === 'WINNT'){
                            $file = mb_convert_encoding($file, 'UTF-8', 'CP1251');
                        }

                        $array_records[$lessonId][] = $file;
                    }
                }
            }
        }

        return $array_records;
    }

    public function actionTest()
    {
        $this->cleanDemoDataFiles();

        $migrate = new CustomMigration();
        $migrate->truncateTable('lesson_image');
        $migrate->resetAutoIncrement('lesson_image');
        $migrate->truncateTable('lesson_media');
        $migrate->resetAutoIncrement('lesson_media');

        $this->setMediaAndImages();
    }

    public function actionClean()
    {
        $this->cleaningTables();
    }
}