<?php
/**
 * Yii2 Shortcuts
 * @author Eugene Terentev <eugene@terentev.net>
 * -----
 * This file is just an example and a place where you can add your own shortcuts,
 * it doesn't pretend to be a full list of available possibilities
 * -----
 */

/**
 * @return int|string
 */
function getMyId()
{
    return Yii::$app->user->getId();
}

/**
 * @param string $view
 * @param array $params
 * @return string
 */
function render($view, $params = [])
{
    return Yii::$app->controller->render($view, $params);
}

/**
 * @param $url
 * @param int $statusCode
 * @return \yii\web\Response
 */
function redirect($url, $statusCode = 302)
{
    return Yii::$app->controller->redirect($url, $statusCode);
}

/**
 * @param $form \yii\widgets\ActiveForm
 * @param $model
 * @param $attribute
 * @param array $inputOptions
 * @param array $fieldOptions
 * @return string
 */
function activeTextinput($form, $model, $attribute, $inputOptions = [], $fieldOptions = [])
{
    return $form->field($model, $attribute, $fieldOptions)->textInput($inputOptions);
}

/**
 * @param string $key
 * @param mixed $default
 * @return mixed
 */
function env($key, $default = false) {

    $value = getenv($key);

    if ($value === false) {
        return $default;
    }

    switch (strtolower($value)) {
        case 'true':
        case '(true)':
            return true;

        case 'false':
        case '(false)':
            return false;
    }

    return $value;
}

/**
 * Echo with \n
 *
 * @param string $message
 *
 * @return string
 */
function echol( $message ){
	echo $message .= PHP_EOL;

	return $message;
}

function unzip($zipfile, $toDir)
{
    $zip = zip_open($zipfile);
    while ($zip_entry = zip_read($zip)){
        zip_entry_open($zip, $zip_entry);
        if (substr(zip_entry_name($zip_entry), -1) == '/'){
            $zdir = $toDir . DIRECTORY_SEPARATOR . substr(zip_entry_name($zip_entry), 0, -1);
//                var_dump( $zdir );
            if (file_exists($zdir)){
                trigger_error('Directory "<b>' . $zdir . '</b>" exists', E_USER_ERROR);

                return false;
            }
            mkdir($zdir);
        } else {
            $name = $toDir . DIRECTORY_SEPARATOR . zip_entry_name($zip_entry);
            if (PHP_OS === 'WINNT'){
                $name = mb_convert_encoding($name, 'CP1251', mb_detect_encoding($name));
            }

            if (file_exists($name)){
                trigger_error('File "<b>' . $name . '</b>" exists', E_USER_ERROR);

                return false;
            }
            $fopen = fopen($name, "w");

            fwrite($fopen, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)), zip_entry_filesize($zip_entry));
        }
        zip_entry_close($zip_entry);
    }
    zip_close($zip);

    return true;
}