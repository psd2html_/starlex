<?php


namespace app\helpers;


class FlashHelper
{
    const TYPE_ALERT = 'alert';
//    const TYPE_ERROR  = 'error';
//    const TYPE_DANGER = 'danger';
//    const TYPE_INFO   = 'info';

    public static function setAlert($message)
    {
        \Yii::$app->session->setFlash(self::TYPE_ALERT, [
            'body'    => $message,
            'options' => [
                'class' => 'alert-danger',
            ],
        ]);

    }

//    public static function setError($message)
//    {
//        \Yii::$app->session->setFlash(self::TYPE_ERROR, $message);
//    }

    public static function getAlert()
    {
        return \Yii::$app->session->getFlash(self::TYPE_ALERT);
    }

//    public static function getError()
//    {
//        return \Yii::$app->session->getFlash(self::TYPE_ERROR);
//    }
}