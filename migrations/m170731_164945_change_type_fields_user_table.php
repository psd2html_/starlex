<?php

use yii\db\Migration;

class m170731_164945_change_type_fields_user_table extends Migration
{
    public $_table = '{{%user}}';

    public function up()
    {
        $this->alterColumn($this->_table, 'username', $this->string()->null());
        $this->alterColumn($this->_table, 'email', $this->string()->null());
        $this->alterColumn($this->_table, 'password_hash', $this->string()->null());
    }

    public function down()
    {
        $this->alterColumn($this->_table, 'username', $this->string()->notNull());
        $this->alterColumn($this->_table, 'email', $this->string()->notNull());
        $this->alterColumn($this->_table, 'password_hash', $this->string()->notNull());
    }
}
