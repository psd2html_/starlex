<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170816_150707_add_col_to_user_table
 */
class m170816_150707_add_col_to_user_table extends Migration
{
    const TABLE_NAME = 'user';
    const COL_NAME = 'type';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->integer()->notNull()->defaultValue(0)->comment('Тип: обычный пользователь, админ, менеджер'));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
