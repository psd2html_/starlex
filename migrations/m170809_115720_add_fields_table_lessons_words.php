<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170809_115720_add_fields_table_lessons_words
 */
class m170809_115720_add_fields_table_lessons_words extends Migration
{
    const TABLE_NAME = 'lessons_words';
    const COL_NAME_1   = 'name';
    const COL_NAME_2   = 'type';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME_1, $this->string());
        $this->addColumn(self::TABLE_NAME, self::COL_NAME_2, $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME_1);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME_2);
    }
}

