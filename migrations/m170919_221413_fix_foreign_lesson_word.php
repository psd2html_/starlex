<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170919_221413_fix_foreign_lesson_word
 */
class m170919_221413_fix_foreign_lesson_word extends Migration
{
    public function safeUp()
    {
//        $this->checkingFK(false);
//        try {
            // Table "lesson_word"
            $tableName = 'lesson_word';
            $this->delete($tableName);
            $this->dropFK($tableName, 'word_id');
            $this->renameColumn($tableName, 'word_id', 'word_translate_id');
            $this->addFK($tableName, 'word_translate_id', 'word_translate', 'id', self::CASCADE, self::CASCADE);

            // Table "lesson_word_bad_translate"
            $tableName = 'lesson_word_bad_translate';
            $this->delete($tableName);
            $this->dropFK($tableName, 'translate_id');
            $this->renameColumn($tableName, 'translate_id', 'bad_translate_id');
            $this->addFK($tableName, 'bad_translate_id', 'word', 'id', self::CASCADE, self::CASCADE);
//        } catch (\Exception $tryError){
//            $this->checkingFK(true);
//
//            return false;
//        }
    }

    public function safeDown()
    {
//        $this->checkingFK(false);
//        try {
            // Table "lesson_word"
            $tableName = 'lesson_word';
            $this->delete($tableName);
            $this->dropFK($tableName, 'word_translate_id');
            $this->renameColumn($tableName, 'word_translate_id', 'word_id');
            $this->addFK($tableName, 'word_id', 'word', 'id', self::CASCADE, self::CASCADE);

            // Table "lesson_word_bad_translate"
            $tableName = 'lesson_word_bad_translate';
            $this->delete($tableName);
            $this->dropFK($tableName, 'bad_translate_id');
            $this->renameColumn($tableName, 'bad_translate_id', 'translate_id');
            $this->addFK($tableName, 'translate_id', 'word', 'id', self::CASCADE, self::CASCADE);
//        } catch (\Exception $tryError){
//            $this->checkingFK(true);
//
//            return false;
//        }
    }
}
