<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170730_220851_create_table_game_user_data
 */
class m170730_220851_create_table_game_user_data extends Migration
{
    const TABLE_NAME = 'game_user_data';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'game_id'    => $this->integer()->notNull(),
            'user_id'    => $this->integer()->notNull(),
            'data'       => $this->text()->comment('Данные игры в JSON'),
            'rating'     => $this->float()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'game_id', 'game', 'id');
        $this->addFK(self::TABLE_NAME, 'user_id', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
