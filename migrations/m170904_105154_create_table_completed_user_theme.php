<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170904_105154_create_table_completed_user_theme
 */
class m170904_105154_create_table_completed_user_theme extends Migration
{
    const TABLE_NAME = 'completed_user_theme';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()
                ->notNull(),
            'theme_id'   => $this->integer()
                ->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'user_id', 'user', 'id', self::CASCADE, self::CASCADE);
        $this->addFK(self::TABLE_NAME, 'theme_id', 'theme', 'id', self::CASCADE, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
