<?php

use app\components\CustomMigration as Migration;
use app\modules\user\models\User;

/**
 * Class m170828_113806_add_user_admin
 */
class m170828_113806_add_user_admin extends Migration
{
    const TABLE_NAME = 'user';

    public function up()
    {
        $query = <<<SQL
SELECT id FROM user WHERE username='admin';
SQL;
        $userIsset = Yii::$app->db->createCommand($query)
            ->queryScalar();

        if (!$userIsset){
            $this->insert(self::TABLE_NAME, [
                'username'             => 'admin',
                'firstname'            => null,
                'lastname'             => null,
                'email'                => 'admin@starlex.co',
                'password_hash'        => '$2y$13$cVp3jabZerSEObs.HTQ8iu8PrhfGJBBOnVBMZKZHKTLBSFaEXAcHW', // admin!admin
                'status'               => 1,
                'auth_key'             => 'uthQCSCbTvG-oaX6Ng9ZYhsNcScXBKBg',
                'email_confirm_token'  => null,
                'password_reset_token' => null,
                'created_at'           => time(),
                'updated_at'           => time(),
                'rating'               => 0,
                'type'                 => User::TYPE_ADMIN,
            ]);
        }
    }

    public function down()
    {

    }
}
