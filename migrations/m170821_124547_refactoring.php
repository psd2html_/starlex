<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170821_124547_refactoring
 */
class m170821_124547_refactoring extends Migration
{
    public function safeUp()
    {
        try {
            // word
            $this->createTable('word', [
                'id'          => $this->primaryKey(),
                'lang_id'     => $this->integer()
                    ->notNull(),
                'name'        => $this->string()
                    ->notNull(),
                'description' => $this->text(),
                'status'      => $this->integer(1)
                    ->notNull()
                    ->defaultValue(1),
                'created_at'  => $this->integer(),
                'updated_at'  => $this->integer(),
            ]);
            $this->addFK('word', 'lang_id', 'language', 'id', self::CASCADE, self::CASCADE);


            // word_translate
            $this->createTable('word_translate', [
                'id'           => $this->primaryKey(),
                'word_id'      => $this->integer()
                    ->notNull(),
                'translate_id' => $this->integer()
                    ->notNull(),
                'order'        => $this->float(1)
                    ->defaultValue(1),
            ]);
            $this->addFK('word_translate', 'word_id', 'word', 'id', self::CASCADE, self::CASCADE);
            $this->addFK('word_translate', 'translate_id', 'word', 'id', self::CASCADE, self::CASCADE);

            // Drop tables
            $dropTables = [
                'lessons',
                'lessons_images',
                'lessons_language',
                'lessons_media',
                'lessons_words',
                'lessons_words_language',
                'tariffs',
                'tariffs_order',
                'themes',
                'themes_language',
            ];
            $this->checkingFK(false);
            foreach ($dropTables as $dropTableName){
                $this->dropTable($dropTableName);
            }

            // Create tables
            $this->createTable('theme', [
                'id'         => $this->primaryKey(),
                'parent_id'  => $this->integer(),
                'name'       => $this->string()
                    ->notNull(),
                'status'     => $this->integer(1)
                    ->notNull()
                    ->defaultValue(1),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
            $this->addFK('theme', 'parent_id', 'theme', 'id', self::CASCADE, self::CASCADE);

            $this->createTable('theme_language', [
                'id'       => $this->primaryKey(),
                'theme_id' => $this->integer()
                    ->notNull(),
                'lang_id'  => $this->integer()
                    ->notNull(),
                'name'     => $this->string(),
            ]);
            $this->addFK('theme_language', 'theme_id', 'theme', 'id', self::CASCADE, self::CASCADE);
            $this->addFK('theme_language', 'lang_id', 'language', 'id', self::CASCADE, self::CASCADE);

            ///////////////////
            $this->createTable('lesson', [
                'id'           => $this->primaryKey(),
                'name'         => $this->string()
                    ->notNull(),
                'theme_id'     => $this->integer()
                    ->notNull(),
                'status'       => $this->integer(1)
                    ->notNull()
                    ->defaultValue(1),
                'time_passage' => $this->integer()
                    ->notNull()
                    ->defaultValue(0),
                'lives_count'  => $this->integer(5)
                    ->defaultValue(0)
                    ->notNull(),
                'points_count'  => $this->integer(5)
                    ->defaultValue(0)
                    ->notNull(),
                'created_at'   => $this->integer(),
                'updated_at'   => $this->integer(),
            ]);
            $this->addFK('lesson', 'theme_id', 'theme', 'id', self::CASCADE, self::CASCADE);

            $this->createTable('lesson_translate', [
                'id'        => $this->primaryKey(),
                'lesson_id' => $this->integer()
                    ->notNull(),
                'lang_id'   => $this->integer()
                    ->notNull(),
                'translate' => $this->string()
                    ->notNull(),
            ]);
            $this->addFK('lesson_translate', 'lesson_id', 'lesson', 'id', self::CASCADE, self::CASCADE);
            $this->addFK('lesson_translate', 'lang_id', 'language', 'id', self::CASCADE, self::CASCADE);

            $this->createTable('lesson_word', [
                'id'           => $this->primaryKey(),
                'lesson_id'    => $this->integer()
                    ->notNull(),
                'word_id'      => $this->integer()
                    ->notNull(),
                'created_at'   => $this->integer(),
                'updated_at'   => $this->integer(),
            ]);
            $this->addFK('lesson_word', 'lesson_id', 'lesson', 'id', self::CASCADE, self::CASCADE);
            $this->addFK('lesson_word', 'word_id', 'word', 'id', self::CASCADE, self::CASCADE);

            $this->createTable('lesson_word_bad_translate', [
                'id'             => $this->primaryKey(),
                'lesson_word_id' => $this->integer()
                    ->notNull(),
                'translate_id'   => $this->integer()
                    ->notNull(),
                'created_at'     => $this->integer(),
                'updated_at'     => $this->integer(),
            ]);
            $this->addFK('lesson_word_bad_translate', 'lesson_word_id', 'lesson_word', 'id', self::CASCADE, self::CASCADE);
            $this->addFK('lesson_word_bad_translate', 'translate_id', 'word', 'id', self::CASCADE, self::CASCADE);

            $this->createTable('lesson_image', [
                'id'             => $this->primaryKey(),
                'lesson_word_id' => $this->integer()
                    ->notNull(),
                'image'          => $this->string()
                    ->notNull(),
                'created_at'     => $this->integer(),
                'updated_at'     => $this->integer(),
            ]);
            $this->addFK('lesson_image', 'lesson_word_id', 'lesson_word', 'id', self::CASCADE, self::CASCADE);

            $this->createTable('lesson_media', [
                'id'             => $this->primaryKey(),
                'lesson_word_id' => $this->integer()
                    ->notNull(),
                'media'          => $this->string()
                    ->notNull(),
                'created_at'     => $this->integer(),
                'updated_at'     => $this->integer(),
            ]);
            $this->addFK('lesson_media', 'lesson_word_id', 'lesson_word', 'id', self::CASCADE, self::CASCADE);

            /////////////////////
            $this->createTable('tariff', [
                'id'          => $this->primaryKey(),
                'name'        => $this->string()
                    ->notNull(),
                'days_active' => $this->integer(),
                'sum'         => $this->double()->unsigned()->notNull(),
                'created_at'  => $this->integer(),
                'updated_at'  => $this->integer(),
            ]);

            $this->createTable('tariff_order', [
                'id'         => $this->primaryKey(),
                'user_id'    => $this->integer()
                    ->notNull(),
                'tariff_id'  => $this->integer()
                    ->notNull(),
                'sum'        => $this->double()
                    ->notNull()->unsigned(),
                'created_at' => $this->integer(),
            ]);
            $this->addFK('tariff_order', 'user_id', 'user', 'id', self::CASCADE, self::CASCADE);
            $this->addFK('tariff_order', 'tariff_id', 'tariff', 'id', self::CASCADE, self::CASCADE);

            $this->dropFK('award', 'tariff_id');
            $this->addFK('award', 'tariff_id', 'tariff', 'id', self::CASCADE, self::CASCADE);
        } catch (\Exception $tryError){
            echo $tryError . PHP_EOL;
        }

        $this->checkingFK(true);
    }

    public function safeDown()
    {

    }
}
