<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170804_101557_add_table_lessons_media
 */
class m170804_101557_add_table_lessons_media extends Migration
{
	const TABLE_NAME = 'lessons_media';

	public function safeUp()
	{
        $this->createTable(self::TABLE_NAME, [
            'id'                => $this->primaryKey(),
            'lesson_id'         => $this->integer()->notNull(),
            'lang_id'           => $this->integer()->notNull(),
            'media'             => $this->string()->notNull(),
            'type'              => $this->boolean()->notNull()->defaultValue(0)->comment('0-не верный вариант;1-правильный вариант'),
            'created_at'        => $this->integer(),
            'updated_at'        => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'lesson_id', 'lessons', 'id');
        $this->addFK(self::TABLE_NAME, 'lang_id', 'language', 'id');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE_NAME);
	}
}
