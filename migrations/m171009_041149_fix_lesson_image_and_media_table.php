<?php

use app\components\CustomMigration as Migration;

/**
 * Class m171009_041149_fix_lesson_image_and_media_table
 */
class m171009_041149_fix_lesson_image_and_media_table extends Migration
{
    public function safeUp()
    {
        $this->checkingFK(false);
        $this->truncateTable('lesson_image');
        $this->resetAutoIncrement('lesson_image');
        $this->truncateTable('lesson_media');
        $this->resetAutoIncrement('lesson_media');
        $this->checkingFK(true);

        $this->addColumn('lesson_image', 'language_id', $this->integer()
            ->notNull());
        $this->addColumn('lesson_media', 'language_id', $this->integer()
            ->notNull());

        $this->addFK('lesson_image', 'language_id', 'language', 'id', self::CASCADE, self::CASCADE);
        $this->addFK('lesson_media', 'language_id', 'language', 'id', self::CASCADE, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropFK('lesson_image', 'language_id');
        $this->dropFK('lesson_media', 'language_id');

        $this->dropColumn('lesson_image', 'language_id');
        $this->dropColumn('lesson_media', 'language_id');
    }
}
