<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170921_171105_fix_table_theme_language
 */
class m170921_171105_fix_table_theme_language extends Migration
{
    const TABLE_OLD_NAME = 'theme_language';
    const TABLE_NEW_NAME = 'theme_translate';

    public function safeUp()
    {
        $this->dropTable(self::TABLE_OLD_NAME);
        $this->createTable(self::TABLE_NEW_NAME, [
            'id'          => $this->primaryKey(),
            'theme_id'    => $this->integer()
                ->notNull(),
            'language_id' => $this->integer()
                ->notNull(),
            'translate'   => $this->string()
                ->notNull(),
        ]);
        $this->addFK(self::TABLE_NEW_NAME, 'theme_id', 'theme', 'id', self::CASCADE, self::CASCADE);
        $this->addFK(self::TABLE_NEW_NAME, 'language_id', 'language', 'id', self::CASCADE, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NEW_NAME);
        $this->createTable(self::TABLE_OLD_NAME, [
            'id'       => $this->primaryKey(),
            'theme_id' => $this->integer()
                ->notNull(),
            'lang_id'  => $this->integer()
                ->notNull(),
            'name'     => $this->string()
                ->notNull(),
        ]);
        $this->addFK(self::TABLE_OLD_NAME, 'theme_id', 'theme', 'id', self::CASCADE, self::CASCADE);
        $this->addFK(self::TABLE_OLD_NAME, 'lang_id', 'language', 'id', self::CASCADE, self::CASCADE);
    }
}
