<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170904_105403_create_table_completed_user_lesson
 */
class m170904_105403_create_table_completed_user_lesson extends Migration
{
    const TABLE_NAME = 'completed_user_lesson';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()
                ->notNull(),
            'lesson_id'  => $this->integer()
                ->notNull(),
            'stars_total' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'user_id', 'user', 'id', self::CASCADE, self::CASCADE);
        $this->addFK(self::TABLE_NAME, 'lesson_id', 'lesson', 'id', self::CASCADE, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
