<?php

use yii\db\Migration;

class m170727_140224_add_table_themes_language extends Migration
{
    public $_table = '{{%themes_language}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->_table, [
            'id'        => $this->primaryKey(),
            'lang_id'   => $this->integer()->notNull(), // FK
            'theme_id'  => $this->integer()->notNull(), // FK
            'name'      => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-lang_id', $this->_table, 'lang_id');
        $this->createIndex('idx-theme_id', $this->_table, 'theme_id');


        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-lang_id',
            $this->_table,
            'lang_id',
            'language',
            'id'
        );
        $this->addForeignKey(
            'fk-theme_id',
            $this->_table,
            'theme_id',
            'themes',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable($this->_table);
    }
}
