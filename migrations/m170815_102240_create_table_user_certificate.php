<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170815_102240_create_table_user_certificate
 */
class m170815_102240_create_table_user_certificate extends Migration
{
    const TABLE_NAME = 'user_certificate';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'                  => $this->primaryKey(),
            'user_id'             => $this->integer()
                ->notNull(),
            'certificate_id'      => $this->integer(),
            'count_parts_collect' => $this->integer()
                ->notNull()
                ->defaultValue(0),
            'created_at'          => $this->integer(),
            'updated_at'          => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'user_id', 'user', 'id', self::CASCADE, self::CASCADE);
        $this->addFK(self::TABLE_NAME, 'certificate_id', 'certificate', 'id', self::SET_NULL, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
