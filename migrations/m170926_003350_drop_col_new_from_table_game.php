<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170926_003350_drop_col_new_from_table_game
 */
class m170926_003350_drop_col_new_from_table_game extends Migration
{
    const TABLE_NAME = 'game';
    const COL_NAME = 'name';

    public function safeUp()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->string()->notNull());
    }
}
