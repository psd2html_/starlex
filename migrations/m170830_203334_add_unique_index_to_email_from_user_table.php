<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170830_203334_add_unique_index_to_email_from_user_table
 */
class m170830_203334_add_unique_index_to_email_from_user_table extends Migration
{
    public function safeUp()
    {
        $this->truncateTable('user_social_account');

        $clearSql = <<<SQL
ALTER TABLE user_social_account DROP FOREIGN KEY `fk-social-account-user_id`;
DROP INDEX `idx-social-account-user_id` ON user_social_account;
SQL;
        $this->execute($clearSql);

        $this->addFK('user_social_account', 'user_id', 'user', 'id', self::CASCADE, self::CASCADE);

        // Drop not unique records
        $sql = <<<SQL
DELETE 
FROM user
WHERE (id NOT IN (SELECT id
                 FROM (SELECT
                         email,
                         id
                       FROM user
                       GROUP BY user.email) AS id)) OR (email = '');
SQL;

        $this->execute($sql);

        $this->execute('DROP INDEX `idx-user-username` ON user;');
        $this->createIndex('user', 'username', null, true);

        $this->execute('DROP INDEX `idx-user-email` ON user;');
        $this->createIndex('user', 'email', null, true);

        $this->execute('DROP INDEX `idx-user-status` ON user;');
        $this->createIndex('user', 'status');
    }

    public function safeDown()
    {
        $sql = <<<SQL
DROP INDEX `idx_user_email` ON user;
CREATE INDEX `idx-user-email` ON user (email);
SQL;
        $this->execute($sql);

        /////////////

        $sql = <<<SQL
DROP INDEX `idx_user_username` ON user;
CREATE INDEX `idx-user-username` ON user (email);
SQL;
        $this->execute($sql);
        /////////////

        $sql = <<<SQL
DROP INDEX `idx_user_status` ON user;
CREATE INDEX `idx-user-status` ON user (email);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
ALTER TABLE user_social_account DROP FOREIGN KEY `fk_user_social_account_user_id`;
DROP INDEX `fk_user_social_account_user_id` ON user_social_account;
SQL;
        $this->execute($sql);

        $this->createIndex('user_social_account', 'user_id', 'idx-social-account-user_id');
        $this->addForeignKey(
            'fk-social-account-user_id',
            '{{%user_social_account}}',
            'user_id',
            'user',
            'id'
        );
    }
}
