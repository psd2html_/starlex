<?php

use yii\db\Migration;

class m170727_121243_add_table_tariffs extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tariffs}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
            'date_to'       => $this->integer()->null()->comment('До какого времени действует'),
            'sum'           => $this->double()->null()->comment('Цена за тариф'),
            'created_at'    => $this->integer()->notNull()->unsigned(),
            'updated_at'    => $this->integer()->null()->unsigned(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%tariffs}}');
    }
}