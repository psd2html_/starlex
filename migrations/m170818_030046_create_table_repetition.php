<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170818_030046_create_table_repetition
 */
class m170818_030046_create_table_repetition extends Migration
{
    const TABLE_NAME = 'repetition';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'            => $this->primaryKey(),
            'word_id'       => $this->integer()
                ->notNull(),
            'status'        => $this->integer(1)
                ->notNull()
                ->defaultValue(1)
                ->comment('1-Enable, 0-Disable, 2-Remove'),
            'count_success' => $this->integer(1)
                ->notNull()
                ->defaultValue(0),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'word_id', 'lessons_words', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
