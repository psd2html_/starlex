<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170730_220414_create_table_game_language
 */
class m170730_220414_create_table_game_translate extends Migration
{
    const TABLE_NAME = 'game_translate';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'          => $this->primaryKey(),
            'game_id'     => $this->integer()->notNull(),
            'language_id' => $this->integer()->notNull(),
            'translate'   => $this->string()->notNull()->comment('Перевод названия'),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'game_id', 'game', 'id');
        $this->addFK(self::TABLE_NAME, 'language_id', 'language', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
