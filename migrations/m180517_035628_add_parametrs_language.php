<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180517_035628_add_parametrs_language
 */
class m180517_035628_add_parametrs_language extends Migration
{
    const TABLE_NAME = 'language';
    const COL_NAME1   = 'is_default';
    const COL_NAME2   = 'is_learn';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME1, $this->integer(1)->notNull()->defaultValue(0)->after('locale'));
        $this->addColumn(self::TABLE_NAME, self::COL_NAME2, $this->integer(1)->notNull()->defaultValue(0)->after(self::COL_NAME1));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME1);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME2);
    }
}
