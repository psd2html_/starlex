<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170809_120852_delete_field_type_table_lessons_words_language
 */
class m170809_120852_delete_field_type_table_lessons_words_language extends Migration
{
	const TABLE_NAME = 'lessons_words_language';

	public function safeUp()
	{
        $this->dropColumn(self::TABLE_NAME, 'type');
	}

	public function safeDown()
	{
        return true;
	}
}
