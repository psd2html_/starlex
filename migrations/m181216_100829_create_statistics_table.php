<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statistics`.
 */
class m181216_100829_create_statistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%statistics}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(), // FK
            'time' => $this->integer()->notNull(),
            'date' => $this->dateTime()->notNull()->defaultExpression('now()'),
            'active_time' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-statistics-user_id', '{{%statistics}}', 'user_id');

        // add foreign key for table `user`
        $this->addForeignKey('fk-statistics-user_id', '{{%statistics}}','user_id','{{%user}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-statistics-user_id', '{{%statistics}}');
        $this->dropForeignKey('fk-statistics-user_id', '{{%statistics}}');

        $this->dropTable('{{%statistics}}');
    }
}
