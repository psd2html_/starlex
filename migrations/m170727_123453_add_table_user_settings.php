<?php

use yii\db\Migration;

class m170727_123453_add_table_user_settings extends Migration
{
    public $_table = '{{%user_settings}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->_table, [
            'id'                    => $this->primaryKey(),
            'user_id'               => $this->integer()->notNull(), // FK
            'tariff_id'             => $this->integer()->null(), // FK
            'native_lang_id'        => $this->integer()->notNull()->defaultValue(1)->comment('Родной язык'), // FK
            'learn_lang_id'         => $this->integer()->notNull()->defaultValue(2)->comment('Изучаемый язык'), // FK
            'target_day'            => $this->boolean()->notNull()->defaultValue(0)->comment('Дневная цель (0-10 минут;1-20 минут;далее уточнить)'),
            'notify_day_task'       => $this->boolean()->notNull()->defaultValue(0)->comment('Напоминать о задании на день(0-выкл;1-вкл)'),
            'notify_words_repeat'   => $this->boolean()->notNull()->defaultValue(0)->comment('Напоминать о словах для повторения(0-выкл;1-вкл)'),
            'test_status'           => $this->boolean()->notNull()->defaultValue(0)->comment('Статус вводного задания(0-не пройдено;1-пройдено)'),
            'rewards'               => $this->integer(5)->notNull()->defaultValue(0)->comment('Кол-во полученных наград'),
            'certificates'          => $this->string()->null()->comment('массив открытых номеров сертификата(1-9)'),
            'rating'                => $this->integer()->null()->comment('Кол-во набранного опыта'),
        ], $tableOptions);

        $this->createIndex('idx-user_id', $this->_table, 'user_id');
        $this->createIndex('idx-tariff_id', $this->_table, 'tariff_id');
        $this->createIndex('idx-native_lang_id', $this->_table, 'native_lang_id');
        $this->createIndex('idx-learn_lang_id', $this->_table, 'learn_lang_id');


        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_id',
            $this->_table,
            'user_id',
            'user',
            'id'
        );
        $this->addForeignKey(
            'fk-tariff_id',
            $this->_table,
            'tariff_id',
            'tariffs',
            'id'
        );
        $this->addForeignKey(
            'fk-native_lang_id',
            $this->_table,
            'native_lang_id',
            'language',
            'id'
        );
        $this->addForeignKey(
            'fk-learn_lang_id',
            $this->_table,
            'learn_lang_id',
            'language',
            'id'
        );
    }


    public function down()
    {
        $this->dropTable($this->_table);
    }
}
