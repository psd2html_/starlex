<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171011_024123_fix_lesson_media_table
 */
class m171011_024123_fix_lesson_media_table extends Migration
{
    const TABLE_NAME = 'lesson_media';
    const COL_NAME = 'lesson_word_id';

    public function safeUp()
    {
        $this->dropFK(self::TABLE_NAME, self::COL_NAME);
        $this->addFK(self::TABLE_NAME, self::COL_NAME, 'lesson_word', 'id', self::CASCADE, self::CASCADE);
    }

    public function safeDown(){}
}
