<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170922_110607_refactor_table_word_translate
 */
class m170922_110607_refactor_table_word_translate extends Migration
{
    public function safeUp()
    {
        $this->delete('lesson_word');
        $this->dropFK('lesson_word', 'word_translate_id');

        $this->dropTable('word_translate');
        $this->createTable('word_group', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()
                ->notNull(),
            'description' => $this->text(),
            'status'      => $this->integer(1),
        ]);

        $this->renameColumn('lesson_word', 'word_translate_id', 'word_group_id');
        $this->addFK('lesson_word', 'word_group_id', 'word_group', 'id', self::CASCADE, self::CASCADE);
    }

    public function safeDown()
    {
        $this->delete('lesson_word');

        $this->dropFK('lesson_word', 'word_group_id');
        $this->dropTable('word_group');

        $this->createTable('word_translate', [
            'id'           => $this->primaryKey(),
            'word_id'      => $this->integer()
                ->notNull(),
            'translate_id' => $this->integer()
                ->notNull(),
            'order'        => $this->float(),
        ]);
        $this->addFK('word_translate', 'word_id', 'word', 'id', self::CASCADE, self::CASCADE);
        $this->addFK('word_translate', 'translate_id', 'word', 'id', self::CASCADE, self::CASCADE);

        $this->renameColumn('lesson_word', 'word_group_id', 'word_translate_id');
        $this->addFK('lesson_word', 'word_translate_id', 'word_translate', 'id', self::CASCADE, self::CASCADE);
    }
}
