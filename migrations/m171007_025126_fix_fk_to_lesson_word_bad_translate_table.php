<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171007_025126_fix_fk_to_lesson_word_bad_translate_table
 */
class m171007_025126_fix_fk_to_lesson_word_bad_translate_table extends Migration
{
	const TABLE_NAME = 'lesson_word_bad_translate';

	public function safeUp()
	{
        $this->dropFK(self::TABLE_NAME, 'bad_translate_id');
        $this->addFK(self::TABLE_NAME, 'bad_translate_id', 'word_group', 'id', self::CASCADE, self::CASCADE);
	}

	public function safeDown()
	{
        $this->dropFK(self::TABLE_NAME, 'bad_translate_id');
        $this->addFK(self::TABLE_NAME, 'bad_translate_id', 'lesson_word', 'id', self::CASCADE, self::CASCADE);
	}
}
