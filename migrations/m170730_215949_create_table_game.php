<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170730_215949_create_table_game
 */
class m170730_215949_create_table_game extends Migration
{
    const TABLE_NAME = 'game';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()->notNull(),
            'banner'     => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
