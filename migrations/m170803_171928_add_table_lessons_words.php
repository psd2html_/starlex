<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170803_171928_add_table_lessons_words
 */
class m170803_171928_add_table_lessons_words extends Migration
{
	const TABLE_NAME = 'lessons_words';

	public function safeUp()
	{
        $this->createTable(self::TABLE_NAME, [
            'id'            => $this->primaryKey(),
            'lesson_id'     => $this->integer()->notNull(),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'lesson_id', 'lessons', 'id');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE_NAME);
	}
}
