<?php
/** @var string $className */

echo "<?php\n";
?>
use <?= app\components\CustomMigration::class; ?> as Migration;

/**
 * Class <?= $className . "\n" ?>
 */
class <?= $className ?> extends Migration
{
//	const TABLE_NAME = '';

	public function safeUp()
	{

	}

	public function safeDown()
	{

	}
}
