<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180531_173749_add_sync_to_lesson_word
 */
class m180531_173749_add_sync_to_lesson_word extends Migration
{
    const TABLE_NAME = 'lesson_word';
    const COL_NAME   = 'sync_id';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
