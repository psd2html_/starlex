<?php

use yii\db\Migration;

class m170726_142711_add_table_themes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%themes}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->notNull()->defaultValue(0),
            'name' => $this->string()->notNull(),
            'status' => $this->boolean()->null()->comment('0-отключена;1-активна'),
            'created_at' => $this->integer()->notNull()->unsigned(),
            'updated_at' => $this->integer()->null()->unsigned(),
        ], $tableOptions);

        $this->createIndex('idx-themes-parent', '{{%themes}}', 'parent_id');
        $this->createIndex('idx-themes-status', '{{%themes}}', 'status');
    }

    public function down()
    {
        $this->dropTable('{{%themes}}');
    }
}
