<?php

use yii\db\Migration;

class m170726_112457_delete_field_access_token extends Migration
{
    public function up()
    {
        $this->dropColumn('{{user}}', 'access_token');
    }

//    public function down()
//    {
//        $this->addColumn('{{user}}', 'access_token', 'string');
//    }
}
