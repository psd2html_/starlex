<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170914_120831_add_unique_index_to_col_name_in_lesson_table
 */
class m170914_120831_add_unique_index_to_col_name_in_lesson_table extends Migration
{
	const TABLE_NAME = 'lesson';
	const COL_NAME = 'name';

	public function safeUp()
	{
        $this->createIndex(self::TABLE_NAME, self::COL_NAME, null, true );
	}

	public function safeDown()
	{
        $this->dropIndex(self::TABLE_NAME, self::COL_NAME);
	}
}
