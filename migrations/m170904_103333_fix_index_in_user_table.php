<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170904_103333_fix_index_in_user_table
 */
class m170904_103333_fix_index_in_user_table extends Migration
{
	public function safeUp()
	{
        $this->dropIndex('user', 'username');
        $this->createIndex('user', 'username', null, false);
	}

	public function safeDown()
	{
        $this->dropIndex('user', 'username');
        $this->createIndex('user', 'username', null, true);
	}
}
