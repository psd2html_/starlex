<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180802_105715_add_position_theme
 */
class m180802_105715_add_position_theme extends Migration
{
    const TABLE_NAME = 'theme';
    const COL_NAME1   = 'pos_row';
    const COL_NAME2   = 'pos_col';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME1, $this->integer()->after('status'));
        $this->addColumn(self::TABLE_NAME, self::COL_NAME2, $this->integer()->after(self::COL_NAME1));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME1);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME2);
    }
}