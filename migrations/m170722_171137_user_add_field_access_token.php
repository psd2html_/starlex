<?php

use yii\db\Migration;

class m170722_171137_user_add_field_access_token extends Migration
{
    public function up()
    {
        $this->addColumn('{{user}}', 'access_token', 'string');
    }

    public function down()
    {
        $this->dropColumn('{{user}}', 'access_token');
    }
}
