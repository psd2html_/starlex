<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170803_160730_add_table_lessons
 */
class m170803_160730_add_table_lessons extends Migration
{
    const TABLE_NAME = 'lessons';

	public function safeUp()
	{
        $this->createTable(self::TABLE_NAME, [
            'id'            => $this->primaryKey(),
            'theme_id'      => $this->integer()->null(),
            'status'        => $this->boolean()->comment('0-выкл;1-вкл'),
            'time_passage'  => $this->integer()->null()->comment('Время прохождения'),
            'lives_count'   => $this->integer(5)->null()->comment('Кол-во жизней'),
            'points_count'  => $this->integer(5)->null()->comment('Ко-во звезд, которые может набрать пользователь'),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'theme_id', 'themes', 'id');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE_NAME);
	}
}
