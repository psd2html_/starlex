<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170802_171155_edit_col_name_to_table_tariffs
 */
class m170802_171155_edit_col_name_to_table_tariffs extends Migration
{
	const TABLE_NAME = 'tariffs';

	public function safeUp()
	{
        $this->renameColumn(self::TABLE_NAME, 'date_to', 'days_active');
        $this->addCommentOnColumn(self::TABLE_NAME, 'days_active', 'Количество дней активности тарифа');
	}

	public function safeDown()
	{
        $this->renameColumn(self::TABLE_NAME, 'days_active', 'date_to');
        $this->addCommentOnColumn(self::TABLE_NAME, 'date_to', 'До какого времени действует');
	}
}
