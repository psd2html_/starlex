<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170803_171029_add_table_lessons_language
 */
class m170803_171029_add_table_lessons_language extends Migration
{
    const TABLE_NAME = 'lessons_language';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'            => $this->primaryKey(),
            'lesson_id'     => $this->integer()->notNull(),
            'lang_id'       => $this->integer()->notNull(),
            'name'          => $this->string()->notNull(),
        ]);

        $this->addFK(self::TABLE_NAME, 'lesson_id', 'lessons', 'id');
        $this->addFK(self::TABLE_NAME, 'lang_id', 'language', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
