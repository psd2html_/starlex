<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170803_172346_add_table_lessons_words_language
 */
class m170803_172346_add_table_lessons_words_language extends Migration
{
	const TABLE_NAME = 'lessons_words_language';

	public function safeUp()
	{
        $this->createTable(self::TABLE_NAME, [
            'id'                => $this->primaryKey(),
            'lessons_words_id'  => $this->integer()->notNull(),
            'lang_id'           => $this->integer()->notNull(),
            'name'              => $this->string()->notNull(),
            'type'              => $this->boolean()->notNull()->defaultValue(0)->comment('0-не верный вариант;1-правильный вариант'),
            'created_at'        => $this->integer(),
            'updated_at'        => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'lessons_words_id', 'lessons_words', 'id');
        $this->addFK(self::TABLE_NAME, 'lang_id', 'language', 'id');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE_NAME);
	}
}
