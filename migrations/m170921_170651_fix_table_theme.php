<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170921_170651_fix_table_theme
 *
 * Тема не должна содержать имени, т.к. названия должны тянуться из таблицы theme_translate
 */
class m170921_170651_fix_table_theme extends Migration
{
	const TABLE_NAME = 'theme';

	public function safeUp()
	{
        $this->dropColumn(self::TABLE_NAME, 'name');
	}

	public function safeDown()
	{
        $this->addColumn(self::TABLE_NAME, 'name', $this->string()->notNull());
	}
}
