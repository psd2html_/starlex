<?php

use yii\db\Migration;

class m170727_113005_add_table_user_social_account extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_social_account}}', [
            'id'            => $this->primaryKey(),
            'user_id'       => $this->integer()->null(), // FK
            'provider'      => $this->string()->notNull(),
            'client_id'     => $this->integer()->null()->comment('Social ID'),
            'data'          => $this->text()->null(),
            'code'          => $this->string()->null(),
            'email'         => $this->string()->null(),
            'username'      => $this->string()->null(),
            'phone'         => $this->string()->null(),
            'avatar'        => $this->string()->null(),
            'social_page'   => $this->string()->null()->comment('Ссылка на профиль юзера'),
            'sex'           => $this->boolean()->null()->comment('1-мужской;2-женский'),
            'birthday'      => $this->string()->null(),
            'created_at'    => $this->integer()->notNull()->unsigned(),
            'updated_at'    => $this->integer()->null()->unsigned(),
        ], $tableOptions);

        $this->createIndex('idx-social-account-user_id', '{{%user_social_account}}', 'user_id');

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-social-account-user_id',
            '{{%user_social_account}}',
            'user_id',
            'user',
            'id'
        );

    }

    public function down()
    {
        $this->dropTable('{{%user_social_account}}');
    }
}
