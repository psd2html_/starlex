<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170815_102804_add_col_to_certificate_table
 */
class m170815_102804_add_col_to_certificate_table extends Migration
{
    const TABLE_NAME = 'certificate';
    const COL_NAME   = 'themes_id';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->integer());
        $this->addFK(self::TABLE_NAME, self::COL_NAME, 'themes', 'id', self::SET_NULL, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropFK(self::TABLE_NAME, self::COL_NAME);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
