<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170815_105118_create_table_user_award
 */
class m170815_105118_create_table_user_award extends Migration
{
    const TABLE_NAME = 'user_award';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()
                ->notNull(),
            'award_id'   => $this->integer()
                ->notNull(),
            'status'     => $this->integer(1)
                ->notNull()
                ->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'user_id', 'user', 'id', self::CASCADE, self::CASCADE);
        $this->addFK(self::TABLE_NAME, 'award_id', 'award', 'id', self::CASCADE, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
