<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170728_104301_create_table_tariffs_order
 */
class m170728_104301_create_table_tariffs_order extends Migration
{
    const TABLE_NAME = 'tariffs_order';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'        => $this->primaryKey(),
            'user_id'   => $this->integer()->notNull(),
            'tariff_id' => $this->integer()->notNull(),
            'sum'       => $this->double()->unsigned()->comment('За сколько было куплено'),
            'create_at' => $this->integer(),
        ], "COMMENT='История приобретения тарифов'");

        $this->addFK(self::TABLE_NAME, 'user_id', 'user', 'id');
        $this->addFK(self::TABLE_NAME, 'tariff_id', 'tariffs', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
