<?php

use app\components\CustomMigration as Migration;

/**
 * Class m180810_140141_add_col_is_word_collocation_to_word_table
 */
class m180810_140141_add_col_is_word_collocation_to_word_table extends Migration
{
    const TABLE_NAME = 'word';
    const COL_NAME   = 'is_word_collocation';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->boolean()
            ->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
