<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180405_061649_add_table_completed_user_lesson
 */
class m180405_061649_add_table_completed_user_lesson extends Migration
{
	const TABLE_NAME = 'completed_user_lesson';
    const COL_NAME_1   = 'points';
    const COL_NAME_2   = 'flag_help';
    const COL_NAME_3   = 'flag_time';
    const COL_NAME_4   = 'answers_count';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME_1, $this->integer());
        $this->addColumn(self::TABLE_NAME, self::COL_NAME_2, $this->integer());
        $this->addColumn(self::TABLE_NAME, self::COL_NAME_3, $this->integer());
        $this->addColumn(self::TABLE_NAME, self::COL_NAME_4, $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME_1);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME_2);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME_3);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME_4);
    }
}
