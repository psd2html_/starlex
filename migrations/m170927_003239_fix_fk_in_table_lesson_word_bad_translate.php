<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170927_003239_fix_fk_in_table_lesson_word_bad_translate
 */
class m170927_003239_fix_fk_in_table_lesson_word_bad_translate extends Migration
{
	const TABLE_NAME = 'lesson_word_bad_translate';

	public function safeUp()
	{
	    $this->truncateTable(self::TABLE_NAME);
        $this->dropFK(self::TABLE_NAME, 'bad_translate_id');
        $this->addFK(self::TABLE_NAME, 'bad_translate_id', 'lesson_word', 'id', self::CASCADE, self::CASCADE);
	}

	public function safeDown()
	{
        $this->truncateTable(self::TABLE_NAME);
        $this->dropFK(self::TABLE_NAME, 'bad_translate_id');
        $this->addFK(self::TABLE_NAME, 'bad_translate_id', 'word', 'id', self::CASCADE, self::CASCADE);
	}
}
