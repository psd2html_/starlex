<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170730_230942_add_col_to_table_language
 */
class m170730_230942_add_col_to_table_language extends Migration
{
    const TABLE_NAME = 'language';
    const COL_NAME   = 'locale';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->string(5));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
