<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170808_100541_add_field_name_table_lessons
 */
class m170808_100541_add_field_name_table_lessons extends Migration
{
    const TABLE_NAME = 'lessons';
    const COL_NAME   = 'name';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
