<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170926_000625_fix_table_word
 */
class m170926_000625_fix_table_word extends Migration
{
    const TABLE_NAME = 'word';
    const COL_NAME = 'word_group_id';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->integer());
        $this->addFK(self::TABLE_NAME, self::COL_NAME, 'word_group', 'id', self::CASCADE, self::CASCADE);
    }

    public function down()
    {
        $this->dropFK(self::TABLE_NAME, self::COL_NAME);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
