<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170904_105630_create_table_completed_user_word
 */
class m170904_105630_create_table_completed_user_word extends Migration
{
    const TABLE_NAME = 'completed_user_word';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()
                ->notNull(),
            'lesson_word_id'  => $this->integer()
                ->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'user_id', 'user', 'id', self::CASCADE, self::CASCADE);
        $this->addFK(self::TABLE_NAME, 'lesson_word_id', 'lesson_word', 'id', self::CASCADE, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
