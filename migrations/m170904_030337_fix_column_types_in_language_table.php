<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170904_030337_fix_column_types_in_language_table
 */
class m170904_030337_fix_column_types_in_language_table extends Migration
{
	public function safeUp()
	{
        $this->alterColumn('language', 'created_at', $this->integer());
        $this->alterColumn('language', 'updated_at', $this->integer());
	}

	public function safeDown()
	{
        $this->alterColumn('language', 'created_at', $this->integer()->unsigned());
        $this->alterColumn('language', 'updated_at', $this->integer()->unsigned());
	}
}
