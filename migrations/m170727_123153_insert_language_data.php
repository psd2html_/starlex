<?php

use yii\db\Migration;

class m170727_123153_insert_language_data extends Migration
{
    public function up()
    {
        $this->insert('{{%language}}', array(
            'id'            => '1',
            'name'          => 'Русский',
            'created_at'    => time(),
            'updated_at'    => time()
        ));

        $this->insert('{{%language}}', array(
            'id'            => '2',
            'name'          => 'Английский',
            'created_at'    => time(),
            'updated_at'    => time()
        ));
    }


    public function down()
    {
        $this->truncateTable('{{%language}}');
    }
}
