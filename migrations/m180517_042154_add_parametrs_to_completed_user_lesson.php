<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180517_042154_add_parametrs_to_completed_user_lesson
 */
class m180517_042154_add_parametrs_to_completed_user_lesson extends Migration
{
    const TABLE_NAME = 'completed_user_lesson';
    const COL_NAME1   = 'lang_id';
    const COL_NAME2   = 'translate_id';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME1, $this->integer()->after('lesson_id'));
        $this->addColumn(self::TABLE_NAME, self::COL_NAME2, $this->integer()->after(self::COL_NAME1));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME1);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME2);
    }
}
