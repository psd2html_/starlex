<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170720_111942_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'firstname' => $this->string()->null(),
            'lastname' => $this->string()->null(),
            'email' => $this->string()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(0)->comment('0-заблокирован;1-активен;2-ожидает активации'),
            'auth_key' => $this->string(32),
            'email_confirm_token' => $this->string(),
            'password_reset_token' => $this->string(),
            'created_at' => $this->integer()->notNull()->unsigned(),
            'updated_at' => $this->integer()->null()->unsigned(),


        ], $tableOptions);

        $this->createIndex('idx-user-username', '{{%user}}', 'username');
        $this->createIndex('idx-user-email', '{{%user}}', 'email');
        $this->createIndex('idx-user-status', '{{%user}}', 'status');
    }


    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
