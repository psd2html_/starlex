<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170811_100947_create_table_award
 */
class m170811_100947_create_table_award extends Migration
{
    const TABLE_NAME = 'award';

    /**
     * Условия для получения достижений (success_json):
     *   - Количество дней  для получения достижения
     *   - Количество опыта для получения достижения
     *   - Количество изученных слов для получения достижения
     *   - Количество пройденных уроков для получения достижения
     *   - Количество закрытых тем для получения достижения
     *   - Позиция в рейтинге для получения достижения
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'              => $this->primaryKey(),
            'attainment_name' => $this->string()
                ->notNull()
                ->comment('Название достижения'),
            'success_json'    => $this->text()
                ->comment('Условия для получения достижений, в формате JSON'),
            'name'            => $this->string()
                ->notNull()
                ->comment('Название награды'),
            'description'     => $this->string()
                ->comment('Описание награды'),
            'img'             => $this->string()
                ->comment('Картинка награды'),
            'logic'           => $this->text()
                ->comment('Логика награды, в формате JSON'),
            'experience'      => $this->integer()
                ->comment('Опыт за достижение'),
            'tariff_id'       => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'tariff_id', 'tariffs', 'id', self::SET_NULL, self::CASCADE);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
