<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170804_105410_add_table_lessons_images
 */
class m170804_105410_add_table_lessons_images extends Migration
{
	const TABLE_NAME = 'lessons_images';

	public function safeUp()
	{
        $this->createTable(self::TABLE_NAME, [
            'id'                => $this->primaryKey(),
            'lesson_id'         => $this->integer()->notNull(),
            'lang_id'           => $this->integer()->notNull(),
            'image'             => $this->string()->notNull(),
            'type'              => $this->boolean()->notNull()->defaultValue(0)->comment('0-не верный вариант;1-правильный вариант'),
            'created_at'        => $this->integer(),
            'updated_at'        => $this->integer(),
        ]);

        $this->addFK(self::TABLE_NAME, 'lesson_id', 'lessons', 'id');
        $this->addFK(self::TABLE_NAME, 'lang_id', 'language', 'id');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE_NAME);
	}
}
