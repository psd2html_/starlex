<?php

use yii\db\Migration;

/**
 * Handles adding lang_id to table `statistics`.
 */
class m190120_212724_add_lang_id_column_to_statistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%statistics}}', 'lang_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%statistics}}', 'lang_id');
    }
}
