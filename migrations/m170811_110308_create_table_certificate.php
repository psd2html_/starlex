<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170811_110308_create_table_certificate
 */
class m170811_110308_create_table_certificate extends Migration
{
    const TABLE_NAME = 'certificate';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()
                ->notNull(),
            'image'       => $this->string()
                ->notNull(),
            'count_parts' => $this->integer()
                ->defaultValue(1)->comment('Количество частей'),
            'logic_parts' => $this->text()->comment('Логика расположения частей'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
