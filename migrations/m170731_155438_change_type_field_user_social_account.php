<?php

use yii\db\Migration;

class m170731_155438_change_type_field_user_social_account extends Migration
{
    public $_table = '{{%user_social_account}}';

    public function up()
    {
        $this->alterColumn($this->_table, 'client_id', $this->string()->null());
    }

    public function down()
    {
        // is old_data_type
        $this->alterColumn($this->_table, 'client_id', $this->int(11)->null());
    }
}
