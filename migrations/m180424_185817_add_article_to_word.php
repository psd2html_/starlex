<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180424_185817_add_article_to_word
 */
class m180424_185817_add_article_to_word extends Migration
{
    const TABLE_NAME = 'word';
    const COL_NAME   = 'article';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
