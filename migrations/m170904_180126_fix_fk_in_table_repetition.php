<?php

use app\components\CustomMigration;
use yii\db\Migration;

/**
 * Class m170904_180126_fix_fk_in_table_repetition
 */
class m170904_180126_fix_fk_in_table_repetition extends CustomMigration
{
    const TABLE_NAME = 'repetition';

    public function safeUp()
    {
        Migration::dropForeignKey('fk_repetition_word_id', self::TABLE_NAME);
        Migration::dropIndex('fk_repetition_word_id', self::TABLE_NAME);

        $this->renameColumn(self::TABLE_NAME, 'word_id', 'lesson_word_id');

        $this->addFK(self::TABLE_NAME, 'lesson_word_id', 'lesson_word', 'id');
    }

    public function safeDown()
    {
        return true;
    }
}
