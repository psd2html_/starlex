<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170926_003000_drop_col_new_from_table_lesson
 */
class m170926_003000_drop_col_new_from_table_lesson extends Migration
{
	const TABLE_NAME = 'lesson';
	const COL_NAME = 'name';

	public function safeUp()
	{
        $this->dropIndex(self::TABLE_NAME,self::COL_NAME);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
	}

	public function safeDown()
	{
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->string()->notNull());
        $this->createIndex(self::TABLE_NAME,self::COL_NAME);
    }
}
