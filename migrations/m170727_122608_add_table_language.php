<?php

use yii\db\Migration;

class m170727_122608_add_table_language extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('language', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()
                ->notNull(),
            'created_at' => $this->integer()
                ->unsigned(),
            'updated_at' => $this->integer()
                ->unsigned(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('language');
    }
}
