<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180424_172639_del_key_lesson_image_language
 */
class m180424_172639_del_key_lesson_image_language extends Migration
{
//	const TABLE_NAME = '';

    const TABLE_NAME = 'lesson_image';
    const COL_NAME = 'language_id';

    public function safeUp()
    {
        $this->dropFK(self::TABLE_NAME, self::COL_NAME);
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COL_NAME);
        $this->addColumn(self::TABLE_NAME, self::COL_NAME, $this->integer());
        //$this->dropColumn(self::TABLE_NAME, self::COL_NAME);
    }
}
