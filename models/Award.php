<?php

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "award".
 *
 * @property integer $id
 * @property string  $attainment_name
 * @property string  $success_json
 * @property string  $name
 * @property string  $description
 * @property string  $img
 * @property string  $logic
 * @property integer $experience
 * @property integer $tariff_id
 *
 * @property Tariff  $tariff
 */
class Award extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'award';
    }

    public static function collection()
    {
        return ArrayHelper::map(self::find()
            ->asArray()
            ->all(), 'id', 'name');
    }

    public function rules()
    {
        return [
            [['attainment_name', 'name'], 'required'],
            [['success_json', 'logic'], 'string'],
            [['experience', 'tariff_id'], 'integer'],
            [['attainment_name', 'name', 'description', 'img'], 'string', 'max' => 255],
            [
                ['tariff_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tariff::class,
                'targetAttribute' => ['tariff_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'attainment_name' => 'Название достижения',
            'success_json'    => 'Условия для получения достижений, в формате JSON',
            'name'            => 'Название награды',
            'description'     => 'Описание награды',
            'img'             => 'Картинка награды',
            'logic'           => 'Логика награды, в формате JSON',
            'experience'      => 'Опыт за достижение',
            'tariff_id'       => 'Tariff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::class, ['id' => 'tariff_id']);
    }
}
