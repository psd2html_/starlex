<?php

namespace app\models;

use app\modules\user\models\UserSettings;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "language".
 *
 * @property integer        $id
 * @property string         $name
 * @property integer        $created_at
 * @property integer        $updated_at
 * @property string         $locale [varchar(5)]
 *
 * @property UserSettings[] $userSettings
 * @property UserSettings[] $userSettings0
 */
class Language extends \yii\db\ActiveRecord
{
    public static $currentLangId;

    public static function tableName()
    {
        return 'language';
    }

    /**
     * @return int
     */
    public static function getCurrentLangId()
    {
        if ( self::$currentLangId === null ){
            return self::$currentLangId = @self::findOne([
                'locale' => \Yii::$app->language
            ])->id;
        } else {
            return self::$currentLangId;
        }
    }

    public static function collection($withSource = true)
    {
        $models = self::find();

        if (!$withSource){
            $models->filterWhere([
                '!=',
                'locale',
                \Yii::$app->language,
            ]);
        }

        $models = $models->asArray()
            ->all();

        return ArrayHelper::map($models, 'id', 'name');
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'locale'], 'required'],
            [['locale'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 255],
            [['is_default', 'is_learn'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Название',
            'locale'     => 'Локаль',
            'is_default' => 'Родной',
            'is_learn' => 'Изучаемый',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettings()
    {
        return $this->hasMany(UserSettings::class, ['learn_lang_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettings0()
    {
        return $this->hasMany(UserSettings::class, ['native_lang_id' => 'id']);
    }


}
