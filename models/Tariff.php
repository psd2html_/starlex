<?php

namespace app\models;

use app\modules\user\models\UserSettings;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tariffs".
 *
 * @property integer        $id
 * @property string         $name
 * @property double         $sum
 * @property integer        $created_at
 * @property integer        $updated_at
 *
 * @property UserSettings[] $userSettings
 * @property int            $days_active [int(11)]  Количество дней активности тарифа
 */
class Tariff extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%tariff}}';
    }

    public static function collection()
    {
        return ArrayHelper::map(self::find()
            ->asArray()
            ->all(), 'id', 'name');
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['days_active'], 'integer'],
            [['sum'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'name'        => 'Название',
            'days_active' => 'Количество дней активности тарифа',
            'sum'         => 'Цена за тариф',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettings()
    {
        return $this->hasMany(UserSettings::class, ['tariff_id' => 'id']);
    }
}
