<?php

namespace app\models;

use app\models\interfaces\Statuses;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "word".
 *
 * @property integer                  $id
 * @property integer                  $lang_id
 * @property string                   $name
 * @property string                   $description
 * @property integer                  $status
 * @property integer                  $created_at
 * @property integer                  $updated_at
 * @property integer                  $word_group_id
 * @property integer                  $is_word_collocation
 *
 * @property LessonWord[]             $lessonWords
 * @property LessonWord[]             $lessonWordsTranslate
 * @property LessonWordBadTranslate[] $lessonWordBadTranslates
 * @property Language                 $lang
 * @property string                   $langName
 * @property WordGroup                $wordGroup
 * @property string                   $article [varchar(255)]
 */
class Word extends ActiveRecord implements Statuses
{
    const COLLOCATION_WORD_TRUE  = 1;
    const COLLOCATION_WORD_FALSE = 0;

    public static function collocationWordStatuses()
    {
        return [
            self::COLLOCATION_WORD_TRUE  => 'Словосочетание',
            self::COLLOCATION_WORD_FALSE => 'Одиночное слово',
        ];
    }

    public static function tableName()
    {
        return 'word';
    }

    public static function collection($condition = [], $status = self::STATUS_ENABLED)
    {
        return ArrayHelper::map(self::find()
            ->filterWhere([
                'status' => $status,
            ])
            ->andFilterWhere($condition)
            ->asArray()
            ->all(), 'id', 'name');
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function rules()
    {
        return [
            [['lang_id', 'name', 'is_word_collocation'], 'required'],
            [['lang_id', 'word_group_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['name', 'article'], 'string', 'max' => 255],
            [
                ['lang_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['lang_id' => 'id'],
            ],
            [
                ['word_group_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WordGroup::class,
                'targetAttribute' => ['word_group_id' => 'id'],
            ],
            ['status', 'in', 'range' => array_keys(self::statuses())],
            ['is_word_collocation', 'boolean'],
            ['is_word_collocation', 'default', 'value' => false],
        ];
    }

    public static function statuses($status = null)
    {
        $statuses = [
            self::STATUS_ENABLED  => 'Активно',
            self::STATUS_DISABLED => 'Не активно',
        ];

        return ($status !== null) ? ($statuses[$status] ?? '') : $statuses;
    }

    public function attributeLabels()
    {
        return [
            'id'                  => 'ID',
            'lang_id'             => 'ID Языка',
            'word_group_id'       => 'Группы слов',
            'lang'                => 'Язык',
            'lang_name'           => 'Язык',
            'name'                => 'Название',
//            'article'             => 'Артикль',
            'description'         => 'Описание',
            'status'              => 'Статус',
            'is_word_collocation' => 'Словосочетание',
            'created_at'          => 'Создано',
            'updated_at'          => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonWordsTranslate()
    {
        return $this->hasMany(LessonWord::class, ['translate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonWords()
    {
        return $this->hasMany(LessonWord::class, ['word_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonWordBadTranslates()
    {
        return $this->hasMany(LessonWordBadTranslate::class, ['translate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::class, ['id' => 'lang_id']);
    }

    public function getLangName()
    {
        return @$this->lang->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWordGroup()
    {
        return $this->hasOne(WordGroup::class, ['id' => 'word_group_id']);
    }
}
