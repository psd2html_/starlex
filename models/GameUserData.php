<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%game_user_data}}".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $user_id
 * @property float   $rating
 * @property string  $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Game    $game
 * @property User    $user
 */
class GameUserData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_user_data}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'user_id', 'rating'], 'required'],
            [['game_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['data'], 'default', 'value' => '{}'],
            [['rating'], 'double'],
            [['rating'], 'default', 'value' => 0],
            [
                ['game_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Game::class,
                'targetAttribute' => ['game_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'game_id'    => Yii::t('app', 'Game ID'),
            'user_id'    => Yii::t('app', 'User ID'),
            'rating'     => Yii::t('app', 'Рейтинг'),
            'data'       => Yii::t('app', 'Данные игры в JSON'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::class, ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
