<?php

namespace app\models\interfaces;

interface Statuses
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    public static function statuses($status = null);
}