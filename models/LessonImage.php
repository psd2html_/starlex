<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lesson_image".
 *
 * @property integer    $id
 * @property integer    $lesson_word_id
 * @property string     $image
 * @property integer    $created_at
 * @property integer    $updated_at
 * @property int        $language_id
 *
 * @property LessonWord $lessonWord
 * @property mixed      $lessonId
 * @property mixed      $lessonName
 * @property mixed      $themeId
 */
class LessonImage extends ActiveRecord
{
    public static function tableName()
    {
        return 'lesson_image';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'ImageBehavior' => [
                'class'           => 'app\behaviors\ImageBehavior',
                'imagePath'       => 'images/words',
                // папка где находятся картинки сущности
                'imageThumbsPath' => 'thumbs/',
                // папка с миниатюрами
                'imageField'      => 'image',
                // поле таблицы где сохраняется картинка
                'subfolderField'  => false,
                // (Если `true` то в параметре subfolder прописывается поле таблицы в качестве подпапки), (Если `false` то в параметре subfolder указывается имя подпапки)
                'subfolder'       => 'lessonId',
                // имя поля или подпапка (например 'subfolder/')
            ],
        ];
    }

    public function rules()
    {
        return [
            [['lesson_word_id'], 'required'],
            [['lesson_word_id', 'language_id', 'created_at', 'updated_at'], 'integer'],
            [
                ['lesson_word_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => LessonWord::class,
                'targetAttribute' => ['lesson_word_id' => 'id'],
            ],
            /*[
                ['language_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['language_id' => 'id'],
            ],*/

            [['image'], 'file', 'extensions' => 'png, jpg, gif'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'lesson_word_id' => 'Word',
            'image'          => 'Image',
            'created_at'     => 'Created At',
            'updated_at'     => 'Updated At',
        ];
    }

    /**
     * @return int
     */
    public function getLessonId()
    {
        return @$this->lessonWord->lesson_id;
    }

    /**
     * @return int
     */
    public function getThemeId()
    {
        return @$this->lessonWord->lesson->theme_id;
    }

    /**
     * @return string
     */
    public function getLessonName()
    {
        return @$this->lessonWord->lesson->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonWord()
    {
        return $this->hasOne(LessonWord::class, ['id' => 'lesson_word_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }*/
}
