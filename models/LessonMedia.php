<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "lesson_media".
 *
 * @property integer    $id
 * @property string     $media
 * @property integer    $created_at
 * @property integer    $updated_at
 * @property integer    $language_id
 * @property integer    $lesson_word_id
 *
 * @property LessonWord $lessonWord
 * @property integer    $lessonId
 * @property string     $lessonName
 * @property string     $languageName
 * @property string     $mediaPath
 * @property Language   $language
 * @property integer    $themeId
 */
class LessonMedia extends ActiveRecord
{
    public static function tableName()
    {
        return 'lesson_media';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'fileBehavior' => [
                'class'          => 'app\behaviors\MediaFileBehavior',
                'filePath'       => 'media/words/uk',
                // папка где находятся картинки сущности
                'fileField'      => 'audio',
                // поле таблицы где сохраняется картинка
                'subfolderField' => false,
                // (Если `true` то в параметре subfolder прописывается поле таблицы в качестве подпапки), (Если `false` то в параметре subfolder указывается имя подпапки)
//                'subfolder'      => 'languageName'
                // имя поля или подпапка (например 'subfolder/')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lesson_word_id', 'language_id'], 'required'],
            [['lesson_word_id', 'language_id', 'created_at', 'updated_at'], 'integer'],
            [
                ['lesson_word_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => LessonWord::class,
                'targetAttribute' => ['lesson_word_id' => 'id'],
            ],
            [
                ['language_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['language_id' => 'id'],
            ],
            [['media'], 'file', 'extensions' => 'mp3, wav, mp4, aac, ac3, m4a, m4r, midi, wave, wma'],
            //['lesson_word_id', 'unique', 'message' => "Для этого слова уже задан файл медиа"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'lesson_word_id' => 'Word',
            'language_id'    => 'Language',
            'media'          => 'Media',
            'created_at'     => 'Created At',
            'updated_at'     => 'Updated At',
        ];
    }

    /**
     * @return int
     */
    public function getLessonId()
    {
        return @$this->lessonWord->lesson_id;
    }

    /**
     * @return int
     */
    public function getThemeId()
    {
        return @$this->lessonWord->lesson->theme_id;
    }

    /**
     * @return string
     */
    public function getLessonName()
    {
        return @$this->lessonWord->lesson->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonWord()
    {
        return $this->hasOne(LessonWord::class, ['id' => 'lesson_word_id']);
    }

    public function getMediaPath()
    {
        return Url::to('/' . $this->getBehavior('fileBehavior')
                ->getBaseFilePath() . '/' . $this->media);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }

    public function getLanguageName()
    {
        return @$this->language->locale;
    }
}
