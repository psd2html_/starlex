<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tariffs_order}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $tariff_id
 * @property double  $sum
 *
 * @property Tariff  $tariff
 * @property User    $user
 * @property int     $created_at [int(11)]
 */
class TariffOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tariff_order}}';
    }

    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at']
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['user_id', 'tariff_id'], 'required'],
            [['user_id', 'tariff_id', 'created_at'], 'integer'],
            [['sum'], 'number'],
            [
                ['tariff_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tariff::class,
                'targetAttribute' => ['tariff_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'user_id'    => Yii::t('app', 'User ID'),
            'tariff_id'  => Yii::t('app', 'Tariff ID'),
            'sum'        => Yii::t('app', 'За сколько было куплено'),
            'created_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::class, ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getUserName()
    {
        return $this->user->username;
    }
    public function getTariffName()
    {
        return $this->tariff->name;
    }
}
