<?php

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "certificate".
 *
 * @property integer             $id
 * @property string              $name
 * @property string              $image
 * @property integer             $count_parts
 * @property string              $logic_parts
 * @property integer             $themes_id
 *
 * @property \yii\db\ActiveQuery $themes
 */
class Certificate extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'certificate';
    }

    public static function collection()
    {
        return ArrayHelper::map(self::find()
            ->asArray()
            ->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'image'], 'required'],
            [['count_parts', 'themes_id'], 'integer'],
            [['logic_parts'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
            [
                ['themes_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Theme::class,
                'targetAttribute' => ['themes_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'name'        => 'Название',
            'image'       => 'Картинка',
            'count_parts' => 'Количество частей',
            'logic_parts' => 'Логика расположения частей',
            'themes_id'   => 'Тема',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThemes()
    {
        return $this->hasOne(Theme::class, ['id' => 'themes_id']);
    }
}
