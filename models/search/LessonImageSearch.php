<?php

namespace app\models\search;

use app\models\LessonImage;
use app\models\LessonWord;
use app\models\Word;
use app\models\WordGroup;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class LessonImageSearch
 *
 * @package app\models\search
 */
class LessonImageSearch extends LessonImage
{
    public function rules()
    {
        return [
            [['id', 'language_id', 'created_at', 'updated_at'], 'integer'],
            [['image', 'lesson_word_id'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LessonImage::find()
            ->with([
                //'language',
                'lessonWord',
                'lessonWord.lesson',
                'lessonWord.word',
            ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'             => $this->id,
            //'lesson_word_id' => $this->lesson_word_id,
            'language_id'    => $this->language_id,
            'lessonId'       => $this->lessonId,
            'image'          => $this->image,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
        ]);

        $query->leftJoin(LessonWord::tableName(), self::tableName() . '.lesson_word_id=' . LessonWord::tableName() . '.id')
            ->leftJoin(WordGroup::tableName(), LessonWord::tableName() . '.word_group_id=' . WordGroup::tableName() . '.id')
            ->leftJoin(Word::tableName(), WordGroup::tableName() . '.id=' . Word::tableName() . '.word_group_id')
            ->andFilterWhere(['word.name' => $this->lesson_word_id]);

//        $query->andFilterWhere(['like', 'image', $this->image])
//            ->andFilterWhere(['like', 'lesson_word_id', $this->lesson_word_id]);

        return $dataProvider;
    }
}
