<?php

namespace app\models\search;

use app\models\LessonWord;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LessonWordSearch represents the model behind the search form about `app\models\LessonWord`.
 */
class LessonWordSearch extends LessonWord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'themeId',
                    'lesson_id',
                    'word_group_id',
                    'created_at',
                    'updated_at',
                ],
                'integer',
            ],
            ['word_group_name', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LessonWord::find()
            ->joinWith(['theme', 'lesson', 'wordGroup', 'word']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            self::tableName() . '.id'            => $this->id,
            self::tableName() . '.lesson_id'     => $this->lesson_id,
            self::tableName() . '.word_group_id' => $this->word_group_id,
            'themeId'                            => $this->themeId,
        ])
            ->andFilterWhere(['like', 'word_group.name', $this->word_group_name]);

        return $dataProvider;
    }
}
