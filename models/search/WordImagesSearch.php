<?php

namespace app\models\search;

use app\models\LessonImage;
use app\models\LessonWord;
use app\models\WordGroup;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Word represents the model behind the search form about `app\models\Word`.
 */
class WordImagesSearch extends WordGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['description', 'name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->leftJoin(LessonWord::tableName(), self::tableName() . '.id=' . LessonWord::tableName() . '.word_group_id')->leftJoin(LessonImage::tableName(), LessonWord::tableName() . '.id=' . LessonImage::tableName() . '.lesson_word_id')->where([LessonImage::tableName() . '.id' => null]);
           // ->joinWith(['theme', 'lesson', 'wordGroup', 'word']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'description', $this->description]);


        return $dataProvider;
    }
}
