<?php

namespace app\models\search;

use app\models\TariffOrder;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TariffsOrderSearch represents the model behind the search form about `app\models\TariffOrder`.
 */
class TariffOrderSearch extends TariffOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'tariff_id', 'created_at'], 'integer'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TariffOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'        => $this->id,
            'user_id'   => $this->user_id,
            'tariff_id' => $this->tariff_id,
            'sum'       => $this->sum,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
