<?php

namespace app\models\search;

use app\models\GameTranslate;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GameTranslateSearch represents the model behind the search form about `app\models\GameTranslate`.
 */
class GameTranslateSearch extends GameTranslate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'game_id', 'language_id', 'created_at', 'updated_at'], 'integer'],
            [['translate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GameTranslate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'          => $this->id,
            'game_id'     => $this->game_id,
            'language_id' => $this->language_id,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'translate', $this->translate]);

        return $dataProvider;
    }
}
