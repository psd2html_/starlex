<?php

namespace app\models\search;

use app\models\WordGroup;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * WordTranslateSearch represents the model behind the search form about `app\models\WordTranslate`.
 */
class WordGroupSearch extends WordGroup
{
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['description', 'name'], 'string'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WordGroup::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()){
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'description', $this->description]);

        if (isset($params['find-duplicate'])){
            $this->findDuplicate($query);
        }

        return $dataProvider;
    }

    /**
     * @param $query \yii\db\ActiveQuery
     */
    private function findDuplicate($query)
    {
        $subQuery = WordGroup::find()
            ->select('name')
            ->groupBy(['name'])
            ->having(new Expression('COUNT(name) > 1'));

        $query->andWhere(['IN', 'word_group.name', $subQuery]);
    }
}
