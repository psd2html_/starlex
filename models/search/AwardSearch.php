<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Award;

/**
 * AwardSearch represents the model behind the search form about `app\models\Award`.
 */
class AwardSearch extends Award
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'experience', 'tariff_id'], 'integer'],
            [['attainment_name', 'success_json', 'name', 'description', 'img', 'logic'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Award::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'experience' => $this->experience,
            'tariff_id' => $this->tariff_id,
        ]);

        $query->andFilterWhere(['like', 'attainment_name', $this->attainment_name])
            ->andFilterWhere(['like', 'success_json', $this->success_json])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'logic', $this->logic]);

        return $dataProvider;
    }
}
