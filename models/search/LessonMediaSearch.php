<?php

namespace app\models\search;

use app\models\LessonMedia;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class LessonMediaSearch
 *
 * @package app\models\search
 */
class LessonMediaSearch extends LessonMedia
{
    public function rules()
    {
        return [
            [['id', 'lesson_word_id', 'language_id', 'created_at', 'updated_at'], 'integer'],
            [['media'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LessonMedia::find()
            ->with([
                'language',
                'lessonWord',
                'lessonWord.lesson',
                'lessonWord.word',
            ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'             => $this->id,
            'lesson_word_id' => $this->lesson_word_id,
            'language_id'    => $this->language_id,
            'lessonId'       => $this->lessonId,
            'media'          => $this->media,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
        ]);

//        $query->andFilterWhere(['like', 'media', $this->media])
//            ->andFilterWhere(['like', 'lesson_word_id', $this->lesson_word_id]);

        return $dataProvider;
    }
}
