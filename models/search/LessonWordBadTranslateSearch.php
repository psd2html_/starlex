<?php

namespace app\models\search;

use app\models\LessonWordBadTranslate;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LessonWordBadTranslateSearch represents the model behind the search form about `app\models\LessonWordBadTranslate`.
 */
class LessonWordBadTranslateSearch extends LessonWordBadTranslate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lesson_word_id', 'bad_translate_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LessonWordBadTranslate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'               => $this->id,
            'lesson_word_id'   => $this->lesson_word_id,
            'bad_translate_id' => $this->bad_translate_id,
            'created_at'       => $this->created_at,
            'updated_at'       => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
