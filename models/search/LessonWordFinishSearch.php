<?php

namespace app\models\search;

use app\models\LessonWord;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LessonWordFinishSearch represents the model behind the search form about `app\models\LessonWord`.
 */
class LessonWordFinishSearch extends LessonWord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lesson_id', 'word_group_id', 'created_at', 'updated_at'], 'integer'],
            [['sync_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LessonWord::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->with([
            'lesson.lessonTranslate',
            'lessonMedia',
            'lessonImages.lessonWord',
            'wordGroup.translateByCurrentLanguage',
        ]);

        $this->load($params);

        if (!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'            => $this->id,
            'lesson_id'     => $this->lesson_id,
            'word_group_id' => $this->word_group_id,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'sync_id', $this->sync_id]);

        return $dataProvider;
    }
}
