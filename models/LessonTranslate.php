<?php

namespace app\models;

/**
 * This is the model class for table "lesson_translate".
 *
 * @property integer  $id
 * @property integer  $lesson_id
 * @property integer  $lang_id
 * @property string   $translate
 *
 * @property Language $language
 * @property Lesson   $lesson
 */
class LessonTranslate extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'lesson_translate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lesson_id', 'lang_id', 'translate'], 'required'],
            [['lesson_id', 'lang_id'], 'integer'],
            [['lesson_id', 'lang_id'], 'unique', 'targetAttribute' => ['lesson_id', 'lang_id']],
            [['translate'], 'string', 'max' => 255],
            [
                ['lang_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['lang_id' => 'id'],
            ],
            [
                ['lesson_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Lesson::class,
                'targetAttribute' => ['lesson_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'lesson_id' => 'Lesson ID',
            'lang_id'   => 'Lang ID',
            'translate' => 'Translate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::class, ['id' => 'lesson_id']);
    }
}
