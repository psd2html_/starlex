<?php

namespace app\models;

use app\modules\user\models\User;

/**
 * This is the model class for table "completed_user_theme".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $theme_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Theme   $theme
 * @property User    $user
 */
class CompletedUserTheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'completed_user_theme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'theme_id'], 'required'],
            [['user_id', 'theme_id', 'created_at', 'updated_at'], 'integer'],
            [
                ['theme_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Theme::class,
                'targetAttribute' => ['theme_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'user_id'    => 'User ID',
            'theme_id'   => 'Theme ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::class, ['id' => 'theme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
