<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "completed_user_lesson".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $lesson_id
 * @property integer $stars_total
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Lesson  $lesson
 * @property User    $user
 */
class CompletedUserLesson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'completed_user_lesson';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stars_total', 'points', 'lang_id', 'translate_id', 'flag_help', 'flag_time', 'answers_count', 'lesson_id'], 'required'],
            [['user_id', 'lesson_id', 'lang_id', 'translate_id', 'stars_total', 'points', 'flag_help', 'flag_time', 'answers_count', 'created_at', 'updated_at'], 'integer'],
            [['user_id'], 'default', 'value'=> \Yii::$app->user->identity->getId()],
            [
                ['lesson_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Lesson::class,
                'targetAttribute' => ['lesson_id' => 'id'],
            ],
            [
                ['lang_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['lang_id' => 'id'],
            ],
            [
                ['translate_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['translate_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'user_id'     => 'User ID',
            'lesson_id'   => 'Lesson ID',
            'lang_id'   => 'Lang ID',
            'translate_id'   => 'Translate Lang ID',
            'stars_total' => 'Stars Total',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
            'points' => 'Exp',
            'flag_help' => 'Flag help',
            'flag_time' => 'Flag time',
            'answers_count' => 'Answer count'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::class, ['id' => 'lesson_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
