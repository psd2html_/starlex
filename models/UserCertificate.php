<?php

namespace app\models;

use app\modules\user\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_certificate".
 *
 * @property integer     $id
 * @property integer     $user_id
 * @property integer     $certificate_id
 * @property integer     $count_parts_collect
 * @property integer     $created_at
 * @property integer     $updated_at
 *
 * @property Certificate $certificate
 * @property User        $user
 */
class UserCertificate extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_certificate';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'certificate_id', 'count_parts_collect', 'created_at', 'updated_at'], 'integer'],
            [
                ['certificate_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Certificate::class,
                'targetAttribute' => ['certificate_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
            ['count_parts_collect', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                  => 'ID',
            'user_id'             => 'User ID',
            'certificate_id'      => 'Certificate ID',
            'count_parts_collect' => 'Count Parts Collect',
            'created_at'          => 'Created At',
            'updated_at'          => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificate()
    {
        return $this->hasOne(Certificate::class, ['id' => 'certificate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
