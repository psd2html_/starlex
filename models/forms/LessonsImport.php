<?php

namespace app\models\forms;

use ZipArchive;

/**
 * Class LessonsImportForm
 *
 * @package app\models
 *
 * @property string $fileUrl
 */
trait LessonsImport
{
    /**
     * @return string
     */
    public function getFileUrl()
    {
        return 'uploads/temp/' . $this->archiveFile->baseName . '.' . $this->archiveFile->extension;
    }

    /**
     * @return bool
     */
    public function upload()
    {
        if ($this->validate() && $this->archiveFile) {

            if ( $this->archiveFile->saveAs($this->getFileUrl()) ){
                $this->importFromFile($this->getFileUrl());
            }

            return true;
        }

        return false;
    }

    public function importAudio($fileName)
    {

    }

    public function importImages($fileName)
    {

    }

    private function importFromFile($fileName)
    {
        $zip = new ZipArchive;
        if ($zip->open($fileName) === TRUE) {
            $zip->extractTo($fileName.'_');
            $zip->close();
            echo 'ok';
        } else {
            echo 'ошибка';
        }
    }
}
