<?php

namespace app\models;

use app\behaviors\LessonRelations;
use app\models\interfaces\Statuses;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lesson".
 *
 * @property integer           $id
 * @property integer           $theme_id
 * @property string            $name
 * @property integer           $status
 * @property integer           $time_passage
 * @property integer           $lives_count
 * @property integer           $points_count
 * @property integer           $created_at
 * @property integer           $updated_at
 *
 * @property mixed             $lang
 * @property mixed             $names
 * @property Theme             $theme
 * @property LessonTranslate   $lessonTranslate
 * @property LessonTranslate[] $lessonTranslates
 *
 * @property LessonWord[]      $lessonWords
 * @property WordGroup[]       $wordGroupsRelation
 */
class Lesson extends ActiveRecord implements Statuses
{
//    use \app\models\forms\LessonsImport;

    public $archiveFile;
    public $names;

    // Relations for save
    public $wordGroups;
//    public $words;
//    public $lessonTranslates;
//    public $lessonWords;

    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * @param integer $status
     *
     * @return array|null|string
     * @throws Exception
     */
    public static function statuses($status = null)
    {
        $statuses = [
            self::STATUS_ENABLED  => 'Активно',
            self::STATUS_DISABLED => 'Не активно',
        ];

        return ($status !== null) ? ($statuses[$status] ?? '') : $statuses;
    }

    /**
     * Список названий (для фильтра)
     *
     * @param array $condition
     *
     * @return array
     */
    public static function collection($condition = [])
    {
        $lessons = self::find()
            ->with('lessonTranslate')
            ->andFilterWhere($condition)
            ->asArray()
            ->all();

        return ArrayHelper::map($lessons, 'id', 'lessonTranslate.translate');
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            LessonRelations::class,
        ];
    }

    public function rules()
    {
        return [
            [
                [
                    'theme_id',
                    'status',
                    'time_passage',
                    'lives_count',
                    'points_count',
                    'created_at',
                    'updated_at',
                ],
                'integer',
            ],
            [
                [
                    'wordGroups',
                ],
                'safe',
            ],
            ['names', 'validateNames'],
            [['theme_id'], 'required'],
            [['time_passage', 'lives_count', 'points_count'], 'default', 'value' => 0],
            ['status', 'default', 'value' => self::STATUS_ENABLED],
            [
                ['theme_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Theme::class,
                'targetAttribute' => ['theme_id' => 'id'],
            ],
            [['archiveFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'zip, gzip'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'name'         => 'Name',
            'theme_id'     => 'Theme',
            'status'       => 'Status',
            'time_passage' => 'time_passage',
            'lives_count'  => 'lives_count',
            'points_count' => 'points_count',
            'created_at'   => 'Created At',
            'updated_at'   => 'Updated At',
            'archiveFile'  => 'Импорт файлов уроков из архива',
        ];
    }

    public function getLang()
    {
        return $this->hasMany(LessonTranslate::class, ['lesson_id' => 'id'])
            ->inverseOf('lesson');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::class, ['id' => 'theme_id']);
    }

    public function getLessonTranslate()
    {
        return $this->hasOne(LessonTranslate::class, ['lesson_id' => 'id'])
            ->where(['lang_id' => Language::getCurrentLangId()]);
    }

    public function getLessonTranslates()
    {
        return $this->hasMany(LessonTranslate::class, ['lesson_id' => 'id']);
    }

    public function getName()
    {
        return @$this->lessonTranslate->translate;
    }

    public function getLessonWords()
    {
        return $this->hasMany(LessonWord::class, ['lesson_id' => 'id']);
    }

    public function getWordGroupsRelation()
    {
        return $this->hasMany(WordGroup::class, ['id' => 'word_group_id'])
            ->via('lessonWords');
    }

    /**
     * @return mixed
     */
    public function getNames()
    {
        if (!$this->names){
            $this->names = LessonTranslate::find()
                ->where(['lesson_id' => $this->id])
                ->with('language')
                ->all();
        }

        return $this->names;
    }

    public function validateNames()
    {
        foreach ($this->names as $name){
            if ($name instanceof LessonTranslate){
                $this->addError('names', 'Неверный атрибут "names"');
            }
        }
    }
}
