<?php

namespace app\models;

use app\modules\user\models\User;

/**
 * This is the model class for table "completed_user_word".
 *
 * @property integer    $id
 * @property integer    $user_id
 * @property integer    $lesson_word_id
 * @property integer    $created_at
 * @property integer    $updated_at
 *
 * @property LessonWord $lessonWord
 * @property User       $user
 */
class CompletedUserWord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'completed_user_word';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'lesson_word_id'], 'required'],
            [['user_id', 'lesson_word_id', 'created_at', 'updated_at'], 'integer'],
            [
                ['lesson_word_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => LessonWord::class,
                'targetAttribute' => ['lesson_word_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'user_id'        => 'User ID',
            'lesson_word_id' => 'Lesson Word ID',
            'created_at'     => 'Created At',
            'updated_at'     => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonWord()
    {
        return $this->hasOne(LessonWord::class, ['id' => 'lesson_word_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
