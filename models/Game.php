<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%game}}".
 *
 * @property integer         $id
 * @property string          $name
 * @property string          $banner
 * @property integer         $created_at
 * @property integer         $updated_at
 *
 * @property GameTranslate[] $gameTranslates
 * @property GameUserData[]  $gameUserDatas
 * @property GameTranslate   $gameTranslatesByCurrentLang
 * @property mixed           $names
 */
class Game extends \yii\db\ActiveRecord
{
    public $names;

    public static function tableName()
    {
        return 'game';
    }

    public static function collection()
    {
        return ArrayHelper::map(self::find()
            ->joinWith('gameTranslatesName')
            ->asArray()
            ->all(), 'id', 'gameTranslatesName.translate');
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner'], 'default', 'value' => ''],
            [['created_at', 'updated_at'], 'integer'],
            [['banner'], 'string', 'max' => 255],
            ['names', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'name'       => Yii::t('app', 'Name'),
            'banner'     => Yii::t('app', 'Banner'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return @$this->gameTranslatesByCurrentLang->translate;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameTranslates()
    {
        return $this->hasMany(GameTranslate::class, ['game_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameTranslatesName()
    {
        return $this->hasOne(GameTranslate::class, ['game_id' => 'id'])
            ->where([
                GameTranslate::tableName().'.language_id' => Language::getCurrentLangId()
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameTranslatesByCurrentLang()
    {
        return $this->hasMany(GameTranslate::class, ['game_id' => 'id'])
            ->andWhere([
                GameTranslate::tableName() . '.language_id' => Language::getCurrentLangId(),
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameUserDatas()
    {
        return $this->hasMany(GameUserData::class, ['game_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function getNames()
    {
        if (!$this->names){
            $this->names = LessonTranslate::find()
                ->where(['lesson_id' => $this->id])
                ->with('lang')
                ->all();
        }

        return $this->names;
    }
}
