<?php

namespace app\models;

use app\models\interfaces\Statuses;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "themes".
 *
 * @property integer                              $id
 * @property integer                              $parent_id
 * @property string                               $name
 * @property integer                              $status
 * @property integer                              $created_at
 * @property integer                              $updated_at
 * @property mixed                                $lang
 *
 * @property \yii\db\ActiveQuery|ThemeTranslate[] $themeTranslate
 * @property \yii\db\ActiveQuery|ThemeTranslate   $themeTranslateByLanguage
 * @property \yii\db\ActiveQuery|Theme            $parent
 */
class Theme extends \yii\db\ActiveRecord implements Statuses
{
    // массив входящих языков с переводами
    public $languages = [];

    /**
     * Get status list or alias by status
     *
     * @param bool|integer $getAlias
     * @param bool|string  $prompt
     *
     * @return string|array
     */
    public static function statuses($getAlias = false, $prompt = false)
    {
        $items = [
            self::STATUS_ENABLED  => 'Активно',
            self::STATUS_DISABLED => 'Выключено',
        ];

        if ($getAlias !== false){
            return $items[$getAlias] ?? '';
        } else {
            $prompt = ($prompt !== false)
                ? []
                : [
                    '' => (is_string($prompt) ? $prompt : 'Выберите элемент'),
                ];

            return $prompt + $items;
        }
    }

    public static function collection($condition = ['status' => self::STATUS_ENABLED])
    {
        $themes = self::find()
            ->addSelect([
                self::tableName() . '.id',
                'name' => 'T.translate',
            ])
            ->joinWith('themeTranslateByLanguage T')
            ->filterWhere($condition)
            ->asArray()
            ->all();

        return ArrayHelper::map($themes, 'id', 'name');
    }

    public static function collectionOneLevel()
    {
        $themes = self::find()
            ->addSelect([
                self::tableName() . '.id',
                'name' => 'T.translate',
            ])
            ->andWhere(['parent_id' => null])
            ->joinWith('themeTranslateByLanguage T')
            ->asArray()
            ->all();

        return ArrayHelper::map($themes, 'id', 'name');
    }

    public static function collectionParent()
    {
        $themes = self::find()
            ->addSelect([
                self::tableName() . '.id',
                'name' => 'T.translate',
            ])
            ->andWhere(['not', ['parent_id' => null]])
            ->joinWith('themeTranslateByLanguage T')
            ->asArray()
            ->all();

        return ArrayHelper::map($themes, 'id', 'name');
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theme';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'pos_row', 'pos_col'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'parent_id'  => 'Parent ID',
            'name'       => 'Name',
            'status'     => 'Status',
            'pos_row'     => 'Position y',
            'pos_col'     => 'Position x',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            't_name'     => 'Theme',
            'l_name'     => 'Translate',
        ];
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if (empty($this->parent_id)){
                $this->parent_id = null;
            }

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThemeTranslate()
    {
        return $this->hasMany(ThemeTranslate::class, ['theme_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThemeTranslateByLanguage()
    {
        return $this->hasOne(ThemeTranslate::class, ['theme_id' => 'id'])
            ->where([
                'language_id' => Language::getCurrentLangId(),
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasMany(ThemeTranslate::class, ['theme_id' => 'id'])
            ->inverseOf('theme');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::class, ['id' => 'parent_id']);
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return @$this->themeTranslateByLanguage->translate;
    }
}
