<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "lesson_word_bad_translate".
 *
 * @property integer    $id
 * @property integer    $lesson_word_id
 * @property integer    $bad_translate_id
 * @property integer    $created_at
 * @property integer    $updated_at
 *
 * @property LessonWord $lessonWord
 * @property WordGroup  $wordGroup
 * @property WordGroup  $badTranslate
 */
class LessonWordBadTranslate extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'lesson_word_bad_translate';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function rules()
    {
        return [
            [['lesson_word_id', 'bad_translate_id'], 'required'],
            [['lesson_word_id', 'bad_translate_id', 'created_at', 'updated_at'], 'integer'],
            [
                ['lesson_word_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => LessonWord::class,
                'targetAttribute' => ['lesson_word_id' => 'id'],
            ],
            [
                ['bad_translate_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WordGroup::class,
                'targetAttribute' => ['bad_translate_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'lesson_word_id'   => 'Слово',
            'bad_translate_id' => 'Перевод',
            'created_at'       => 'Создано',
            'updated_at'       => 'Обновлено',
        ];
    }

    public function getWordGroup()
    {
        return $this->hasOne(WordGroup::class, ['word_group_id' => 'lesson_word_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonWord()
    {
        return $this->hasOne(LessonWord::class, ['id' => 'lesson_word_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBadTranslate()
    {
        return $this->hasOne(WordGroup::class, ['id' => 'bad_translate_id']);
    }
}
