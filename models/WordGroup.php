<?php

namespace app\models;

use app\models\interfaces\Statuses;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "word_group".
 *
 * @property integer $id
 * @property string  $name
 * @property integer $status
 * @property string  $description
 *
 * @property Word[]  $words
 * @property Word[]  $translateByCurrentLanguage
 * @property Word    $currentTranslate
 */
class WordGroup extends \yii\db\ActiveRecord implements Statuses
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'word_group';
    }

    public static function collection(array $condition = ['status' => self::STATUS_ENABLED])
    {
        return ArrayHelper::map(self::find()
            ->filterWhere($condition)
            ->asArray()
            ->all(), 'id', 'name');
    }

    public function rules()
    {
        return [
            [['status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ENABLED],
            ['status', 'in', 'range' => array_keys(self::statuses())],
            [['name', 'description'], 'string'],
            [['name'], 'required'],
        ];
    }

    /**
     * @param int|null $status
     *
     * @return array|mixed|string
     */
    public static function statuses($status = null)
    {
        $statuses = [
            self::STATUS_ENABLED  => 'Активно',
            self::STATUS_DISABLED => 'Не активно',
        ];

        return ($status !== null) ? ($statuses[$status] ?? '') : $statuses;
    }

    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'name'        => 'Название на языке по-умолчанию',
            'status'      => 'Статус',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWords()
    {
        return $this->hasMany(Word::class, ['word_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslateByCurrentLanguage()
    {
        return $this->getWords()
            ->andWhere([Word::tableName() . '.lang_id' => Language::getCurrentLangId()]);
    }

    /**
     * @return Word
     */
    public function getCurrentTranslate()
    {
        return current($this->translateByCurrentLanguage);
    }
}
