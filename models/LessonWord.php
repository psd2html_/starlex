<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lesson_word".
 *
 * @property integer                  $id
 * @property integer                  $lesson_id
 * @property integer                  $created_at
 * @property integer                  $updated_at
 * @property int                      $word_group_id
 * @property string                   $sync_id [varchar(255)]
 *
 * @property Theme                    $theme
 * @property LessonImage[]            $lessonImages
 * @property LessonMedia[]            $lessonMedia
 * @property Lesson                   $lesson
 * @property Word                     $word
 * @property Word[]                   $words
 * @property WordGroup                $wordGroup
 * @property \yii\db\ActiveQuery      $lessonWordBadTranslate
 * @property LessonWordBadTranslate[] $lessonWordBadTranslates
 */
class LessonWord extends \yii\db\ActiveRecord
{
    public $lessonLangId;

    public $themeId;
    public $word_group_name;


    public static function tableName()
    {
        return 'lesson_word';
    }

    public static function collection($condition = [])
    {
        return ArrayHelper::map(self::find()
            ->joinWith('word')
            ->andFilterWhere($condition)
            ->asArray()
            ->all(), 'id', 'word.name');
    }

    public static function collectionUsa($condition = [])
    {
        return ArrayHelper::map(self::find()
            ->joinWith('word')
            ->andFilterWhere($condition)
            ->asArray()
            ->all(), 'id', 'wordGroup.name');
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function rules()
    {
        return [
            [['lesson_id', 'word_group_id'], 'required'],
            [['lesson_id', 'word_group_id'], 'unique', 'targetAttribute' => ['lesson_id', 'word_group_id']],
            [['lesson_id', 'word_group_id', 'themeId', 'created_at', 'updated_at', 'lessonLangId'], 'integer'],
            [['sync_id'], 'string', 'max' => 255],
            [
                ['lesson_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Lesson::class,
                'targetAttribute' => ['lesson_id' => 'id'],
            ],
            [
                ['word_group_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WordGroup::class,
                'targetAttribute' => ['word_group_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'lesson_id'       => 'Лекция',
            'themeId'         => 'Тема',
            'word_group_id'   => 'Слово ID',
            'created_at'      => 'Создано',
            'updated_at'      => 'Обновлено',
            'lessonLangId'    => 'Язык лекции',
            'word_group_name' => 'Слово',
            'sync_id'         => 'ID для выгрузки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonImages()
    {
        return $this->hasMany(LessonImage::class, ['lesson_word_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonMedia()
    {
        return $this->hasMany(LessonMedia::class, ['lesson_word_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::class, ['id' => 'lesson_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWordGroup()
    {
        return $this->hasOne(WordGroup::class, ['id' => 'word_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Word::class, ['word_group_id' => 'id'])
            ->via('wordGroup');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWords()
    {
        return $this->hasMany(Word::class, ['word_group_id' => 'id'])
            ->via('wordGroup');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonWordBadTranslate()
    {
        return $this->hasOne(LessonWordBadTranslate::class, ['lesson_word_id' => 'id'])
            ->via('wordGroup');
    }

    public function getLessonWordBadTranslates()
    {
        return $this->hasMany(LessonWordBadTranslate::class, ['lesson_word_id' => 'id']);
    }

    public function getTheme()
    {
        return $this->hasOne(Theme::class, ['id' => 'theme_id'])
            ->via('lesson');
    }
}
