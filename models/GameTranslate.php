<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%game_translate}}".
 *
 * @property integer  $id
 * @property integer  $game_id
 * @property integer  $language_id
 * @property string   $translate
 * @property integer  $created_at
 * @property integer  $updated_at
 *
 * @property Game     $game
 * @property Language $language
 */
class GameTranslate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_translate}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'language_id', 'translate'], 'required'],
            [['game_id', 'language_id', 'created_at', 'updated_at'], 'integer'],
            [['translate'], 'string', 'max' => 255],
            [
                ['game_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Game::class,
                'targetAttribute' => ['game_id' => 'id'],
            ],
            [
                ['language_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['language_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'game_id'     => Yii::t('app', 'Game ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'translate'   => Yii::t('app', 'Перевод названия'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::class, ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }
}
