<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "repetition".
 *
 * @property integer    $id
 * @property integer    $lesson_word_id
 * @property integer    $status
 * @property integer    $count_success
 * @property integer    $created_at
 * @property integer    $updated_at
 *
 * @property string     $wordName
 * @property Word       $word
 * @property LessonWord $lessonWord
 */
class Repetition extends \yii\db\ActiveRecord
{
    const STATUS_ENABLE  = 1;
    const STATUS_DISABLE = 0;
    const STATUS_REMOVE  = 2;

    public static function tableName()
    {
        return 'repetition';
    }

    public static function statuses()
    {
        return [
            self::STATUS_ENABLE  => 'Enable',
            self::STATUS_DISABLE => 'Disable',
            self::STATUS_REMOVE  => 'Remove',
        ];
    }

    public static function find()
    {
        return new \app\models\queries\RepetitionQuery(get_called_class());
    }

    public function rules()
    {
        return [
            [['lesson_word_id'], 'required'],
            [
                [
                    'lesson_word_id',
                    'status',
                    'count_success',
                    'created_at',
                    'updated_at',
                ],
                'integer',
            ],
            [
                ['lesson_word_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => LessonWord::class,
                'targetAttribute' => ['lesson_word_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'lesson_word_id' => 'Lesson Word ID',
            'status'         => 'Статус',
            'count_success'  => 'Число верных повторений',
            'created_at'     => 'Created At',
            'updated_at'     => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery|LessonWord
     */
    public function getLessonWord()
    {
        return $this->hasOne(LessonWord::class, ['id' => 'lesson_word_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|Word
     */
    public function getWord()
    {
        return $this->hasOne(Word::class, ['id' => 'word_id'])
            ->viaTable(LessonWord::tableName(), ['word_id' => 'lesson_word_id']);
    }

    /**
     * @return string
     */
    public function getWordName()
    {
        return $this->word->name;
    }
}
