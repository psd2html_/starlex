<?php

namespace app\models;

/**
 * This is the model class for table "theme_translate".
 *
 * @property integer  $id
 * @property integer  $language_id
 * @property integer  $theme_id
 * @property string   $translate
 *
 * @property Language $lang
 * @property Theme    $theme
 */
class ThemeTranslate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theme_translate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'theme_id', 'translate'], 'required'],
            [['language_id', 'theme_id'], 'integer'],
            [['translate'], 'string', 'max' => 255],
            [
                ['language_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['language_id' => 'id'],
            ],
            [
                ['theme_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Theme::class,
                'targetAttribute' => ['theme_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'language_id' => 'Lang ID',
            'theme_id'    => 'Theme ID',
            'translate'   => 'Translate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::class, ['id' => 'theme_id']);
    }
}
