<?php

namespace app\models;

use app\modules\user\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_award".
 *
 * @property integer     $id
 * @property integer     $user_id
 * @property integer     $award_id
 * @property integer     $status
 * @property integer     $created_at
 * @property integer     $updated_at
 *
 * @property Award       $award
 * @property array|mixed $statusName
 * @property User        $user
 */
class UserAward extends \yii\db\ActiveRecord
{
    const STATUS_ENABLE  = 'Enable';
    const STATUS_DISABLE = 'Disable';

    public static function tableName()
    {
        return 'user_award';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function getStatusName()
    {
        return self::getStatuses($this->status);
    }

    public static function getStatuses($status = false)
    {
        $statuses = [
            1 => self::STATUS_ENABLE,
            0 => self::STATUS_DISABLE,
        ];

        return ($status !== false) ? $statuses[$status] : $statuses;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'award_id'], 'required'],
            [['user_id', 'award_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['user_id', 'award_id'], 'unique', 'targetAttribute' => ['user_id', 'award_id']],
            [
                ['award_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Award::class,
                'targetAttribute' => ['award_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'user_id'    => 'User ID',
            'award_id'   => 'Award ID',
            'status'     => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAward()
    {
        return $this->hasOne(Award::class, ['id' => 'award_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
