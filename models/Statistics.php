<?php

namespace app\models;

use app\modules\user\models\User;

/**
 * This is the model class for table "statistics".
 *
 * @property int $id
 * @property int $user_id
 * @property int $time
 * @property string $date
 * @property int $active_time
 * @property int $lang_id
 *
 * @property User $user
 */
class Statistics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'time', 'active_time'], 'required'],
            [['user_id', 'time', 'active_time', 'lang_id'], 'integer'],
            [['date'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'time' => 'Time',
            'date' => 'Date',
            'active_time' => 'Active Time',
            'lang_id' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
