CREATE TABLE `game` (
	`id` int(11) NOT NULL,
	`theme_id` int(11),
	`image` varchar(255),
	`create_at` int(11) NOT NULL,
	`update_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `lesson` (
	`id` int(11) NOT NULL,
	`name` varchar(255) NOT NULL,
	`theme_id` int(11),
	`status` tinyint(1),
	`time_passage` int(11),
	`lives_count` int(5),
	`points_count` int(5),
	`create_at` int(11) NOT NULL,
	`update_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `lesson_image` (
	`id` int(11) NOT NULL,
	`lesson_word_id` int(11) NOT NULL,
	`image` varchar(255) NOT NULL,
	`type` int(1) NOT NULL,
	`create_at` int(11) NOT NULL,
	`update_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `lesson_media` (
	`id` int(11) NOT NULL,
	`lesson_word_id` int(11) NOT NULL,
	`media` varchar(255) NOT NULL,
	`type` int(1) NOT NULL,
	`create_at` int(11) NOT NULL,
	`update_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `lesson_word` (
	`id` int(11) NOT NULL,
	`lesson_id` int(11),
	`word_id` int(11) NOT NULL,
	`created_at` int(11) NOT NULL,
	`updated_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `setting` (
	`id` int(11) NOT NULL,
	`key` varchar(255) NOT NULL UNIQUE,
	`value` TEXT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `user_social_account` (
	`id` int(11) NOT NULL,
	`user_id` int(11),
	`provider` varchar(255) NOT NULL,
	`client_id` varchar(255),
	`data` TEXT,
	`code` varchar(255),
	`email` varchar(255),
	`username` varchar(255),
	`phone` varchar(255),
	`avatar` varchar(255),
	`social_page` varchar(255),
	`sex` tinyint(1),
	`birthday` varchar(255),
	`create_at` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `tariff` (
	`id` int(11) NOT NULL,
	`name` varchar(255) NOT NULL,
	`date_to` int(11),
	`sum` double,
	`created_at` int(11) NOT NULL,
	`updated_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `theme` (
	`id` int(11) NOT NULL,
	`parent_id` int(11) NOT NULL DEFAULT '0',
	`name` varchar NOT NULL,
	`status` tinyint(1),
	`created_at` int(11) NOT NULL,
	`updated_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `user` (
	`id` int(11) NOT NULL,
	`username` varchar(255) NOT NULL,
	`firstname` varchar(255),
	`lastname` varchar(255),
	`email` varchar(255) NOT NULL,
	`password_hash` varchar(255) NOT NULL,
	`status` tinyint(1) DEFAULT '0',
	`auth_key` varchar(255) NOT NULL,
	`email_confirm_token` varchar(255) NOT NULL,
	`password_reset_token` varchar(255) NOT NULL,
	`created_at` int(11) NOT NULL,
	`updated_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `user_lesson` (
	`id` int(11) NOT NULL,
	`user_id` int(11) NOT NULL,
	`lesson_id` int(11) NOT NULL,
	`points` int(11),
	`rating` int(11),
	`flag_help` int(1),
	`flag_time` int(1),
	`flag_finish` int(1),
	`answers_count` int(5),
	`created_at` int(11) NOT NULL,
	`updated_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `user_lesson_word` (
	`id` int(11) NOT NULL,
	`lesson_words_id` int(11) NOT NULL,
	`user_id` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `user_setting` (
	`id` int(11) NOT NULL,
	`user_id` int(11) NOT NULL,
	`tariff_id` int(11),
	`native_lang_id` tinyint(1) NOT NULL DEFAULT '0',
	`learn_lang_id` tinyint(1) NOT NULL DEFAULT '0',
	`target_day` tinyint(1) NOT NULL DEFAULT '0',
	`notify_day_task` tinyint(1) NOT NULL DEFAULT '0',
	`notify_words_repeat` tinyint(1) NOT NULL DEFAULT '0',
	`test_status` tinyint(1) NOT NULL DEFAULT '0',
	`rewards` int(5) NOT NULL DEFAULT '0',
	`certificates` varchar(255),
	`rating` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `user_order_history` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`tariff_id` int(11) NOT NULL,
	`sum` double NOT NULL,
	`create_at` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `language` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` int(11) NOT NULL AUTO_INCREMENT,
	`locale` varchar(5) NOT NULL,
	`created_at` int(11) NOT NULL,
	`updated_at` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `theme_language` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`lang_id` int(11) NOT NULL,
	`theme_id` int(11) NOT NULL,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `lesson_translate` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`lesson_id` int(11) NOT NULL,
	`lang_id` int(11) NOT NULL,
	`translate` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `game_language` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`game_id` int(11) NOT NULL,
	`lang_id` int(11) NOT NULL,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `word` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`lang_id` int(11) NOT NULL,
	`name` varchar(255) NOT NULL,
	`description` TEXT,
	`created_at` int(11),
	`updated_at` int(11),
	PRIMARY KEY (`id`)
);

CREATE TABLE `word_translate` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`word_id` INT(11) NOT NULL,
	`translate_id` INT(11) NOT NULL,
	`order` FLOAT(1) NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`)
);

CREATE TABLE `lesson_word_translate` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`lesson_word_id` int(11) NOT NULL,
	`word_translate_id` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `lesson_word_bad_translate` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`lesson_word_id` int(11) NOT NULL,
	`word_translate_id` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `game` ADD CONSTRAINT `game_fk0` FOREIGN KEY (`theme_id`) REFERENCES `theme`(`id`);

ALTER TABLE `lesson` ADD CONSTRAINT `lesson_fk0` FOREIGN KEY (`theme_id`) REFERENCES `theme`(`id`);

ALTER TABLE `lesson_image` ADD CONSTRAINT `lesson_image_fk0` FOREIGN KEY (`lesson_word_id`) REFERENCES `lesson_word`(`id`);

ALTER TABLE `lesson_media` ADD CONSTRAINT `lesson_media_fk0` FOREIGN KEY (`lesson_word_id`) REFERENCES `lesson_word`(`id`);

ALTER TABLE `lesson_word` ADD CONSTRAINT `lesson_word_fk0` FOREIGN KEY (`lesson_id`) REFERENCES `lesson`(`id`);

ALTER TABLE `lesson_word` ADD CONSTRAINT `lesson_word_fk1` FOREIGN KEY (`word_id`) REFERENCES `word`(`id`);

ALTER TABLE `user_social_account` ADD CONSTRAINT `user_social_account_fk0` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`);

ALTER TABLE `user_lesson` ADD CONSTRAINT `user_lesson_fk0` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`);

ALTER TABLE `user_lesson` ADD CONSTRAINT `user_lesson_fk1` FOREIGN KEY (`lesson_id`) REFERENCES `lesson`(`id`);

ALTER TABLE `user_lesson_word` ADD CONSTRAINT `user_lesson_word_fk0` FOREIGN KEY (`lesson_words_id`) REFERENCES `lesson_word`(`id`);

ALTER TABLE `user_lesson_word` ADD CONSTRAINT `user_lesson_word_fk1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`);

ALTER TABLE `user_setting` ADD CONSTRAINT `user_setting_fk0` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`);

ALTER TABLE `user_setting` ADD CONSTRAINT `user_setting_fk1` FOREIGN KEY (`tariff_id`) REFERENCES `tariff`(`id`);

ALTER TABLE `user_setting` ADD CONSTRAINT `user_setting_fk2` FOREIGN KEY (`native_lang_id`) REFERENCES `language`(`id`);

ALTER TABLE `user_setting` ADD CONSTRAINT `user_setting_fk3` FOREIGN KEY (`learn_lang_id`) REFERENCES `language`(`id`);

ALTER TABLE `user_order_history` ADD CONSTRAINT `user_order_history_fk0` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`);

ALTER TABLE `user_order_history` ADD CONSTRAINT `user_order_history_fk1` FOREIGN KEY (`tariff_id`) REFERENCES `tariff`(`id`);

ALTER TABLE `theme_language` ADD CONSTRAINT `theme_language_fk0` FOREIGN KEY (`lang_id`) REFERENCES `language`(`id`);

ALTER TABLE `theme_language` ADD CONSTRAINT `theme_language_fk1` FOREIGN KEY (`theme_id`) REFERENCES `theme`(`id`);

ALTER TABLE `lesson_translate` ADD CONSTRAINT `lesson_translate_fk0` FOREIGN KEY (`lesson_id`) REFERENCES `lesson`(`id`);

ALTER TABLE `lesson_translate` ADD CONSTRAINT `lesson_translate_fk1` FOREIGN KEY (`lang_id`) REFERENCES `language`(`id`);

ALTER TABLE `game_language` ADD CONSTRAINT `game_language_fk0` FOREIGN KEY (`game_id`) REFERENCES `game`(`id`);

ALTER TABLE `game_language` ADD CONSTRAINT `game_language_fk1` FOREIGN KEY (`lang_id`) REFERENCES `language`(`id`);

ALTER TABLE `word` ADD CONSTRAINT `word_fk0` FOREIGN KEY (`lang_id`) REFERENCES `language`(`id`);

ALTER TABLE `word_translate` ADD CONSTRAINT `word_translate_fk0` FOREIGN KEY (`word_id`) REFERENCES `word`(`id`);

ALTER TABLE `word_translate` ADD CONSTRAINT `word_translate_fk1` FOREIGN KEY (`translate_id`) REFERENCES `word`(`id`);

ALTER TABLE `lesson_word_translate` ADD CONSTRAINT `lesson_word_translate_fk0` FOREIGN KEY (`lesson_word_id`) REFERENCES `lesson_word`(`id`);

ALTER TABLE `lesson_word_translate` ADD CONSTRAINT `lesson_word_translate_fk1` FOREIGN KEY (`word_translate_id`) REFERENCES `word_translate`(`id`);

ALTER TABLE `lesson_word_bad_translate` ADD CONSTRAINT `lesson_word_bad_translate_fk0` FOREIGN KEY (`lesson_word_id`) REFERENCES `lesson_word`(`id`);

ALTER TABLE `lesson_word_bad_translate` ADD CONSTRAINT `lesson_word_bad_translate_fk1` FOREIGN KEY (`word_translate_id`) REFERENCES `word_translate`(`id`);

