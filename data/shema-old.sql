/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.13 : Database - starlex
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`starlex` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `starlex`;

/*Table structure for table `games` */

DROP TABLE IF EXISTS `games`;

CREATE TABLE `games` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `themes_id` int(11) unsigned DEFAULT NULL,
  `create_at` int(11) unsigned NOT NULL,
  `update_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `themes_id` (`themes_id`),
  CONSTRAINT `games_ibfk_1` FOREIGN KEY (`themes_id`) REFERENCES `themes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `lessons` */

DROP TABLE IF EXISTS `lessons`;

CREATE TABLE `lessons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `theme_id` int(11) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '0-выкл;1-вкл',
  `time_passage` int(11) DEFAULT NULL COMMENT 'Время прохождения',
  `lives_count` int(5) DEFAULT NULL COMMENT 'Кол-во жизней',
  `points_count` int(5) DEFAULT NULL COMMENT 'Ко-во звезд, которые может набрать пользователь',
  `create_at` int(11) unsigned NOT NULL,
  `update_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `theme_id` (`theme_id`),
  CONSTRAINT `lessons_ibfk_1` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `lessons_images` */

DROP TABLE IF EXISTS `lessons_images`;

CREATE TABLE `lessons_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lesson_id` int(11) unsigned NOT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Картинка урока',
  `flag_image` int(1) NOT NULL COMMENT '0-не верный вариант;1-правильный вариант',
  `create_at` int(11) unsigned NOT NULL,
  `update_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lesson_id` (`lesson_id`),
  CONSTRAINT `lessons_images_ibfk_1` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `lessons_media` */

DROP TABLE IF EXISTS `lessons_media`;

CREATE TABLE `lessons_media` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lesson_id` int(11) unsigned NOT NULL,
  `media` varchar(255) NOT NULL COMMENT 'путь к файлу',
  `type` int(1) NOT NULL COMMENT '0-не верный вариант;1-правильный вариант',
  `create_at` int(11) unsigned NOT NULL,
  `update_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lessons_media_ibfk_1` (`lesson_id`),
  CONSTRAINT `lessons_media_ibfk_1` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `lessons_words` */

DROP TABLE IF EXISTS `lessons_words`;

CREATE TABLE `lessons_words` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lesson_id` int(11) unsigned DEFAULT NULL,
  `create_at` int(11) unsigned NOT NULL,
  `update_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lesson_id` (`lesson_id`),
  CONSTRAINT `lessons_words_ibfk_1` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `lessons_words_translate` */

DROP TABLE IF EXISTS `lessons_words_translate`;

CREATE TABLE `lessons_words_language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lessons_words_id` int(11) unsigned NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Перевод',
  `type` int(1) NOT NULL COMMENT '0-не верный ответ;1-верный ответ',
  `create_at` int(11) unsigned NOT NULL,
  `update_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lessons_words_language_ibfk_1` (`lessons_words_id`),
  KEY `lang_id_ibfk_1` (`lang_id`),
  CONSTRAINT `lessons_words_translate_ibfk_1` FOREIGN KEY (`lessons_words_id`) REFERENCES `lessons_words` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  CONSTRAINT `lang_id_ibfk_1` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Email администратора',
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `social_account` */

DROP TABLE IF EXISTS `social_account`;

CREATE TABLE `social_account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `data` text,
  `code` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `social_page` varchar(255) DEFAULT NULL COMMENT 'Ссылка на профиль юзера',
  `sex` tinyint(1) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `created_at` int(11) unsigned NOT NULL,
  `updated_at` int(11) unsigned DEFAULT NULL COMMENT 'время последнего входа',
  PRIMARY KEY (`id`),
  KEY `social_account_ibfk_1` (`user_id`),
  CONSTRAINT `social_account_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tariffs` */

DROP TABLE IF EXISTS `tariffs`;

CREATE TABLE `tariffs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_to` int(11) DEFAULT NULL,
  `sum` double DEFAULT NULL,
  `create_at` int(11) unsigned NOT NULL,
  `update_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `themes` */

DROP TABLE IF EXISTS `themes`;

CREATE TABLE `themes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'родитель темы',
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '0-отключена;1-активна',
  `create_at` int(11) unsigned NOT NULL,
  `update_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0-заблокирован;1-активен;2-ожидает активации',
  `created_at` int(11) unsigned NOT NULL,
  `updated_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user_lessons` */

DROP TABLE IF EXISTS `user_lessons`;

CREATE TABLE `user_lessons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `lesson_id` int(11) unsigned NOT NULL,
  `points` int(11) DEFAULT NULL COMMENT 'Кол-во набранных звезд за урок',
  `rating` int(11) DEFAULT NULL COMMENT 'Кол-во набранного опыта за урок',
  `flag_help` int(1) DEFAULT NULL COMMENT '0-пройдено без подсказок;1-с подсказками',
  `flag_time` int(1) DEFAULT NULL COMMENT '0-пройдено во время;1-пройдено сверх лимита',
  `flag_finish` int(1) DEFAULT NULL COMMENT '0-пройдено до конца;1-пройдено частично',
  `answers_count` int(5) DEFAULT NULL COMMENT 'Кол-во правильных ответов',
  `create_at` int(11) unsigned NOT NULL COMMENT 'время прохождения',
  `update_at` int(11) unsigned DEFAULT NULL COMMENT 'время когда урок был пройден повторно',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `lesson_id` (`lesson_id`),
  CONSTRAINT `user_lessons_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_lessons_ibfk_2` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user_lessons_words` */

DROP TABLE IF EXISTS `user_lessons_words`;

CREATE TABLE `user_lessons_words` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lessons_words_id` int(11) unsigned NOT NULL COMMENT 'изученное слово',
  `user_id` int(11) unsigned NOT NULL COMMENT 'пользователь (дублирование связи)',
  PRIMARY KEY (`id`),
  KEY `lessons_words_id` (`lessons_words_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_lessons_words_ibfk_1` FOREIGN KEY (`lessons_words_id`) REFERENCES `lessons_words` (`id`),
  CONSTRAINT `user_lessons_words_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='таблица изученных слов';

/*Table structure for table `user_order_history` */

DROP TABLE IF EXISTS `user_order_history`;

CREATE TABLE `user_order_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `tariff_id` int(11) NOT NULL,
  `sum` double NOT NULL COMMENT 'За сколько было куплено',
  `create_at` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `tariff_id` (`tariff_id`),
  CONSTRAINT `user_order_history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_order_history_ibfk_2` FOREIGN KEY (`tariff_id`) REFERENCES `tariffs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='История приобретения тарифов';

/*Table structure for table `user_settings` */

DROP TABLE IF EXISTS `user_settings`;

CREATE TABLE `user_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `tariff_id` int(11) DEFAULT NULL COMMENT 'тариф пользователя',
  `native_lang_id` int(11) NOT NULL COMMENT 'Родной язык',
  `learn_lang_id` tinyint(1) NOT NULL COMMENT 'Язык обучения',
  `target_day` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Дневная цель (0-10 минут;1-20 минут;далее уточнить)',
  `notify_day_task` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Напоминать о задании на день(0-выкл;1-вкл)',
  `notify_words_repeat` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Напоминать о словах для повторения(0-выкл;1-вкл)',
  `test_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Статус вводного задания(0-не пройдено;1-пройдено)',
  `rewards` int(5) NOT NULL DEFAULT '0' COMMENT 'Кол-во полученных наград',
  `certificates` varchar(255) DEFAULT NULL COMMENT 'массив открытых номеров сертификата(1-9)',
  `rating` int(11) DEFAULT NULL COMMENT 'Кол-во набранного опыта',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `tariff_id` (`tariff_id`),
  KEY `native_lang_id` (`native_lang_id`),
  KEY `learn_lang_id` (`learn_lang_id`),
  CONSTRAINT `user_settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_settings_ibfk_2` FOREIGN KEY (`tariff_id`) REFERENCES `tariffs` (`id`)
  CONSTRAINT `user_settings_ibfk_3` FOREIGN KEY (`native_lang_id`) REFERENCES `language` (`id`)
  CONSTRAINT `user_settings_ibfk_4` FOREIGN KEY (`learn_lang_id`) REFERENCES `language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
