var $ = jQuery;
document.addEventListener('DOMContentLoaded', function () {
    var Sandbox = {
        changeUrl: function () {
            var urlSelected = $("#url-select").find('option:selected').val();
            $("#url").val(urlSelected);
        },
        formSubmit: function () {
            var editor = ace.edit("response_ace");
            $.ajax({
                url: $("#url").val(),
                cache: false,
                dataType: "json",
                data: $.parseJSON($("#params").val()),
                type: $("#method").val(),
                beforeSend: function (xhr) {
                    //xhr.setRequestHeader("X-Username", $("#username").val());
                    //xhr.setRequestHeader("X-Password", $("#password").val());
                    //console.log(xhr);
                },
                success: function (data, textStatus, XMLHttpRequest) {
                    editor.setValue(JSON.stringify(data, null, 4));
                    // .height($("#response-ace")[0].scrollHeight);
                    console.dir(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    editor.setValue(XMLHttpRequest.responseText);
                    console.log(XMLHttpRequest);
                }
            });

            return false;
        },
        init: function () {
            this.changeUrl();

            $("#url-select").change(this.changeUrl);
            $("#form-api").submit(this.formSubmit);
        }
    };
    Sandbox.init();

    var lesson = {
        countGroups : 0,
        createNewWord: function () {
            // Создание новых полей для группы слов
            var newGroup = $('.new-group-hidden').clone(true).appendTo('#words-list').removeClass('new-group-hidden').addClass('new-group').addClass('word-group');
            var countGroups = lesson.reCalcCountGroups();

            newGroup.find('h4').text(countGroups);
            lesson.reNameGroups();

            if (countGroups > 1) {
                $('.remove-word').each(function (indx, element) {
                    $(element).show();
                });
            }
        },
        reCalcCountGroups: function () {
            // Подсчёт всех групп слов
            this.countGroups = $('.word-group:not(.new-group-hidden)').length;
            $('#count-words').text(this.countGroups);

            return this.countGroups;
        },
        reNameGroups: function () {
            // Переименование названий новых групп по порядку.
            // Переименование названий input в новых группах.
            $('.word-group').each(function (groupId, groupElem) {
                groupId++;
                var isRenamed = false;
                $(groupElem).find('input[type="text"]').each(function (indx, element) {
                    var oldName = $(element).attr('name');

                    var newName = 'Lesson[wordGroups][new_' + groupId + ']';
                    newName = oldName.replace(/Lesson\[wordGroups\]\[new(_\d+)?\]/i, newName);
                    $(element).attr('name', newName);

                    isRenamed = (oldName !== newName);
                });
                if ( isRenamed ){
                    $(groupElem).find('h4').text(groupId);
                }
            });
        },
        init: function () {
            if(lesson.reCalcCountGroups() === 0) {
                lesson.createNewWord();
            }

            // Создание новых полей для группы слов
            $('#create-new-word').click(lesson.createNewWord);

            var removes = $('.remove-word');

            if (removes.length === 1) {
                removes.hide();
            }

            removes.click(function () {
                var buttonDelete = this;
                var deleteGroup = function (resource) {
                    $(buttonDelete).closest('.word-group').remove();
                    lesson.reCalcCountGroups();
                    if ((removes = $('.remove-word')).length === 1) {
                        removes.hide();
                    }
                    lesson.reNameGroups();
                };
                var groupId = $(buttonDelete).data('group-id');

                if (typeof groupId !== 'undefined') {
                    $.ajax({
                        'method': 'POST',
                        'url': '/lesson/remove-word-group?id=' + $(buttonDelete).data('group-id'),
                        'success': deleteGroup
                    });
                } else {
                    deleteGroup();
                }
            });
        }
    };
    lesson.init();


});
