<?php

namespace app\modules\api\models;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;

/**
 * Signup form api
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;

    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            //['username', 'required'],
            ['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
            [
                'username',
                'unique',
                'targetClass' => User::class,
                //'message'     => Yii::t('app', 'Это имя пользователя уже занято.'),
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => User::class,
                'message'     => Yii::t('app', 'Этот e-mail уже используется.'),
            ],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()){
            $user = new User();
            //$user->username = $this->username;
            $user->username = $this->email;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->status = User::STATUS_ACTIVE;
            $user->generateAuthKey();
            //$user->generateEmailConfirmToken();

            if ($user->save()){
                Yii::$app->mailer->compose('@app/modules/api/mails/newAccount', ['user' => $user])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($this->email)
                    ->setSubject(Yii::t('app', 'Thank you for posting in app "{app_name}"', [
                        'app_name' => Yii::$app->name,
                    ]))
                    ->send();

                return $user;
            }
        }

        return null;
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email'    => 'E-mail',
            'password' => 'Пароль',
        ];
    }
}