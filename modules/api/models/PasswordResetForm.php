<?php
namespace app\modules\api\models;

use yii\base\Model;
use yii\base\InvalidParamException;
use app\modules\user\models\User;
use Yii;
/**
 * Password reset form
 */
class PasswordResetForm extends Model
{
    public $username;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*['username', 'trim'],
            ['username', 'required'],
            ['username', 'exist',
                'targetClass' => User::class,
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'Нет пользователя с данным логином.'
            ],*/

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::class,
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'Пользователь с данным e-mail не зарегистрирован.'
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        /* @var $user User */
        if($this->validate()) {

            $user = User::findOne([
                'status' => User::STATUS_ACTIVE,
                'email' => $this->email
            ]);

            if (!$user) {
                $this->addError('username', 'Кобминации из Логина и Адреса электронной почты не найдено.');
                return false;
            }

            $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890";
            $length = intval(6);
            $size = strlen($chars) - 1;
            $password = "";
            while ($length--) $password .= $chars[rand(0, $size)];
            $user->setPassword($password);
            $user->generateAuthKey();
            $user->save(false);

            Yii::$app->mailer->compose('@app/modules/api/mails/passwordReset', ['user' => $user, 'password' => $password])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                ->setTo($this->email)
                ->setSubject('Password reset for ' . Yii::$app->name)
                ->send();

            return $password;
        }

        return null;
    }
}