<?php

namespace app\modules\api\models;

use app\models\Word;

class WordModel extends Word
{
    public function fields()
    {
        return [
            'id',
            'lang_id',
            'name',
            'description',
            'status',
            'created_at',
            'updated_at',
            'word_group_id',
            'is_word_collocation',
        ];
    }
}
