<div class="row routes">
    <div class="col-xs-12">
        <h2>Методы:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/lesson-image</a>: получение списка картинок (сущности, без переводов)
            </li>
            <li>GET
                <a href="#">/api/v1/lesson-image/1</a>: получение информации по id равным 1;
            </li>
        </ul>


        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Field</th>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>id</td>
                            <td>Int</td>
                            <td>Primary ID</td>
                        </tr>
                        <tr>
                            <td>lesson_id</td>
                            <td>Int</td>
                            <td>ID Урока - внешний ключ(таблица Lesson)</td>
                        </tr>
                        <tr>
                            <td>lang_id</td>
                            <td>Int</td>
                            <td>ID языка - внешний ключ(таблица Language)</td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>string</td>
                            <td>имя картинки</td>
                        </tr>
                        <tr>
                            <td>type</td>
                            <td>Int</td>
                            <td>Тип варианта ответа - 0-не верный вариант; 1-правильный вариант</td>
                        </tr>
                        <tr>
                            <td>created_at</td>
                            <td>Timestamp</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>updated_at</td>
                            <td>Timestamp</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>




        <h3>Путь к картинкам</h3>
<pre>
/uploads/images/lessons/[LESSON_ID]/[ИМЯ_КАРТИНКИ] - большая оригинальная картинка
/uploads/images/lessons/[LESSON_ID]/thumbs/100_100_[NAME_КАРТИНКИ] - миниатюра картинки (по умолчанию 100х100)
</pre>


        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/lesson-image/1
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": 1,
    "lesson_id": 4,
    "lang_id": 1,
    "image": "c4ca4238a0b9238.jpg",
    "type": 1,
    "created_at": 1502706308,
    "updated_at": 1502706308
}
                </pre>

            </div>
        </div>



    </div>
</div>