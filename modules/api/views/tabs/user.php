    <!-->
    <div class="row routes">
        <div class="col-xs-12">
            <h2>Methods:</h2>
            <ul>
                <li>POST <a href="#auth">/api/v1/auth/login</a> - <b>Аутентификация пользователя</b></li>
                <li>POST <a href="#signup">/api/v1/auth/signup</a> - <b>Регистрация пользователя</b></li>
                <li>POST <a href="#reset">/api/v1/auth/reset</a> - <b>Восстановление пароля</b></li>
                <li>POST <a href="#authsocial">/api/v1/auth/loginsocial</a> - <b>Аутентификация через соц. сети</b></li>
            </ul>
            <ul>
                <li>GET
                    <a href="#luser">/api/v1/user</a>: получение постранично списка всех пользователей;
                </li>
                <li>POST <a href="#luser">/api/v1/user</a>: создание нового пользователя;
                </li>
                <li>GET
                    <a href="#luser">/api/v1/user/123</a>: получение информации по конкретному пользователю с id равным 123;
                </li>
                <li>PUT
                    <a href="#put">/api/v1/user/1</a>: изменение информации по пользователю с id равным 1;
                </li>
                <li>DELETE
                    <a href="#luser">/api/v1/user/123</a>: удаление пользователя с id равным 123;
                </li>
                <li>OPTIONS
                    <a href="#luser">/api/v1/user</a>: получение поддерживаемых методов, по которым можно обратится к /users
                </li>
                <li>OPTIONS
                    <a href="#luser">/api/v1/user/123</a>: получение поддерживаемых методов, по которым можно обратится к /user/123.
                </li>
            </ul>
        </div>
    </div>

    <div id="authsocial">
        <h3>Аутентификация через соц. сети</h3>
        <pre>/api/v1/auth/loginsocial</pre>
        <h4>Параметры</h4>
        <pre>
{"provider":"[PROVIDER]","token":"[TOKEN]"}
</pre>
        <pre>
provider - vk|facebook|google
token - токен полученный в приложении (AccessToken)
</pre>
    </div>

    <div id="auth">
        <h3>Аутентификация пользователя</h3>
        <pre>/api/v1/auth/login</pre>
        <h4>Параметры</h4>
        <pre>
{"email":"[email]","password":"[PASSWORD]"}
</pre>
    </div>

    <div id="signup">
        <h3>Регистрация пользователя</h3>
        <pre>/api/v1/auth/signup</pre>
        <h4>Параметры</h4>
        <pre>
{"email":"[EMAIL]","password":"[PASSWORD]"}
</pre>
    </div>

    <div id="reset">
        <h3>Восстановление пароля</h3>
        <pre>/api/v1/auth/reset</pre>
        <h4>Параметры</h4>
        <pre>
{"email":"[EMAIL]"}
</pre>
    </div>

    <div id="put">
    <h2>Изменение информации пользователя</h2>
    <h5>метод вызывается с токеном пользователя</h5>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>
                <span class="label label-info">POST</span> - PUT /api/v1/user/1
            </h4>
        </div>
        <div class="panel-heading">
            <h4>Параметры (доступны для изменения только Имя и Фамилия)</h4>
            <pre>
{
  "firstname": "Test",
  "lastname": "Test2"
}
                </pre>
        </div>
        <div class="panel-body">
            <h4>Ответ</h4>
            <pre>
{
    "id": 1,
    "username": "ad",
    "firstname": "Test",
    "lastname": "Test2",
    "email": "ad",
    "password_hash": "$2y$13$",
    "status": 1,
    "auth_key": "uthQCS",
    "email_confirm_token": null,
    "password_reset_token": null,
    "created_at": 1504461497,
    "updated_at": 1526535450,
    "rating": 0,
    "type": 9
}
                </pre>

        </div>
    </div>
    </div>


    <div id="luser">
        <h3>User:</h3>

        <pre>/api/v1/user/:id</pre>

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Field</th>
                    <th>Type</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>Int</td>
                    <td>Primary ID</td>
                </tr>
                <tr>
                    <td>username</td>
                    <td>string</td>
                    <td>login</td>
                </tr>
                <tr>
                    <td>firsttname</td>
                    <td>String</td>
                    <td></td>
                </tr>
                <tr>
                    <td>lastname</td>
                    <td>String</td>
                    <td></td>
                </tr>
                <tr>
                    <td>email</td>
                    <td>String</td>
                    <td></td>
                </tr>
                <tr>
                    <td>password_hash</td>
                    <td>String</td>
                    <td></td>
                </tr>
                <tr>
                    <td>status</td>
                    <td>Int</td>
                    <td>0-заблокирован;1-активен;2-ожидает активации</td>
                </tr>
                <tr>
                    <td>auth_key</td>
                    <td>String</td>
                    <td>Это и есть token для юзера</td>
                </tr>
                <tr>
                    <td>email_confirm_token</td>
                    <td>String</td>
                    <td></td>
                </tr>
                <tr>
                    <td>password_reset_token</td>
                    <td>String</td>
                    <td></td>
                </tr>
                <tr>
                    <td>created_at</td>
                    <td>Timestamp</td>
                    <td></td>
                </tr>
                <tr>
                    <td>updated_at</td>
                    <td>Timestamp</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>


        <h4>Relations which you can use:</h4>
        <div class="row">
            <div class="col-xs-3"> <!-- required for floating -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-left">
                    <li class="active">
                        <a href="#tkt-t1" data-toggle="tab" aria-expanded="true">(none)</a>
                    </li>
                </ul>
            </div>

            <div class="col-xs-9">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="tkt-t1">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>(none)</td>
                                    <td>(none)</td>
                                    <td>(none)</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tkt-t2">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>id</td>
                                    <td>Int</td>
                                    <td>Primary ID</td>
                                </tr>
                                <tr>
                                    <td>note</td>
                                    <td>String</td>
                                    <td>Note for the ticket</td>
                                </tr>
                                <tr>
                                    <td>created_at</td>
                                    <td>Timestamp</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tkt-t3">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>id</td>
                                    <td>Int</td>
                                    <td>Primary ID</td>
                                </tr>
                                <tr>
                                    <td>email</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>firstname</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>lastname</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>imageUrl</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>phone</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>imageUrl</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>about</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tkt-t4">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>id</td>
                                    <td>Int</td>
                                    <td>Primary ID</td>
                                </tr>
                                <tr>
                                    <td>fileUrl</td>
                                    <td>String</td>
                                    <td>Path to the file</td>
                                </tr>
                                <tr>
                                    <td>caption</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>created_at</td>
                                    <td>Timestamp</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tkt-t5">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>id</td>
                                    <td>Int</td>
                                    <td>Primary ID</td>
                                </tr>
                                <tr>
                                    <td>imageUrl</td>
                                    <td>String</td>
                                    <td>Path to the file</td>
                                </tr>
                                <tr>
                                    <td>caption</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>created_at</td>
                                    <td>Timestamp</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tkt-t6">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>id</td>
                                    <td>Int</td>
                                    <td>Primary ID</td>
                                </tr>
                                <tr>
                                    <td>videoUrl</td>
                                    <td>String</td>
                                    <td>Path to the file</td>
                                </tr>
                                <tr>
                                    <td>caption</td>
                                    <td>String</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>created_at</td>
                                    <td>Timestamp</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <h4>Create new user:</h4>
        <div class="row">
            <div class="col-md-12">
                <pre>POST /api/v1/user</pre>
                <h5>Params:</h5>
                <pre>
{
  "username": "User1",
  "firstname": "Test",
  "lastname": "Test",
  "email": "test@gmail.com",
  "password": "123456"
}
                                    </pre>
            </div>
        </div>


    </div>
    <!-->