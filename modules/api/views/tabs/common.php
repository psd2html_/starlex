    <h3>Как это работает</h3>
    <ol>
        <li>Регистрируем пользователя по своему логину и паролю</li>
        <li>Проходим аутентификацию юзера - получаем массив даных юзера, где есть `auth_key` (это
            <b>Token</b> юзера)
        </li>
        <li>Используем всегда Token в дальнейших запросах при работе с разными ресурсами</li>
    </ol>

    <h3>Передача Token-а через заголовок</h3>
    <pre>
В заголовках должно быть:
 - Accept: application/json
 - Content-Type: application/json; charset=utf-8
 - Authorization: Bearer [TOKEN_ПОЛЬЗОВАТЕЛЯ] -- TOKEN_ПОЛЬЗОВАТЕЛЯ после регистрации записывается в таблицу `user`.`auth_key`
</pre>

    <h3>Передача Token-а в url</h3>
    <pre>
POST  /api/v1/user?access-token=[TOKEN_ПОЛЬЗОВАТЕЛЯ]
</pre>
    <h3>Передача Token-а в параметрах</h3>
    <pre>
{"access-token":"[TOKEN_ПОЛЬЗОВАТЕЛЯ]"}
</pre>