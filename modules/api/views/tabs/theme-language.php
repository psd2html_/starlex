<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/theme-language</a>: получение списка тем c переводами
            </li>
            <li>GET
                <a href="#">/api/v1/theme-language/1</a>: получение информации с id равным 1;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>theme_id</td>
                        <td>Int</td>
                        <td>ID темы - внешний ключ(таблица themes)</td>
                    </tr>
                    <tr>
                        <td>lang_id</td>
                        <td>Int</td>
                        <td>ID языка - внешний ключ(таблица Language)</td>
                    </tr>
                    <tr>
                        <td>name</td>
                        <td>string</td>
                        <td>название темы (перевод)</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>



        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/theme-language/1
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": 1,
    "lang_id": 1,
    "theme_id": 1,
    "name": "Первые Слова"
}
                </pre>

            </div>
        </div>


    </div>
</div>