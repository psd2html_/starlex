<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/language</a>: получение списка языков системы
            </li>
            <li>GET
                <a href="#">/api/v1/language/1</a>: получение информации по конкретному языку;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>name</td>
                        <td>string</td>
                        <td>название языка</td>
                    </tr>
                    <tr>
                        <td>locale</td>
                        <td>string</td>
                        <td>кодировка (локаль) Языка</td>
                    </tr>
                    <tr>
                        <td>is_default</td>
                        <td>Int</td>
                        <td>родной язык (если 1 - то можно выбрать как родной язык)</td>
                    </tr>
                    <tr>
                        <td>is_learn</td>
                        <td>Int</td>
                        <td>изучаемый язык (если 1 - то можно выбрать как изучаемый язык)</td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/language/1
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
        "id": 1,
        "name": "Русский",
        "locale": "ru",
        "is_default": 1,
        "is_learn": 0,
        "created_at": 1507526833,
        "updated_at": 1507526833
}
                </pre>

            </div>
        </div>


    </div>
</div>