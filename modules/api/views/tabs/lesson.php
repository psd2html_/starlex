<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/lesson</a>: получение списка уроков
            </li>
            <li>GET
                <a href="#">/api/v1/lesson/1</a>: получение информации по id равным 1;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>theme_id</td>
                        <td>Int</td>
                        <td>ID темы - внешний ключ(таблица themes)</td>
                    </tr>
                    <tr>
                        <td>lang_id</td>
                        <td>Int</td>
                        <td>ID языка</td>
                    </tr>
                    <tr>
                        <td>status</td>
                        <td>Int</td>
                        <td>0-выкл;1-вкл</td>
                    </tr>
                    <tr>
                        <td>name</td>
                        <td>string</td>
                        <td>имя урока</td>
                    </tr>
                    <tr>
                        <td>time_passage</td>
                        <td>Int</td>
                        <td>Время прохождения</td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/lesson/1
                </h4>
            </div>
            <div class="panel-heading">
                <h4>Параметры
                </h4>
                <pre>
{
    "lang_id":"[LANG_ID]", - по умолчанию первый язык
}
                </pre>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": "1",
    "theme_id": "2",
    "lang_id": "1",
    "name": "ПЕРВЫЙ УРОК",
    "status": "1",
    "time_passage": "0",
    "created_at": "1528544734",
    "updated_at": "1528544734"
}
                </pre>

            </div>
        </div>


    </div>
</div>