<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/lessons-media</a>: получение списка аудио файлов (сущности, без переводов)
            </li>
            <li>GET
                <a href="#">/api/v1/lessons-media/123</a>: получение информации по id равным 123;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>lesson_id</td>
                        <td>Int</td>
                        <td>ID Урока - внешний ключ(таблица Lesson)</td>
                    </tr>
                    <tr>
                        <td>lang_id</td>
                        <td>Int</td>
                        <td>ID языка - внешний ключ(таблица Language)</td>
                    </tr>
                    <tr>
                        <td>media</td>
                        <td>string</td>
                        <td>имя файла</td>
                    </tr>
                    <tr>
                        <td>type</td>
                        <td>Int</td>
                        <td>Тип варианта ответа - 0-не верный вариант; 1-правильный вариант</td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>




        <h3>Путь к файлу</h3>
<pre>
/uploads/media/lessons/[LESSON_ID]/[ИМЯ_ФАЙЛА]
</pre>


        <h2>Получить запись по id сущности</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/lessons-media/1
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": 1,
    "lesson_id": 4,
    "lang_id": 1,
    "media": "test.mp3",
    "type": 1,
    "created_at": 1502706308,
    "updated_at": 1502706308
}
                </pre>

            </div>
        </div>

    </div>
</div>