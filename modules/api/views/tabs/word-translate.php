<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/themes</a>: получение списка тем (сущности, без переводов)
            </li>
            <li>GET
                <a href="#">/api/v1/themes/1</a>: получение информации по конкретной теме с id равным 1;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>parent_id</td>
                        <td>Int</td>
                        <td>ID родителя (в этой же таблице)</td>
                    </tr>
                    <tr>
                        <td>status</td>
                        <td>Int</td>
                        <td>0-отключена;1-активна</td>
                    </tr>
                    <tr>
                        <td>name</td>
                        <td>string</td>
                        <td>название темы</td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/themes/1
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": 1,
    "parent_id": 0,
    "status": 1,
    "created_at": 1502188368,
    "updated_at": null,
    "name": "Первые Слова"
}
                </pre>

            </div>
        </div>

    </div>
</div>