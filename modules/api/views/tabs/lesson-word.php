<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/lesson-word</a>: получение списка слов темы (сущности, без переводов)
            </li>
            <li>GET
                <a href="#">/api/v1/lesson-word/1</a>: получение информации по id равным 1;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>lesson_id</td>
                        <td>Int</td>
                        <td>ID Урока - внешний ключ(таблица lesson)</td>
                    </tr>
                    <tr>
                        <td>word_group_id</td>
                        <td>Int</td>
                        <td>ID Урока - внешний ключ(таблица lesson)</td>
                    </tr>
                    <tr>
                        <td>sync_id</td>
                        <td>Int</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/lesson-word/1
                </h4>
            </div>
            <div class="panel-heading">
                <h4>Параметры
                </h4>
                <pre>
{
    "lang_id":"[LANG_ID]", - по умолчанию первый язык
}
                </pre>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": "1",
    "lesson_id": "1",
    "name": "first name",
    "lesson_world_bad_translate_id": "1",
    "bad_translate": "last name",
    "created_at": "1528544735",
    "updated_at": "1528544735"
}
                </pre>

            </div>
        </div>


    </div>
</div>