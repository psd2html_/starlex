<?php
/**
 * Created by PhpStorm.
 * User: jintropin
 * Date: 05.04.2018
 * Time: 19:27
 */
?>
<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/completed-user-lesson</a>: получение списка выполненных уроков
            </li>
            <li>GET
                <a href="#">/api/v1/completed-user-lesson/1</a>: получение информации по id равным 1;
            </li>
            <li>GET
                <a href="#">/api/v1/completed-user-lesson/user?id=2</a>: получение информации по id пользователя 2;
            </li>
            <li>POST
                <a href="#">/api/v1/completed-user-lesson</a>: записать данные о выполнение урока;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>user_id</td>
                        <td>Int</td>
                        <td>ID пользователя</td>
                    </tr>
                    <tr>
                        <td>lesson_id</td>
                        <td>Int</td>
                        <td>ID урока</td>
                    </tr>
                    <tr>
                        <td>lang_id</td>
                        <td>Int</td>
                        <td>Родной язык</td>
                    </tr>
                    <tr>
                        <td>translate_id</td>
                        <td>Int</td>
                        <td>Изучаемый язык</td>
                    </tr>
                    <tr>
                        <td>stars_total</td>
                        <td>Int</td>
                        <td>Количество звезд</td>
                    </tr>
                    <tr>
                        <td>points</td>
                        <td>Int</td>
                        <td>Опыт</td>
                    </tr>
                    <tr>
                        <td>flag_help</td>
                        <td>Int</td>
                        <td>Использование помощи</td>
                    </tr>
                    <tr>
                        <td>flag_time</td>
                        <td>Int</td>
                        <td>Время</td>
                    </tr>
                    <tr>
                        <td>answers_count</td>
                        <td>Int</td>
                        <td>Количество ответов</td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <h2>Записать данные о выполнение урока</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">POST</span> - /api/v1/completed-user-lesson
                </h4>
            </div>
            <div class="panel-heading">
                <h4>Параметры
                </h4>
                <pre>
{
    "lesson_id":"[LESSON_ID]", - Ид урока
    "lang_id":"[LANG_ID]", - Ид языка родного,
    "translate_id":"[TRANSLATE_LANG_ID]", - Ид языка изучаемого
    "stars_total":"[STARS_TOTAL]", - количество звезд
    "points":"[TRANSLATE_ID]", - опыт
    "flag_help":"[FLAG_HELP]", - Использование помощи
    "flag_time":"[FLAG_TIME]", - Время
    "answers_count":"[ANSWER_COUNT]", - Количество ответов
}
                </pre>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": 1,
    "user_id": 1,
    "lesson_id": 1,
    "lang_id": 1,
    "translate_id": 2,
    "stars_total": 1,
    "created_at": 1522926930,
    "updated_at": 1522926930,
    "points": 1,
    "flag_help": 1,
    "flag_time": 1,
    "answers_count": 1
}
                </pre>

            </div>
        </div>

        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/completed-user-lesson/1
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": 1,
    "user_id": 1,
    "lesson_id": 1,
    "lang_id": 1,
    "translate_id": 2,
    "stars_total": 1,
    "created_at": 1522926930,
    "updated_at": 1522926930,
    "points": 1,
    "flag_help": 1,
    "flag_time": 1,
    "answers_count": 1
}
                </pre>

            </div>
        </div>


    </div>
</div>