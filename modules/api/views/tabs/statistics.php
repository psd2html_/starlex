<div class="row routes">
    <div class="col-xs-12">
        <div class="danger">
            Надо заполнить с разработчиками
        </div>
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/statistics</a>: получение статистики (за последние 27 календарных дней)
            </li>
            <li>GET
                <a href="#">/api/v1/statistics/info</a>: получение информации о пользователе: количеcтво выученных слов, количество
                пройденых уроков, xp - пользователя, рейтинг пользователя относительно всех пользователей, ТОП.
            </li>
            <li>POST
                <a href="#">/api/v1/statistics/create</a>: сохранение статистики на сервере;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>time</td>
                        <td>Int</td>
                        <td>Дневная норма времени (в секундах)</td>
                    </tr>
                    <tr>
                        <td>date</td>
                        <td>string</td>
                        <td>Дата</td>
                    </tr>
                    <tr>
                        <td>active_time</td>
                        <td>Int</td>
                        <td>Активное время (в секундах)</td>
                    </tr>
                    <tr>
                        <td>lang_id</td>
                        <td>Int</td>
                        <td>ID языка</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <h2>Получение информации</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/statistics/info
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
                    {
                        "words": 301, // количество пройденных слов
                        "lessons": 37, // количество пройденных уроков
                        "xp": 1332, // рейтинг пользователя
                        "xp_all": 66.7 // рейтинг пользователя относительно всех пользователей
                    }
                </pre>

            </div>
        </div>


        <h2>Получение статистики</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/statistics
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
                    {
                        "id": "1",
                        "time": "120",
                        "date": "2018-12-16 13:44:08",
                        "active_time": "40",
                        "lang_id": "2"
                    },
                    {
                        "id": "2",
                        "time": "100",
                        "date": "2018-11-20 13:48:44",
                        "active_time": "60",
                        "lang_id": "3"
                    }
                </pre>

            </div>
        </div>

        <h2>Сохранение статистики на сервере</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">POST</span> - /api/v1/statistics/create
                </h4>
            </div>
            <div class="panel-heading">
                <h4>Параметры (дата сохраняется автоматически в БД)</h4>
                <pre>
                    {
                      "time": 200,
                      "active_time": 20,
                      "date": "2019-01-11 18:48:56",
                      "lang_id": 2
                    }
                </pre>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
                    {
                        "time": "202",
                        "active_time": "21",
                        "date": "2019-01-11 18:48:56",
                        "lang_id": "2",
                        "user_id": 1,
                        "id": 6
                    }
                </pre>

            </div>
        </div>

    </div>
</div>