<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/theme</a>: получение списка тем (сущности, без переводов)
            </li>
            <li>GET
                <a href="#">/api/v1/theme/1</a>: получение информации по конкретной теме с id равным 1;
            </li>
            <li>GET
                <a href="#list">/api/v1/theme/list-theme</a>: получение списка тем с переводом;
            </li>
            <li>GET
                <a href="#get">/api/v1/theme/get-theme</a>: получение темы по ID;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>parent_id</td>
                        <td>Int</td>
                        <td>ID родителя (в этой же таблице)</td>
                    </tr>
                    <tr>
                        <td>status</td>
                        <td>Int</td>
                        <td>0-отключена;1-активна</td>
                    </tr>
                    <tr>
                        <td>name</td>
                        <td>string</td>
                        <td>название темы</td>
                    </tr>
                    <tr>
                        <td>pos_row</td>
                        <td>Int</td>
                        <td>Положение подтемы по Y</td>
                    </tr>
                    <tr>
                        <td>pos_col</td>
                        <td>Int</td>
                        <td>Положение подтемы по X</td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/theme/1
                </h4>
            </div>
            <div class="panel-heading">
                <h4>Параметры
                </h4>
                <pre>
{
    "lang_id":"[LANG_ID]", - default 1 (получить методом <a href="#tkt-language" data-toggle="tab" aria-expanded="true">/api/v1/language</a>)
}
                </pre>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": "1",
    "parent_id": "1",
    "status": "1",
    "pos_row": "1",
    "pos_col": "1",
    "created_at": "1528544734",
    "updated_at": "1528544734",
    "name": "Знакомимся"
}
                </pre>

            </div>
        </div>

        <div id="list">
        <h2>Полученить список тем с переводом</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/theme/list-theme
                </h4>
            </div>
            <div class="panel-heading">
                <h4>Параметры
                </h4>
                <pre>
{
    "lang_id":"[LANG_ID]", - default 1 (получить методом <a href="#tkt-language" data-toggle="tab" aria-expanded="true">/api/v1/language</a>)
    "translate_id":"[TRANSLATE_ID]", - default 2 (получить методом <a href="#tkt-language" data-toggle="tab" aria-expanded="true">/api/v1/language</a>)
}
                </pre>
            </div>

            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
[
    {
        "id": "1",
        "name": "Первые Слова",
        "translate": "English (USA)"
    },
    {
        "id": "3",
        "name": "Вторые слова",
        "translate": "Second words"
    }
]
                </pre>

            </div>
        </div>
        </div>

        <div id="get">
        <h2>Получить тему по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/theme/get-theme
                </h4>
            </div>
            <div class="panel-heading">
                <h4>Можно выбирать любую тему, как Тему так и Подтему, в случае выбора Темы, в ответе будет полный массив с подтемами, соответственно задав ИД подтемы, получим только подтему с уроками</h4>
            </div>
            <div class="panel-heading">
                <h4>Параметры
                </h4>
                <pre>
{
    "id":"[ID]", - Ид темы или подтемы (обязательный параметр)
    "lang_id":"[LANG_ID]", - по умолчанию первый язык
    "translate_id":"[TRANSLATE_ID]", - по умолчанию второй язык
}
                </pre>
            </div>

            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
[
    {
        "id": "1",
        "name": "Первые Слова",
        "translate": "English (USA)",
        "progress": {
            "stars_total": 9,
            "points": 80,
            "flag_help": 1,
            "flag_time": 2,
            "answers_count": 6
        },
        "sub_themes": [
            {
                "id": "2",
                "name": "Знакомимся",
                "pos_row": "1",
                "pos_col": "1",
                "translate": "Let's Get to Know Each Other (USA)",
                "progress": {
                    "stars_total": 4,
                    "points": 30,
                    "flag_help": 1,
                    "flag_time": 2,
                    "answers_count": 3
                },
                "lessons": [
                    {
                        "id": "1",
                        "theme_id": "2",
                        "time_passage": "0",
                        "lives_count": "0",
                        "points_count": "0",
                        "name": "Знакомство 1",
                        "translate": "Introduction 1",
                        "progress": {
                            "id": "3",
                            "user_id": "1",
                            "lesson_id": "1",
                            "stars_total": "3",
                            "created_at": "1524891676",
                            "updated_at": "1524891676",
                            "points": "20",
                            "flag_help": "0",
                            "flag_time": "1",
                            "answers_count": "2",
                            "theme_id": "2"
                        },
                        "words": [
                            {
                                "id": "1",
                                "name": "имя",
                                "translate": "first name",
                                "lesson_id": "1",
                                "word_group_id": "1",
                                "word_group_name": "name",
                                "bad_translate": "last name",
                                "image": "/uploads/images/words/0019-001.jpg",
                                "media": "/uploads/media/words/en/0019-001.mp3"
                            },
                            {
                                "id": "2",
                                "name": "фамилия",
                                "translate": "last name",
                                "lesson_id": "1",
                                "word_group_id": "2",
                                "word_group_name": "surname",
                                "image": "/uploads/images/words/0019-002.jpg",
                                "media": "/uploads/media/words/en/0019-002.mp3"
                            },
                    ...
                </pre>

            </div>
        </div>
        </div>
    </div>
</div>