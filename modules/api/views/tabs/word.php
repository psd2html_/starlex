<div class="row routes">
    <div class="col-xs-12">
        <h2>Methods:</h2>
        <ul>
            <li>GET
                <a href="#">/api/v1/word</a>: получение списка слов
            </li>
            <li>GET
                <a href="#">/api/v1/word/1</a>: получение информации по конкретному слову с id равным 1;
            </li>
        </ul>

        <h2>Поля</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Схема таблицы</div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        <td>Int</td>
                        <td>Primary ID</td>
                    </tr>
                    <tr>
                        <td>lang_id</td>
                        <td>Int</td>
                        <td>ID языка</td>
                    </tr>
                    <tr>
                        <td>word_group_id</td>
                        <td>Int</td>
                        <td>ID группы переводов одного слова</td>
                    </tr>
                    <tr>
                        <td>status</td>
                        <td>Int</td>
                        <td>0-отключено;1-активно</td>
                    </tr>
                    <tr>
                        <td>name</td>
                        <td>string</td>
                        <td>Слово</td>
                    </tr>
                    <tr>
                        <td>description</td>
                        <td>string</td>
                        <td>Описание</td>
                    </tr>
                    <tr>
                        <td>is_word_collocation</td>
                        <td>boolean</td>
                        <td>Является ли слово словосочетанием</td>
                    </tr>
                    <tr>
                        <td>created_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>updated_at</td>
                        <td>Timestamp</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <h2>Получить данные по ID</h2>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>
                    <span class="label label-info">GET</span> - /api/v1/word/1
                </h4>
            </div>
            <div class="panel-body">
                <h4>Ответ</h4>
                <pre>
{
    "id": 1,
    "parent_id": 0,
    "word_group_id": 3,
    "name": "Первые Слова",
    "status": 1,
    "description": "",
    "is_word_collocation": true,
    "created_at": 1502188368,
    "updated_at": null
}
                </pre>

            </div>
        </div>

    </div>
</div>