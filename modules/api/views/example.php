<h3>Получить список всех пользователей</h3>
<pre>GET: /api/v1/user - all users
GET: /api/v1/user/2 - юзер с id=2
</pre>
<!--
<h4>search and filtering</h4>
<pre>{"name":"alex", "age":"25"} — WHERE name='alex' AND age=25
[{"name":"alex"}, {"age":"25"}]  WHERE name='alex' OR age=25
</pre>
<p>The comparison operator is intelligently determined based on the   first few characters in the given value. In particular, it recognizes   the following operators if they appear as the leading characters in the   given value:</p>
<ul>
    <li>&lt;: the column must be less than the given value.</li>
    <li>&gt;: the column must be greater than the given value.</li>
    <li>&lt;=: the column must be less than or equal to the given value.</li>
    <li>&gt;=: the column must be greater than or equal to the given value.</li>
    <li>&lt;&gt;: the column must not be the same as the given value.</li>
    <li>=: the column must be equal to the given value.</li>
</ul>
<p>Examples:</p>
<pre>GET: /users?filter={"name":"alex"} — user with name alex
GET: /users?filter={"name":"alex", "age":"&gt;25"} — user with name alex AND age greater than 25
GET: /users?filter=[{"name":"alex"}, {"name":"dmitry"}] — user with name alex OR dmitry
GET: /users?search={"name":"alex"} — user with name contains the substring alex (alexey, alexander, alex)
</pre>
<h4>relations</h4>
<pre>GET: /user/1?with=comments,posts — get user data with comments and posts array (comma separated list of relations in `with` GET parameter)
{
"id":"1",
"first_name":"Alex",
"comments":[{"id":"1","text":"..."},{"id":"2","text":"..."}],
"posts":[{"id":"1","content":"..."}],{"id":"2","content":"..."}],
...
}
</pre>
-->
<h3>Удаление пользователя</h3>
<pre>DELETE: /api/v1/user/42 - удалить юзера с ID = 42
DELETE: /api/v1/user  - удалить всех пользователей
DELETE: /api/v1/user?filter={"firstname":"Alex"} - delete users with name 'Alex'
</pre>
<h3>Создать пользователя</h3>
<pre>POST: /api/v1/user - создать нового пользователя  </pre>
<p>POST параметры:</p>
<pre>[
{"username":"test", "email":"test@gmail.com", "password":"123456"}
]
</pre>

<h3>Изменить/обновить запись</h3>
<pre>PUT: /user/42 - обновить юзера с ID = 42  </pre>
<h4>Обновить коллекцию</h4>
<pre>PUT: /user  </pre>
<p>POST параметры:</p>
<pre>[
{"id":"1","firstname":"Ivan"},
{"id":"2","firstname":"Vova"}
]
</pre>
<p>update users with id 1 and 2</p>
<!--                    <h3>limit, offset, order</h3>-->
<!--        <pre>GET: /users/?offset=10&amp;limit=10-->
<!--GET: /users/?order=id DESC-->
<!--GET: /users/?order=id ASC-->
<!--GET: /users/?order=parent_id ASC,ordering ASC-->
<!--GET: /users/?order=comment.id&amp;with=comment-->
<!--        </pre>-->
<!--                    <h3>Response format</h3>-->
<!--                    <p>By default response is sent in the format of JSON. To change the format of response pass format GET parameter with value xml</p>-->
<!--                    <pre>GET: /users?format=xml  </pre>-->


<h2>Example CURL console:</h2>

<!--
<pre>
$filename = __DIR__.'/example.jpg';

$post_params = array();
$post_params['firstname'] = 'Title';
$post_params['lastname'] = 'Description';
...
$post_params['image'] = '@'.$filename; // upload file

$post_params = json_encode($post_params); // json encode data

$api_url = 'http://www.example.com/api/v1/client';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $api_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'X-Username: jessika@gmail.com',
'X-Password: 12345',
'Content-Length: ' . strlen($post_params)
));
$result = curl_exec($ch);
curl_close($ch);

echo $result;
</pre>
-->
<pre>
curl -i -H "Accept:application/json" "http://[DOMAIN]/api/v1/user" -d '{"access-token":"imw2UaMVzGy3n1jIPLIY5XzrU9XjJQzo"}'
</pre>

<!--
<h2>Example jQuery script for testing API:</h2>
<pre>$.ajax({
url: "http://www.example.com/index.php",
dataType: "json",
type: "GET",
beforeSend: function(xhr) {
xhr.setRequestHeader("X-Username", "jessika@gmail.com");
xhr.setRequestHeader("X-Password", "12345");
console.log(xhr);
},
success: function(data, textStatus, XMLHttpRequest) {
alert(data);
},
error: function(XMLHttpRequest, textStatus, errorThrown) {
console.log(XMLHttpRequest);
}
})
</pre>
-->

<h2>Вывод с конкретными полями</h2>
<pre>
/api/v1/user?fields=id,email
</pre>
Используя параметры fields и expand в URL, можно указать, какие поля должны быть включены в результат.
Например, по адресу http://localhost/users?fields=id,email мы получим информацию по пользователям, которая будет содержать только id и email