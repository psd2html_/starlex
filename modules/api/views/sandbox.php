<?php

use app\modules\api\helpers\RouteControllersHelper;
use trntv\aceeditor\AceEditor;
use yii\helpers\Html;
use yii\helpers\Json;

$userID = Yii::$app->user->identity;

?>
<h2>Sandbox:</h2>
<form action="#" id="form-api" class="form-horizontal">
    <div class="form-group">
        <label for="url" class="col-sm-1 control-label">API URL</label>
        <div class="col-sm-3">
            <?php
            $apiRoutes = new RouteControllersHelper();

            $routes = array_keys($apiRoutes->controllers);
            echo Html::dropDownList('api_methods', null, array_combine($routes, $routes), [
                'class' => 'form-control api-url',
                'id'    => 'url-select',
            ]);

            ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-1 col-sm-11">
            <?= Html::input('text', 'api_method', null, [
                'class'    => 'form-control api-url',
                'id'       => 'url',
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="method" class="col-sm-1 control-label">Method</label>
        <div class="col-sm-3">
            <select class="form-control" id="method">
                <option value="GET">GET</option>
                <option value="POST">POST</option>
                <option value="PUT">PUT</option>
                <option value="DELETE">DELETE</option>
                <option value="OPTIONS">OPTIONS</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="params" class="col-sm-1 control-label" onclick="jQuery('#params').select()">Params</label>
        <div class="col-sm-11">
            <?= AceEditor::widget([
                'name'             => 'params',
                'mode'             => 'json', // programing language mode. Default "html"
                'theme'            => 'github', // editor theme. Default "github"
                'options'          => [
                    'class' => 'form-control',
                    'id'    => 'params',
                ],
                'containerOptions' => [
                    'style' => 'width: 100%; height: 100px;',
                    'rows'  => 2,
                ],
                'value'            => Json::encode([
                    'access-token' => Yii::$app->user->identity->auth_key,
                ]),
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-1 col-sm-11">
            <button class="btn btn-success">Make Request</button>
        </div>
    </div>
    <div class="form-group">
        <label for="response" class="col-sm-1 control-label" rows="1" cols="100" onclick="jQuery('#response').select()">Response</label>
        <div class="col-sm-11">
            <?= AceEditor::widget([
                'id'               => 'response_ace',
                'name'             => 'params',
                'mode'             => 'json', // programing language mode. Default "html"
                'theme'            => 'github', // editor theme. Default "github"
                'options'          => [
                    'class' => 'form-control',
                    'id'    => 'response',
                ],
                'containerOptions' => [
                    'style' => 'width: 100%; height: 300px;',
                    'rows'  => 2,
                ]
            ]); ?>
        </div>
    </div>
</form>