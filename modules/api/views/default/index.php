<?php

/**
 * @var yii\web\View $this
 */

$this->title = 'API v1.0 documentation';

?>

<section class="content background-white">
    <div class="row">
        <div class="col-xs-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#usage" aria-controls="usage" role="tab" data-toggle="tab" aria-expanded="true">Описание API</a>
                </li>
                <li role="presentation">
                    <a href="#examples" aria-controls="examples" role="tab" data-toggle="tab" aria-expanded="false">Примеры</a>
                </li>
                <li role="presentation">
                    <a href="#sandbox" aria-controls="sandbox" role="tab" data-toggle="tab" aria-expanded="false">Sandbox</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="usage">
                    <h2>Сущности</h2>

                    <div class="row">
                        <div class="col-xs-3"> <!-- required for floating -->
                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a href="#tkt-common" data-toggle="tab" aria-expanded="true">Общее</a></li>
                                <li><a href="#tkt-user" data-toggle="tab" aria-expanded="true">Пользователи</a></li>

                                <li><a href="#tkt-language" data-toggle="tab" aria-expanded="true">Языки</a></li>
                                <li><a href="#tkt-word-group" data-toggle="tab" aria-expanded="true">Группы слов</a></li>
                                <li><a href="#tkt-word" data-toggle="tab" aria-expanded="true">Слова</a></li>
                                <!--<li><a href="#tkt-word-translate" data-toggle="tab" aria-expanded="true">Переводы слов</a></li>-->
                                <li><a href="#tkt-lesson" data-toggle="tab" aria-expanded="true">Уроки</a></li>
                                <li><a href="#tkt-lesson-translate" data-toggle="tab" aria-expanded="true">Переводы уроков</a></li>
                                <li><a href="#tkt-theme" data-toggle="tab" aria-expanded="true">Темы</a></li>
                                <li><a href="#tkt-theme-language" data-toggle="tab" aria-expanded="true">Переводы тем</a></li>
                                <li><a href="#tkt-lesson-word" data-toggle="tab" aria-expanded="true">Слова темы</a></li>
                                <li><a href="#tkt-lesson-word-bad-translate" data-toggle="tab" aria-expanded="true">Неправильные переводы уроков слов</a></li>
                                <li><a href="#tkt-lesson-image" data-toggle="tab" aria-expanded="true">Картинки</a></li>
                                <li><a href="#tkt-lesson-media" data-toggle="tab" aria-expanded="true">Медиа</a></li>
                                <li><a href="#tkt-repetition" data-toggle="tab" aria-expanded="true">Повторения</a></li>
                                <li><a href="#tkt-statistics" data-toggle="tab" aria-expanded="true">Статистика</a></li>
                                <li><a href="#tkt-lesson-completed" data-toggle="tab" aria-expanded="true">Завершённые уроки</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-9">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tkt-common">
                                    <?php include(__DIR__ . '/../tabs/common.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-user">
                                    <?php include(__DIR__ . '/../tabs/user.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-language">
                                    <?php include(__DIR__ . '/../tabs/language.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-theme">
                                    <?php include(__DIR__ . '/../tabs/theme.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-theme-language">
                                    <?php include(__DIR__ . '/../tabs/theme-language.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-word-group">
                                    <?php include(__DIR__ . '/../tabs/word-group.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-word">
                                    <?php include(__DIR__ . '/../tabs/word.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-word-translate">
                                    <?php include(__DIR__ . '/../tabs/word-translate.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-lesson">
                                    <?php include(__DIR__ . '/../tabs/lesson.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-lesson-translate">
                                    <?php include(__DIR__ . '/../tabs/lesson-translate.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-lesson-word">
                                    <?php include(__DIR__ . '/../tabs/lesson-word.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-lesson-word-bad-translate">
                                    <?php include(__DIR__ . '/../tabs/lesson-word-bad-translate.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-lesson-image">
                                    <?php include(__DIR__ . '/../tabs/lesson-image.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-lesson-media">
                                    <?php include(__DIR__ . '/../tabs/lesson-media.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-repetition">
                                    <?php include(__DIR__ . '/../tabs/repetition.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-statistics">
                                    <?php include(__DIR__ . '/../tabs/statistics.php'); ?>
                                </div>
                                <div class="tab-pane" id="tkt-lesson-completed">
                                    <?php include(__DIR__ . '/../tabs/completed-user-lessons.php'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--//.tab-pane -->


                <div role="tabpanel" class="tab-pane" id="examples">
                    <?php include(__DIR__ . '/../example.php'); ?>
                </div>

                <div role="tabpanel" class="tab-pane" id="sandbox">
                    <?php include(__DIR__ . '/../sandbox.php'); ?>
                </div>

            </div>

        </div><!-- /.col-xs-12 -->
    </div>
</section>