<?php

namespace app\modules\api\helpers;

use yii\base\Component;
use yii\base\Exception;
use yii\base\Module;
use yii\helpers\Inflector;
use yii\web\Controller;

/**
 * Хелпер для отображения списка активных роутов API
 *
 * Class MyHelper
 *
 * @package app\helpers
 *
 * @property void  $routesByController
 * @property array $apiControllers
 */
class RouteControllersHelper extends Component
{
    /**
     * @var Controller[]
     */
    public $controllers = [];

    public $routes = [];
    /**
     * @var Module
     */
    private $module;
    /**
     * @var bool
     */
    public  $autoInit;

    public function __construct($config = [], Module $module = null, $autoInit = true)
    {
        $this->module = $module ?? \Yii::$app->getModule('api');
        $this->autoInit = $autoInit;
        parent::__construct($config);
    }

    public function init()
    {
        if ($this->autoInit){
            $this->getApiControllers();
        }
    }

    /**
     * Получение всех роутов контроллеров модуля
     *
     * @param string $prefix
     *
     * @return Controller[]
     * @throws Exception
     * @internal param Module $this->module
     * @internal param array $result
     */
    public function getApiControllers($prefix = '')
    {
        $namespace = trim($this->module->controllerNamespace, '\\') . '\\';

        return $this->getControllerFiles($namespace, $prefix);
    }

    /**
     * Get list controller under module
     *
     * @param string $namespace
     * @param string $prefix
     *
     * @return Controller[]
     * @throws Exception
     * @internal param Module $module
     */
    private function getControllerFiles($namespace, $prefix)
    {
        $path = @\Yii::getAlias('@' . str_replace('\\', '/', $namespace));
        if (!is_dir($path)){
            $errorMessage = "Не найдена папка: \"{$path}\"";
            \Yii::error($errorMessage, __METHOD__);

            throw new Exception($errorMessage);
        }

        foreach (scandir($path) as $file){
            if ($file == '.' || $file == '..'){
                continue;
            }
            if (is_dir($path . '/' . $file)){
                $this->getControllerFiles($namespace . $file . '\\', $prefix . $file . '/');
            } elseif (strcmp(substr($file, -14), 'Controller.php') === 0) {
                $this->getControllerAsClass($namespace, $prefix, $file);
            }
        }

        return $this->controllers;
    }

    private function getControllerAsClass($namespace, $prefix, $file)
    {
        $id = Inflector::camel2id(substr(basename($file), 0, -14));
        $className = $namespace . Inflector::id2camel($id) . 'Controller';
        if (strpos($className, '-') === false && class_exists($className) && is_subclass_of($className, 'yii\base\Controller')){
            $this->filterControllers($className, $prefix . $id);
        }
    }

    /**
     * @param mixed  $type
     * @param string $id
     * @param array  $result
     *
     * @internal param Module $this ->module
     */
    private function filterControllers($type, $id)
    {
        // Подавление ошибки нужно, если, к примеру какой-то контроллер просит инициализировать переменную.
        try {
            /* @var $controller \app\modules\api\components\ApiController */
            $controller = \Yii::createObject($type, [$id, $this->module]);
            if (property_exists($controller, 'isVisible')){
                if ($controller->isVisible){
                    $this->getActionRoutes($controller);
                    $this->controllers['/' . $controller->uniqueId] = $controller;
                }
            }
        } catch (\Exception $tryError){
            \Yii::error($tryError->getMessage(), __METHOD__);
        }
    }

    /**
     * Get route of action
     *
     * @param \yii\base\Controller $controller
     * @param array                $result all controller action.
     */
    private function getActionRoutes($controller)
    {
        $excludeActions = [
            'index',
            'create',
            'view',
            'update',
            'delete',
            'options',
        ];
        try {
            $prefix = '/' . $controller->uniqueId . '/';
            foreach ($controller->actions() as $id => $value){
                if (!in_array($id, $excludeActions)){
                    $this->controllers[] = $prefix . $id;
                }
            }
            $class = new \ReflectionClass($controller);
            foreach ($class->getMethods() as $method){
                $name = $method->getName();
                if ($method->isPublic() && !$method->isStatic() && strpos($name, 'action') === 0 && $name !== 'actions'){
                    $this->controllers[$prefix . Inflector::camel2id(substr($name, 6))] = $controller;
                }
            }
        } catch (\Exception $exc){
            \Yii::error($exc->getMessage(), __METHOD__);
        }
    }

    public function getRoutesByController()
    {
//        foreach ($this->controllers as ){
//
//        }

    }
}