<?php

namespace app\modules\api;

use Yii;

/**
 * Class Module API
 *
 * @package app\modules\api
 */
class Module extends \yii\base\Module
{
    /** @var  yii\web\Response */
    public $response;

    public $controllerNamespace = 'app\modules\api\controllers';


    public function init()
    {
        parent::init();
        // делаем свой конфиг для модуля
        \Yii::configure($this, require(__DIR__ . '/config/api.config.php'));
    }
}