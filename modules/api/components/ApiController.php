<?php

namespace app\modules\api\components;

use yii\filters\AccessControl;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

/**
 * Class ApiController
 *
 * @property \app\modules\api\Module $module
 *
 * @package app\modules\api\rest
 */
class ApiController extends ActiveController
{
    /**
     * @var bool Отображать ли этот роут в списке роутов
     */
    public $isVisible = true;

    public function init()
    {
        parent::init();

        \Yii::$app->user->enableSession = false;
        \Yii::$app->user->enableAutoLogin = false;

        // переопределяем/делаем свои настройки для своего модуля `api`
        $this->serializer = [
            'class'    => 'yii\rest\Serializer',
            'response' => $this->module->response
            // также и с request, если необходимо
        ];
    }

    public function behaviors()
    {
        $request = \Yii::$app->request;
        $bodyParams = \Yii::$app->request->bodyParams;
        $request->get("access-token");
        if (!isset($_GET["access-token"]) && isset($bodyParams["access-token"])){
            $_GET["access-token"] = $bodyParams["access-token"];
        }

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['authMethods'] = [
            HttpBasicAuth::class,
            HttpBearerAuth::class,
            QueryParamAuth::class,
        ];
        $behaviors['authenticator']['response'] = $this->module->response; // берем настройки из своего конфига модуля
        //$behaviors['authenticator']['except'] = ['options'];
        $behaviors['authenticator']['request'] = $request;
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['contentNegotiator']['formatParam'] = 'format';
        $behaviors['contentNegotiator']['languageParam'] = 'lang';

        return $behaviors;
    }


    public function verbs()
    {
        return [
            'index'  => ['get', 'options'],
            'view'   => ['get', 'options'],
            'create' => ['post', 'options'],
            'update' => ['put', 'patch'],
            'delete' => ['delete'],
        ];
    }
}