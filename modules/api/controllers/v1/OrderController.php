<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `TariffOrder`
 *
 * Class OrderController
 *
 * @package app\modules\api\controllers
 */
class OrderController extends ApiController
{
    public $modelClass = 'app\models\TariffOrder';

    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }

    /**
     * Передача данных о покупке
     */
    public function setPurchase()
    {

    }
}