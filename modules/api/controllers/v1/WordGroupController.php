<?php

namespace app\modules\api\controllers\v1;

use app\models\WordGroup;
use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `WordGroup`
 *
 *  Поскольку клиентское приложение не имеет возможности менять темы
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class WordController
 *
 * @property \app\models\WordGroup $modelClass
 *
 * @package app\modules\api\controllers
 */
class WordGroupController extends ApiController
{
    public $modelClass = WordGroup::class;
    public $isVisible  = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}