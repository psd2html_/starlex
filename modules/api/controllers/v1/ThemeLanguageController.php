<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `ThemeLanguage`
 *
 *  Поскольку клиентское приложение не имеет возможности менять данные
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class ThemeLanguageController
 *
 * @property \app\models\ThemeTranslate $modelClass
 *
 * @package app\modules\api\controllers
 */
class ThemeLanguageController extends ApiController
{
    public $modelClass = 'app\models\ThemeTranslate';
    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}