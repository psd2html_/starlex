<?php

namespace app\modules\api\controllers\v1;

use app\models\Lesson;
use app\modules\api\components\ApiController;
use Yii;
/**
 * API для работы с ресурсом `Lessons`
 *
 *  Поскольку клиентское приложение не имеет возможности менять данные
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class LessonsController
 *
 * @property \app\models\Lesson $modelClass
 *
 * @package app\modules\api\controllers
 */
class LessonController extends ApiController
{
    public $modelClass = 'app\models\Lesson';
    public $isVisible = true;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        return $actions;
    }


    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }

    public function actionIndex()
    {
        $get = Yii::$app->request->get();
        $lang_id = 1;
        if(isset($get["lang_id"])){
            $lang_id = $get["lang_id"];
        }

        $query = Lesson::find()->select(['lesson.id', 'lesson.theme_id', 'l.lang_id', 'l.translate name', 'lesson.status', 'lesson.time_passage', 'lesson.created_at', 'lesson.updated_at'])->joinWith('lang l')->where(['l.lang_id' => $lang_id]);

        $lesson = $query->createCommand()->queryAll();

        return $lesson;
    }

    public function actionView($id)
    {
        $get = Yii::$app->request->get();
        $lang_id = 1;
        if(isset($get["lang_id"])){
            $lang_id = $get["lang_id"];
        }

        $query = Lesson::find()->select(['lesson.id', 'lesson.theme_id', 'l.lang_id', 'l.translate name', 'lesson.status', 'lesson.time_passage', 'lesson.created_at', 'lesson.updated_at'])->joinWith('lang l')->where(['lesson.id' => $id,'l.lang_id' => $lang_id]);

        $lesson = $query->createCommand()->queryOne();

        return $lesson;
    }
}