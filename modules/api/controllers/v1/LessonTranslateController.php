<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `lesson_translate`
 *
 *  Поскольку клиентское приложение не имеет возможности менять данные
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class LessonTranslateController
 *
 * @property \app\models\LessonTranslate $modelClass
 *
 * @package app\modules\api\controllers
 */
class LessonTranslateController extends ApiController
{
    public $modelClass = 'app\models\LessonTranslate';
    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}