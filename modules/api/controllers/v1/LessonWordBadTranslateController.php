<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `LessonWordBadTranslate`
 *
 *  Поскольку клиентское приложение не имеет возможности менять данные
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class LessonsWordsController
 *
 * @property \app\models\LessonWordBadTranslate $modelClass
 *
 * @package app\modules\api\controllers
 */
class LessonWordBadTranslateController extends ApiController
{
    public $modelClass = 'app\models\LessonWordBadTranslate';
    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}