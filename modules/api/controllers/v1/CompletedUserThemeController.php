<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `CompletedUserTheme`
 *
 * Методы: [GET, POST, DELETE, PUT/PATCH]
 *
 * Результат: получение данных в JSON
 *
 * Class CompletedUserThemeController
 *
 * @package app\modules\api\controllers
 */
class CompletedUserThemeController extends ApiController
{
    public $modelClass = 'app\models\CompletedUserTheme';
    public $isVisible = true;

}