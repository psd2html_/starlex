<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `Award`
 *
 *  Поскольку клиентское приложение не имеет возможности менять тарифы, отпадает необходимость
 *  в реализации методов POST, DELETE, PUT/PATCH
 *
 * Class AwardController
 *
 * @property \app\models\Award $modelClass
 *
 * @package app\modules\api\controllers
 */
class AwardController extends ApiController
{
    public $modelClass = 'app\models\Award';

    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}