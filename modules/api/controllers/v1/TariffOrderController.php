<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `TariffOrder`
 *
 * Методы: [GET, POST, DELETE, PUT/PATCH]
 *
 * Результат: получение данных в JSON
 *
 * Class TariffOrderController
 *
 * @package app\modules\api\controllers
 */
class TariffOrderController extends ApiController
{
    public $modelClass = 'app\models\TariffOrder';

    public $isVisible = true;
}