<?php

namespace app\modules\api\controllers\v1;

use app\models\LessonImage;
use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `LessonImage`
 *
 *  Поскольку клиентское приложение не имеет возможности менять темы
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class LessonsImagesController
 *
 * @property \app\models\LessonImage $modelClass
 *
 * @package app\modules\api\controllers
 */
class LessonImageController extends ApiController
{
    public $modelClass = LessonImage::class;
    public $isVisible  = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}