<?php

namespace app\modules\api\controllers\v1;

use Yii;
use app\models\CompletedUserLesson;
use app\models\LessonWord;
use app\models\Statistics;
use app\modules\api\components\ApiController;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

/**
 * API для работы с ресурсом `Statistics`
 *
 *  Поскольку клиентское приложение не имеет возможности менять темы
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class StatisticsController
 *
 * @property \app\models\Statistics $modelClass
 *
 * @package app\modules\api\controllers
 */
class StatisticsController extends ApiController
{
    public $modelClass = 'app\models\Statistics';
    public $isVisible = true;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['create']);

        return $actions;
    }

    public function verbs()
    {
        return [
            'index' => ['get'],
            'create'  => ['post'],
        ];
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $query = Statistics::find()->select(['id', 'time', 'date', 'active_time', 'lang_id'])->where(['user_id' => Yii::$app->user->identity->getId()])
            ->andWhere('date > curdate() - interval 27 day');

        $arr = $query->createCommand()
            ->queryAll();

        return $arr;
    }


    /**
     * @return mixed
     */
    public function actionInfo()
    {
        $user_id = Yii::$app->user->identity->getId();

        $models = CompletedUserLesson::find()->where(['user_id' => $user_id])->all();

        $words = [];
        foreach ($models as $model) {
            $words[] = LessonWord::find()->where(['lesson_id' => $model->lesson_id])->count();
        }

        $res['words'] = array_sum($words); // количество пройденных слов
        $res['lessons'] = count($models); // количество пройденных уроков

        $users = CompletedUserLesson::find()->select('user_id')->distinct()->all();
        $arr_users = [];
        foreach ($users as $user) {
            $points = CompletedUserLesson::find()->select(['points'])->where(['user_id' => $user->user_id])->all();
            $arr_users[$user->user_id] = array_sum(ArrayHelper::getColumn($points, 'points'));
        }
        arsort($arr_users);

        if(isset($arr_users[$user_id])) {
            $res['xp'] = $arr_users[$user_id]; // рейтинг пользователя
            $res['xp_all'] = 100 - round((array_search($user_id, array_keys($arr_users)) + 1) / count($arr_users) * 100, 1); // рейтинг пользователя относительно всех пользователей
        } else {
            $res['xp'] = 0; // рейтинг пользователя
            $res['xp_all'] = 0; // рейтинг пользователя относительно всех пользователей
        }

        return $res;
    }


    /**
     * @return Statistics
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new Statistics();

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->user_id = Yii::$app->user->identity->getId();
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
}