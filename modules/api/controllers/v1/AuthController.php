<?php

namespace app\modules\api\controllers\v1;

use app\components\eauth\services\extended\FacebookOAuth2Service;
use app\components\eauth\services\extended\GoogleOAuth2Service;
use app\components\SocialSignup;
use app\modules\api\components\ApiController;
use app\modules\api\models\LoginForm;
use app\modules\api\models\PasswordResetForm;
use app\modules\api\models\SignupForm;
use Yii;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\models\UserSocialAccount;
use yii\authclient\OAuth2;
use yii\authclient;
/**
 * Class AuthController
 *
 * @package app\modules\api\controllers
 */
class AuthController extends ApiController
{
    public $modelClass           = 'app\modules\user\models\User';
    public $enableCsrfValidation = false;

    public $isVisible = true;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['authMethods'] = []; // обнуляем
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'actions' => ['login', 'loginsocial', 'signup', 'reset'],
                    'allow'   => true,
                    'roles'   => ['?'],
                ],
            ],
        ];

        return $behaviors;
    }


    // todo: использовать для авторизации для социальных сетей
    //    public function actions()
    //    {
    //        return [
    //            'social' => [
    //                'class' => 'yii\authclient\AuthAction',
    //                'successCallback' => [$this, 'successCallback'],
    //            ],
    //        ];
    //    }


    /**
     * Простая аутентификация пользователя
     *
     * POST /api/auth/login c параметрами {"username":"[USERNAME]","password":"[PASSWORD]"}
     *
     * @return LoginForm|null|\yii\web\IdentityInterface
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        $model->load(Yii::$app->request->bodyParams, '');
        if ($model->auth()){
            return \Yii::$app->user->identity;
        } else {
            return $model;
        }
    }

    /**
     * Регистрация
     *
     * POST /api/auth/signup c параметрами {"username":"[USERNAME]","password":"[PASSWORD]", "email":"[EMAIL]"}
     *
     * @return SignupForm|null|\yii\web\IdentityInterface
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->bodyParams, '')){
            if ($user = $model->signup()){
                return $user;
            }else{
                return $model;
            }
        }
    }


    /**
     * Сброс пароля
     *
     * POST /api/auth/signup c параметрами {"username":"[USERNAME]", "email":"[EMAIL]"}
     *
     * @return ResetPasswordForm|null|\yii\web\IdentityInterface
     */
    public function actionReset()
    {
        $model = new PasswordResetForm();
        if ($model->load(Yii::$app->request->bodyParams, '')){
            if ($user = $model->resetPassword()){
                $response[] = ["message"=>"На указанный электронный адрес отправлен новый пароль."];
                return $response;
            }else{
                return $model;
            }
        }
    }

    // todo: использовать для авторизации для социальных сетей
    //    public function successCallback($client)
    //    {
    //        $attributes = $client->getUserAttributes();
    //        // user login or signup comes here
    //    }

    // Авторизация в соц сети, по средствам передачи готового токена с мобильного устройства
    public function actionLoginsocial()
    {
        $bodyParams = \Yii::$app->request->bodyParams;
        if(!isset($bodyParams["provider"])){
            $errors[] = ["field"=>"provider","message"=>"Необходимо выбрать социальную сеть"];
        }
        if(!isset($bodyParams["token"])){
            $errors[] = ["field"=>"token","message"=>"Необходимо отправить токен из мобильного приложения"];
        }
        if(isset($bodyParams["provider"]) && isset($bodyParams["token"])){
            $profile = new SocialSignup();
            $profile->getProfile($bodyParams["provider"], $bodyParams["token"]);
            if(isset($profile->profile['id'])){
                $find_SocialAccount = UserSocialAccount::findOne([
                    'provider'  => $bodyParams["provider"],
                    'client_id' => $profile->profile['id']
                ]);
                if (!isset($find_SocialAccount->id)) {
                    // создаем пользователя в таблице user
                    $user = new User();
                    $user->username = "".$bodyParams["provider"]."".($profile->profile['id'])."";
                    $user->email = isset($profile->profile['email']) ? ($profile->profile['email']) : null;
                    $user->status = User::STATUS_ACTIVE;
                    if ($user->save()){
                        $SocialAccount = new UserSocialAccount();
                        $SocialAccount->setAttributes($profile->attributes);
                        $SocialAccount->user_id = $user->id;
                        $SocialAccount->provider = $bodyParams["provider"];
                        $SocialAccount->created_at = time();
                        $SocialAccount->save();

                        if(\Yii::$app->user->login($user)){
                            return \Yii::$app->user->identity;
                        }else{
                            $errors[] = ["field"=>"email","message"=>"not auth 1"];
                            return $errors;
                        }
                    }else{
                        if(isset($user->errors["email"])){
                            $errors[] = ["field"=>"email","message"=>"Этот e-mail уже используется"];
                            return $errors;
                        }
                    }
                }else{
                    $find_SocialAccount->setAttributes($profile->attributes);
                    $find_SocialAccount->save();

                    if(\Yii::$app->user->login($find_SocialAccount->user)){
                        return \Yii::$app->user->identity;
                    }else{
                        $errors[] = ["field"=>"email","message"=>"not auth 2"];
                        return $errors;
                    }
                }
            }

        }
        if(count($errors)>0){
            return $errors;
        }
        if(count($profile->errors)>0){
            return $profile->errors;
        }

    }

    /*public function actionLoginsocial()
    {
        // todo: Пробы. Как говорится `это то к чему пришли и застряли`. Может быть подтолкнет тебя на какую-то на мысль.
        //-----------------------------------------------
        //        $username = '[свой логин]';
        //        $password = '[свой пароль]';
        //
        //$client = new authclient\clients\Google();
        //$token = $client->authenticateUser($username, $password);

        //        и ловим такую ошибку:
        //        {
        //              "error": "unsupported_grant_type",
        //              "error_description": "Invalid grant_type: password"
        //         }
        //-----------------------------------------------
    }*/

    /**
     *
     * @return array
     */
    public function verbs()
    {
        return [
            'login'       => ['post'],
            'loginsocial' => ['post'],
            'social'      => ['post'],
            'reset'      => ['post'],
            'signup'      => ['post'],
        ];
    }
}