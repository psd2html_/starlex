<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `Game`
 *
 * Class GamesController
 *
 * @package app\modules\api\controllers
 */
class GameController extends ApiController
{
    public $modelClass = 'app\models\Game';

    public $isVisible = true;
}