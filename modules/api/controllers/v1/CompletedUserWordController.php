<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `CompletedUserWord`
 *
 * Методы: [GET, POST, DELETE, PUT/PATCH]
 *
 * Результат: получение данных в JSON
 *
 * Class CompletedUserWordController
 *
 * @package app\modules\api\controllers
 */
class CompletedUserWordController extends ApiController
{
    public $modelClass = 'app\modules\CompletedUserWord';

    public $isVisible = true;
}