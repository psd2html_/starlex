<?php

namespace app\modules\api\controllers\v1;

use app\models\LessonWord;
use app\models\LessonWordBadTranslate;
use app\models\Word;
use app\models\WordGroup;
use app\modules\api\components\ApiController;
use Yii;
/**
 * API для работы с ресурсом `LessonsWords`
 *
 *  Поскольку клиентское приложение не имеет возможности менять данные
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class LessonsWordsController
 *
 * @property \app\models\LessonWord $modelClass
 *
 * @package app\modules\api\controllers
 */
class LessonWordController extends ApiController
{
    public $modelClass = 'app\models\LessonWord';
    public $isVisible = true;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }


    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }

    public function actionIndex()
    {
        $get = Yii::$app->request->get();
        $lang_id = 1;
        if(isset($get["lang_id"])){
            $lang_id = $get["lang_id"];
        }
        // $query = LessonWord::find()->select(['lesson_word.id', 'lesson_word.lesson_id', 'w.name', 'lesson_word.created_at', 'lesson_word.updated_at' ])->joinWith('word w')->where(['w.lang_id' => $lang_id]);

        $query = LessonWord::find()
            ->select(['lesson_word.id', 'lesson_word.lesson_id', 'w.name', 'bad.id as lesson_world_bad_translate_id', 'lesson_word.created_at', 'lesson_word.updated_at'])
            ->joinWith('lessonWordBadTranslate bad')
            ->joinWith('word w')
            ->where(['w.lang_id' => $lang_id]);

        $lesson = $query->createCommand()->queryAll();

        return $lesson;
    }

    public function actionView($id)
    {
        $get = Yii::$app->request->get();
        $lang_id = 1;
        if(isset($get["lang_id"])){
            $lang_id = $get["lang_id"];
        }

        $query = LessonWord::find()
            ->select(['lesson_word.id', 'lesson_word.lesson_id', 'w.name', 'bad.id as lesson_world_bad_translate_id', 'bad.bad_translate_id as lesson_world_bad_translate_id_word', 'lesson_word.created_at', 'lesson_word.updated_at'])
            ->joinWith('lessonWordBadTranslate bad')
            ->joinWith('word w')
            ->where(['lesson_word.id' => $id, 'w.lang_id' => $lang_id]);

        $lesson = $query->createCommand()->queryOne();

        if(isset($lesson["lesson_world_bad_translate_id_word"])){

            $query = Word::find()
                ->select(['word.*'])
                ->where(["lang_id"=>$lang_id, "word_group_id"=>$lesson["lesson_world_bad_translate_id_word"]]);

            $bad = $query->createCommand()->queryOne();
        }

        //$temp = $lesson['lesson_word.created_at'
        //$temp = $lesson['lesson_word.updated_at'
        $temp = $lesson;
        unset($lesson["created_at"]);
        unset($lesson["updated_at"]);
        unset($lesson["lesson_world_bad_translate_id_word"]);

        $lesson["bad_translate"] = $bad["name"];
        $lesson["created_at"] = $temp["created_at"];
        $lesson["updated_at"] = $temp["updated_at"];

        return $lesson;
    }
}