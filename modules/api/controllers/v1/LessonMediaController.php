<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `LessonsMedia`
 *
 *  Поскольку клиентское приложение не имеет возможности менять темы
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class LessonsMediaController
 *
 * @property \app\models\LessonMedia $modelClass
 *
 * @package app\modules\api\controllers
 */
class LessonMediaController extends ApiController
{
    public $modelClass = 'app\models\LessonMedia';
    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}