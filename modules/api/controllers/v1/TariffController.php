<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `Tariff`
 *
 *  Поскольку клиентское приложение не имеет возможности менять тарифы, отпадает необходимость
 *  в реализации методов POST, DELETE, PUT/PATCH
 *
 * Class TariffController
 *
 * @property \app\models\Tariff $modelClass
 *
 * @package app\modules\api\controllers
 */
class TariffController extends ApiController
{
    public $modelClass = 'app\models\Tariff';

    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}