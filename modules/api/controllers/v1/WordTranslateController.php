<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `WordTranslate`
 *
 *  Поскольку клиентское приложение не имеет возможности менять темы
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class WordTranslateController
 *
 * @property \app\models\WordGroup $modelClass
 *
 * @package app\modules\api\controllers
 */
class WordTranslateController extends ApiController
{
    public $modelClass = 'app\models\WordGroup';
    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}