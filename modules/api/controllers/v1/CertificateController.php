<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `Certificate`
 *
 * Class CertificateController
 *
 * @property \app\models\Certificate $modelClass
 *
 * @package app\modules\api\controllers
 */
class CertificateController extends ApiController
{
    public $modelClass = 'app\models\Certificate';

    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}