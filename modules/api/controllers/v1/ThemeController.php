<?php

namespace app\modules\api\controllers\v1;

use app\models\Language;
use app\models\Theme;
use app\modules\api\components\ApiController;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * API для работы с ресурсом `Theme`
 *
 *  Поскольку клиентское приложение не имеет возможности менять темы
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class ThemeController
 *
 * @property \app\models\Theme $modelClass
 *
 * @package app\modules\api\controllers
 */
class ThemeController extends ApiController
{
    public $modelClass = 'app\models\Theme';
    public $isVisible  = true;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);

        return $actions;
    }

    public function verbs()
    {
        return [
            'index'      => ['get', 'options'],
            'view'       => ['get', 'options'],
            'list-theme' => ['get', 'options'],
            'get-theme'  => ['get', 'options'],
        ];
    }

    public function actionIndex()
    {
        $get = Yii::$app->request->get();
        $lang_id = 1;
        if (isset($get["lang_id"])){
            $lang_id = $get["lang_id"];
        }

        $query = Theme::find()
            ->select(['theme.*', 'l.translate name'])
            ->joinWith('lang l')
            ->where(['l.language_id' => $lang_id]);

        $themes = $query->createCommand()
            ->queryAll();

        return $themes;
    }

    public function actionView($id)
    {
        $get = Yii::$app->request->get();
        $lang_id = 1;
        if (isset($get["lang_id"])){
            $lang_id = $get["lang_id"];
        }

        $query = Theme::find()
            ->select(['theme.*', 'l.translate name'])
            ->joinWith('lang l')
            ->where(['theme.id' => $id, 'l.language_id' => $lang_id]);

        $theme = $query->createCommand()
            ->queryOne();

        return $theme;
    }

    public function actionListTheme()
    {
        $get = Yii::$app->request->get();
        $lang_id = 1;
        $translate_id = 2;
        if (isset($get["lang_id"])){
            $lang_id = $get["lang_id"];
        }
        if (isset($get["translate_id"])){
            $translate_id = $get["translate_id"];
        }
        $query = '
        SELECT 
         t1.id,
         tt.translate as name,
         ttt.translate
        FROM 
         `theme` as t1
        LEFT JOIN 
         `theme_translate` AS tt
        ON 
         tt.theme_id = t1.id
        LEFT JOIN 
         `theme_translate` AS ttt
        ON 
         ttt.theme_id = t1.id 
        WHERE 
         t1.parent_id IS NULL
         AND
         t1.status = 1
         AND 
         tt.language_id = ' . $lang_id . '
         AND 
         ttt.language_id = ' . $translate_id . '
        ORDER BY 
         t1.id
        ';

        $themes_array = Yii::$app->db->createCommand($query)
            ->queryAll();

        return $themes_array;
    }

    public function actionGetTheme()
    {
        $user_id = \Yii::$app->user->identity->getId();
        $get = Yii::$app->request->get();
        $id = @$get["id"];

        $languages = ArrayHelper::map(Language::find()
            ->asArray()
            ->all(), 'id', 'locale');

        $lang_id = 1;
        $translate_id = 2;
        if (isset($get["lang_id"])){
            $lang_id = $get["lang_id"];
        }
        if (isset($get["translate_id"])){
            $translate_id = $get["translate_id"];
        }

        $queryWithId = $id ? '
         (
          t1.id = ' . $id . '
          OR
          t1.parent_id = ' . $id . '
         ) 
         AND' : '';

        $query = '
        SELECT 
         t1.id,
         t1.parent_id,
         tt.translate as name,
         ttt.translate,
         t1.pos_row,
         t1.pos_col
        FROM 
         `theme` as t1
        LEFT JOIN 
         `theme_translate` AS tt
        ON 
         tt.theme_id = t1.id
        LEFT JOIN 
         `theme_translate` AS ttt
        ON 
         ttt.theme_id = t1.id 
        WHERE
        ' . $queryWithId . '
         t1.status = 1
         AND 
         tt.language_id = ' . $lang_id . '
         AND 
         ttt.language_id = ' . $translate_id . '
        ORDER BY 
         t1.id
        ';

        $theme_array = Yii::$app->db->createCommand($query)
            ->queryAll();

        $themes = [];
        $themes_ids = [];
        $themes_not_sort_array = [];

        if (count($theme_array) > 0){
            foreach ($theme_array as $t){
                $themes_ids[] = $t["id"];
                $themes_not_sort_array[$t["id"]] = $t;
            }
        } else {
            return [];
        }

        $query_lesson = '
        SELECT 
         l1.id,
         l1.theme_id,
         l1.time_passage,
         l1.lives_count,
         l1.points_count,
         lt.translate as name,
         ltt.translate
        FROM 
         `lesson` as l1
        LEFT JOIN 
         `lesson_translate` AS lt
        ON 
         lt.lesson_id = l1.id
        LEFT JOIN 
         `lesson_translate` AS ltt
        ON 
         ltt.lesson_id = l1.id 
        WHERE 
         l1.theme_id IN (' . implode(", ", $themes_ids) . ')
         AND
         l1.status = 1
         AND 
         lt.lang_id = ' . $lang_id . '
         AND 
         ltt.lang_id = ' . $translate_id . '
        ORDER BY 
         l1.id
        ';

        $lessons_array = Yii::$app->db->createCommand($query_lesson)
            ->queryAll();
        $lessons = [];
        $lessons_ids = [];
        if (count($lessons_array) > 0){
            foreach ($lessons_array as $l){
                $lessons[$l["theme_id"]][] = $l;
                $lessons_ids[] = $l["id"];
            }
        }

        if (count($lessons_ids) > 0){
            $query_lesson_words = '
        SELECT 
         lw.id,
         w.article,
         w.name,
         w.description,
         wt.article as translate_article,
         wt.name as translate,
         wt.description as translate_description,
         lw.lesson_id,
         lw.word_group_id,
         wg.name as word_group_name,
         (
          SELECT 
           t.name 
          FROM 
           `word` as t 
          WHERE 
           t.lang_id = ' . $translate_id . ' 
          AND
           t.word_group_id = lwbt.bad_translate_id
         ) as bad_translate,
         (
          SELECT 
           lm.image 
          FROM 
           `lesson_image` as lm 
          WHERE 
           lm.lesson_word_id = lw.id
         ) as image,
         (
          SELECT 
           lm.media 
          FROM 
           `lesson_media` as lm 
          WHERE 
           lm.language_id = ' . $translate_id . ' 
          AND
           lm.lesson_word_id = lw.id
         ) as media
        FROM 
         `lesson_word` as lw
        LEFT JOIN 
         `word_group` AS wg
        ON 
         wg.id = lw.word_group_id
        LEFT JOIN 
         `word` AS w
        ON 
         w.word_group_id = lw.word_group_id 
        LEFT JOIN 
         `word` AS wt
        ON 
         wt.word_group_id = lw.word_group_id 
        LEFT JOIN 
         `lesson_word_bad_translate` AS lwbt
        ON 
         lwbt.lesson_word_id = lw.id  
        WHERE 
         lw.lesson_id IN (' . implode(", ", $lessons_ids) . ')
         AND 
         w.lang_id = ' . $lang_id . '
        AND 
         wt.lang_id = ' . $translate_id . ' 
        ORDER BY 
         lw.id
        ';

            $lesson_words_array = Yii::$app->db->createCommand($query_lesson_words)
                ->queryAll();

            //"/uploads/images/lessons/1/"
            //
            $lesson_words = [];

            if (count($lesson_words_array) > 0){
                foreach ($lesson_words_array as $lw){
                    if (isset($lw["image"])){
                        $lw["image"] = "/uploads/images/words/" . $lw["image"];
                    } else {
                        unset($lw["image"]);
                    }
                    if (isset($lw["media"])){
                        $lw["media"] = "/uploads/media/words/" . $languages[$translate_id] . "/" . $lw["media"];
                    } else {
                        unset($lw["media"]);
                    }
                    if (!isset($lw["bad_translate"])){
                        unset($lw["bad_translate"]);
                    }
                    if (!isset($lw["article"]) || empty($lw["article"])){
                        unset($lw["article"]);
                    }
                    if (!isset($lw["description"]) || empty($lw["description"])){
                        unset($lw["description"]);
                    }
                    if (!isset($lw["translate_article"]) || empty($lw["translate_article"])){
                        unset($lw["translate_article"]);
                    }
                    if (!isset($lw["translate_description"]) || empty($lw["translate_description"])){
                        unset($lw["translate_description"]);
                    }
                    $lesson_words[$lw["lesson_id"]][] = $lw;
                }
            }


            $query_completed_lesson = '
            SELECT 
             cul.id,
             cul.user_id,
             l.theme_id,
             cul.lesson_id,
             cul.stars_total,
             cul.points,
             cul.answers_count,
             cul.flag_help,
             cul.flag_time    
            FROM 
             `completed_user_lesson` as cul
            LEFT JOIN 
             `lesson` AS l
            ON 
             l.id = cul.lesson_id 
            WHERE 
              cul.user_id = ' . $user_id . '
            AND
              cul.lang_id = ' . $lang_id . ' 
            AND
              cul.translate_id = ' . $translate_id . '     
            ORDER BY 
             cul.id
            ';

            $completed_lesson_array = Yii::$app->db->createCommand($query_completed_lesson)
                ->queryAll();

            $completed_lesson = [];
            $completed_lesson_themes = [];
            $themes_progress = [];
            $themes_parent_progress = [];

            foreach ($theme_array as $themeItem){
                $themes_progress[$themeItem['id']] = [
                    "stars_total"   => 0,
                    "points"        => 0,
                    "flag_help"     => 0,
                    "flag_time"     => 0,
                    "answers_count" => 0,
                ];
            }

            //Записываем массив с пройденными уроками
            if (count($completed_lesson_array) > 0){
                foreach ($completed_lesson_array as $t){
                    $completed_lesson[$t["lesson_id"]] = $t;
                    $completed_lesson_themes[$t["theme_id"]][] = $t;
                }
            }

            // Прогресс по лекциям
            if (count($completed_lesson_themes) > 0){
                foreach ($completed_lesson_themes as $theme_id => $cul){
                    foreach ($cul as $el){
                        $themes_progress[$theme_id]["stars_total"] += $el["stars_total"];
                        $themes_progress[$theme_id]["points"] += $el["points"];
                        $themes_progress[$theme_id]["flag_help"] += $el["flag_help"];
                        $themes_progress[$theme_id]["flag_time"] += $el["flag_time"];
                        $themes_progress[$theme_id]["answers_count"] += $el["answers_count"];
                    }
                }
            }

            foreach ($theme_array as $themeItem){
                $themes_parent_progress[$themeItem['id']] = [
                    "stars_total"   => 0,
                    "points"        => 0,
                    "flag_help"     => 0,
                    "flag_time"     => 0,
                    "answers_count" => 0,
                ];
            }

            // Прогресс по подтемам
            if (count($themes_progress) > 0){
                foreach ($theme_array as $themeItem){
                    if ($themeItem['parent_id']){
                        $themesProgressById = $themes_progress[$themeItem['id']];
                        $themeParent = &$themes_parent_progress[$themeItem['parent_id']];

                        $themeParent["stars_total"] += $themesProgressById["stars_total"];
                        $themeParent["points"] += $themesProgressById["points"];
                        $themeParent["flag_help"] += $themesProgressById["flag_help"];
                        $themeParent["flag_time"] += $themesProgressById["flag_time"];
                        $themeParent["answers_count"] += $themesProgressById["answers_count"];
                    }
                }
                unset($themeParent);
            }


            //Заполняем слова для уроков и прогресс по урокам
            if (count($lessons) > 0){
                foreach ($lessons as $theme_id => $l){
                    if (count($l) > 0){
                        foreach ($l as $key => $el){
                            if (isset($completed_lesson[$el["id"]])){
                                $lessons[$theme_id][$key]["progress"] = $completed_lesson[$el["id"]];
                            }
                            if (isset($lesson_words[$el["id"]])){
                                $lessons[$theme_id][$key]["words"] = $lesson_words[$el["id"]];
                            }
                        }
                    }
                }
            }


        }

        //Формируем конечный массив
        if (count($themes_not_sort_array) > 0){
            foreach ($themes_not_sort_array as $t){
                if (isset($themes_progress[$t["id"]])){
                    $t["progress"] = $themes_progress[$t["id"]];
                }
                if (isset($lessons[$t["id"]]) && count($lessons[$t["id"]]) > 0){
                    $t["lessons"] = $lessons[$t["id"]];
                }
                if (!isset($t["parent_id"])){
                    unset($t["parent_id"]);
                    unset($t["pos_row"]);
                    unset($t["pos_col"]);
                    if (count($themes_parent_progress) > 0){
                        $t["progress"] = $themes_parent_progress[$t['id']];
                    }
                    $themes[$t["id"]] = $t;
                } else {
                    $parent_id = $t["parent_id"];
                    unset($t["parent_id"]);
                    if (count($themes_not_sort_array) == 1 && isset($parent_id)){
                        $themes[] = $t;
                    } else {
                        $themes[$parent_id]["sub_themes"][] = $t;
                    }

                }
            }
        }


        $theme = array_values($themes);

        return $theme;


    }

}