<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;
use app\modules\api\models\WordModel;

/**
 * API для работы с ресурсом `Word`
 *
 *  Поскольку клиентское приложение не имеет возможности менять темы
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class WordController
 *
 * @property \app\models\Word $modelClass
 *
 * @package app\modules\api\controllers
 */
class WordController extends ApiController
{
    public $modelClass = WordModel::class;
    public $isVisible  = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}