<?php

namespace app\modules\api\controllers\v1;

use app\models\CompletedUserLesson;
use app\modules\api\components\ApiController;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
/**
 * API для работы с ресурсом `CompletedUserLesson`
 *
 * Методы: [GET, POST, DELETE, PUT/PATCH]
 *
 * Результат: получение данных в JSON
 *
 * Class CompletedUserLessonController
 *
 * @package app\modules\api\controllers
 */
class CompletedUserLessonController extends ApiController
{
    public $modelClass = 'app\models\CompletedUserLesson';

    public $isVisible = true;


    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }


    /**
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query' => CompletedUserLesson::find(),
            'pagination' => false
        ]);
    }


    /**
     * @param $id
     * @return ActiveDataProvider
     */
    public function actionUser($id)
    {
        return new ActiveDataProvider([
            'query' => CompletedUserLesson::find()->where(['user_id' => $id]),
            'pagination' => false
        ]);
    }

}