<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;

/**
 * API для работы с ресурсом `Language`
 *
 *  Поскольку клиентское приложение не имеет возможности менять Языки
 *  в реализации методов POST, DELETE, PUT/PATCH нет необходимости
 *
 * Class LanguagesController
 *
 * @property \app\models\Language $modelClass
 *
 * @package app\modules\api\controllers
 */
class LanguageController extends ApiController
{
    public $modelClass = 'app\models\Language';
    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }
}