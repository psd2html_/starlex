<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\components\ApiController;
use app\modules\user\models\User;

/**
 * API для работы с ресурсом `User`
 *
 * Методы: [GET, POST, DELETE, PUT/PATCH]
 * Пример зарпоса: GET /api/user
 * В заголовках должно быть:
 * - Accept: application/json
 * - Content-Type: application/json; charset=utf-8
 * - Cache-Control: no-cache -- [не обязательно]
 * - Authorization: Bearer [TOKEN_ПОЛЬЗОВАТЕЛЯ] -- TOKEN_ПОЛЬЗОВАТЕЛЯ берется из таблички `user`.`auth_key`
 *
 * Результат: получение данных в JSON
 *
 * Class UserController
 *
 * @package app\modules\api\controllers
 */
class UserController extends ApiController
{
    public $modelClass = 'app\modules\user\models\User';

    public $isVisible = true;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['update']);
        return $actions;
    }

    public function verbs()
    {
        return [
            'update' => ['put'],
        ];
    }

    public function actionUpdate()
    {
        $user_id = \Yii::$app->user->identity->getId();
        $model = User::findOne($user_id);
        $bodyParams = \Yii::$app->request->bodyParams;
        $post = [];
        if(isset($bodyParams["firstname"])){
            $post["firstname"] = $bodyParams["firstname"];
        }
        if(isset($bodyParams["lastname"])){
            $post["lastname"] = $bodyParams["lastname"];
        }
        if ($model->load($post, '')){
            $model->scenario = 'rest-profile';
            $model->save();
        }
        return $model;
    }
}