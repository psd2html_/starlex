<?php

namespace app\modules\api\controllers\v1;

use app\models\Repetition;
use app\modules\api\components\ApiController;
use yii\helpers\ArrayHelper;

/**
 * API для работы с ресурсом `Repetition`
 *
 *  Поскольку клиентское приложение не имеет возможности менять тарифы, отпадает необходимость
 *  в реализации методов POST, DELETE, PUT/PATCH
 *
 * Class AwardController
 *
 * @property mixed             $newWord
 * @property void              $words
 * @property \app\models\Award $modelClass
 *
 * @package app\modules\api\controllers
 */
class RepetitionController extends ApiController
{
    public $modelClass = 'app\models\Repetition';

    public $isVisible = true;

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'view'  => ['get', 'options'],
        ];
    }

    public function setNewWord($id)
    {
        $newWordR = new Repetition([
            'lesson_word_id' => $id,
        ]);
        $newWordR->save();
    }

    public function getWords()
    {
        // Метод формирования и отправки списка слов
    }

    /**
     * Request must have array view:
     * [
     *      'ids' => [
     *          'word_id' => 'status repetition (true or false)',
     *          ...
     *      ]
     * ]
     */
    public function setWords()
    {
        $ids = \Yii::$app->request->post('ids');

        // Метод обработки списка слов
        if ($words = Repetition::findAll($ids)){
            /** @var Repetition[] $words */
            $words = ArrayHelper::index($words, 'id');
            foreach ($ids as $id => $status){
                if (isset($words[$id])){
                    $word = $words[$id];
                    // Выключение для следующего повтора
                    $word->status = Repetition::STATUS_DISABLE;

                    if ($status){
                        if ($word->count_success < 5){
                            $word->count_success++;
                        } else {
                            $word->status = Repetition::STATUS_REMOVE;
                        }
                    } else {
                        $word->count_success = 0;
                    }

                    $word->save();
                } else {
                    \Yii::error("Word ID '$id' is not find in table 'repetition'");
                }
            }
        } else {
            // Обработка ошибки пустого массива 'ids'
        }
    }
}