<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

?>

    Здравствуйте, <?= Html::encode($user->username) ?>!

    Ваш новый пароль: <?= Html::encode($password) ?>