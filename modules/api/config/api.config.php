<?php

use yii\web\Response;

return [
    'components' => [
        'response' => [
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON, // Всё (в том числе и все ошибки и исключения) будут выводится в json формате
            'charset' => 'UTF-8',
        ],
        'cache'                => [
            'class' => 'yii\caching\FileCache',
            'keyPrefix' => 'api_',
        ],
    ],
];