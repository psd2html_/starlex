<?php

namespace app\modules\user\controllers;

use app\modules\user\models\forms\EmailConfirmForm;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\models\forms\PasswordResetForm;
use app\modules\user\models\forms\PasswordResetRequestForm;
use app\modules\user\models\forms\SignupForm;
use app\modules\user\models\User;
use app\modules\user\models\UserSocialAccount;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * SignIn controller for the `user` module
 */
class SignInController extends Controller
{
    public $layout = '/base';

    public function behaviors()
    {
        return [
            // отключить CSRF-проверку Yii2 для OpenID callbacks
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::class,
                'only'  => ['login'],
            ],
            'access' => [
                'class' => AccessControl::class,
                'only'  => ['logout', 'signup', 'index'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');

        if (isset($serviceName))
        {

            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('/login'));

//            file_put_contents(\Yii::getAlias('@rumtime').'/vk/eauth.log', Json::encode($));

            try
            {
                if ($eauth->authenticate())
                {
//                    var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

//                    echo '<pre>';
//                    print_r($eauth->getAttributes());
//                    echo '</pre>';
//                    die('STOP');

                    $identity = User::findByEAuth($eauth);
                    Yii::$app->getUser()->login($identity);

                    $identity = Yii::$app->getUser()->getIdentity();
                    if (isset($identity->profile))
                    {
                        $s_provider = isset($identity->profile['service']) ? ($identity->profile['service']) : '';

                        $find_SocialAccount = UserSocialAccount::findOne([
                            'provider'  => $s_provider,
                            'client_id' => isset($identity->profile['id']) ? ($identity->profile['id']) : '',
                        ]);

                        if (!$find_SocialAccount)
                        {
                            // создаем пользователя в таблице user
                            $user = new User();
                            $user->username = isset($identity->profile['name']) ? ($identity->profile['name']) : '';
                            $user->email = isset($identity->profile['email']) ? ($identity->profile['email']) : '';
                            $user->status = User::STATUS_ACTIVE;
                            if ($user->save()){
                                $SocialAccount = new UserSocialAccount();
                                $SocialAccount->setAttributes($identity->profile);
                                $SocialAccount->user_id = $user->id;
                                $SocialAccount->provider = $s_provider;
                                $SocialAccount->created_at = time();

                                $SocialAccount->save();
                            }
                        }
                    }

                    // special redirect with closing popup window
                    $eauth->redirect();
                }
                else
                {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e){
                // save error to show it later
                Yii::$app->getSession()
                    ->setFlash('error', 'EAuthException: ' . $e->getMessage());

                // close popup window and redirect to cancelUrl
                // $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }


        /**
         * default authorization code through login/password ..
         */
        if (!Yii::$app->user->isGuest){
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()){
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }


    public function actionOauth2callback()
    {
        echo 'actionOauthcallback';

        return 'actionOauthcallback';
    }


    /**
     * Logout action.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())){
            if ($user = $model->signup()){
                Yii::$app->getSession()
                    ->setFlash('success', 'Спасибо за регистрацию! Теперь можете войти, используя свои логин и пароль.');

                return $this->redirect('login');
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     *
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionEmailConfirm($token)
    {
        try {
            $model = new EmailConfirmForm($token);
        } catch (InvalidParamException $e){
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->confirmEmail()){
            Yii::$app->getSession()
                ->setFlash('success', 'Спасибо! Ваш Email успешно подтверждён.');
        } else {
            Yii::$app->getSession()
                ->setFlash('error', 'Ошибка подтверждения Email.');
        }

        return $this->goHome();
    }


    /**
     * @return string|\yii\web\Response
     */
    public function actionPasswordResetRequest()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            if ($model->sendEmail()){
                Yii::$app->getSession()
                    ->setFlash('success', 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()
                    ->setFlash('error', 'Извините. У нас возникли проблемы с отправкой.');
            }
        }

        return $this->render('passwordResetRequest', [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     *
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionPasswordReset($token)
    {
        try {
            $model = new PasswordResetForm($token);
        } catch (InvalidParamException $e){
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()){
            Yii::$app->getSession()
                ->setFlash('success', 'Спасибо! Пароль успешно изменён.');

            return $this->goHome();
        }

        return $this->render('passwordReset', [
            'model' => $model,
        ]);
    }
}
