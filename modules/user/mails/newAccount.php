<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
?>

<?= Yii::t('app', "Здравствуйте, {username} !", [
    'username' => Html::encode($user->username),
]); ?>

<?= Yii::t('app', 'Спасибо за регистрацию в административной части приложения Starlex!'); ?>