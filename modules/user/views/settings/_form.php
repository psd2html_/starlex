<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'tariff_id')->textInput() ?>

    <?= $form->field($model, 'native_lang_id')->textInput() ?>

    <?= $form->field($model, 'learn_lang_id')->textInput() ?>

    <?= $form->field($model, 'target_day')->textInput() ?>

    <?= $form->field($model, 'notify_day_task')->textInput() ?>

    <?= $form->field($model, 'notify_words_repeat')->textInput() ?>

    <?= $form->field($model, 'test_status')->textInput() ?>

    <?= $form->field($model, 'rewards')->textInput() ?>

    <?= $form->field($model, 'certificates')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rating')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
