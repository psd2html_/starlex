<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\forms\LoginForm */

$this->title = 'Вход в панель администрирования';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sign-in user-default-login">
    <div class="flex-content">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php
        if (Yii::$app->getSession()
            ->hasFlash('error')){
            echo '<div class="alert alert-danger">' . Yii::$app->getSession()
                    ->getFlash('error') . '</div>';
        }
        if (Yii::$app->getSession()
            ->hasFlash('success')){
            echo '<div class="alert alert-success">' . Yii::$app->getSession()
                    ->getFlash('success') . '</div>';
        }
        ?>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')
            ->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')
            ->checkbox() ?>

        <p style="color:#999;margin:1em 0">
            Если вы забыли свой пароль, вы можете его . <?= Html::a('сбросить', ['password-reset-request']) ?>.
        </p>
        <div class="form-group" id="login">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-lg btn-primary', 'name' => 'login-button']) ?> или <?= Html::a('Зарегистрироваться', ['signup']); ?>
        </div>
        <?php ActiveForm::end(); ?>
        <p class="lead">У вас уже есть учетная запись на одном из этих сайтов?
            <br>Нажмите на логотип, чтобы войти с ней здесь:</p>
        <?php echo \nodge\eauth\Widget::widget([
            'action' => '/login',
        ]); ?>
    </div>
</div>
