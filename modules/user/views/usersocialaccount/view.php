<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserSocialAccount */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Social Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-social-account-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'provider',
            'client_id',
            'data:ntext',
            'code',
            'email:email',
            'username',
            'phone',
            'avatar',
            'social_page',
            'sex',
            'birthday',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
