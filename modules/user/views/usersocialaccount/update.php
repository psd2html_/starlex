<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserSocialAccount */

$this->title = 'Update User Social Account: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Social Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-social-account-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
