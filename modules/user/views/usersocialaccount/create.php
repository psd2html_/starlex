<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserSocialAccount */

$this->title = 'Create User Social Account';
$this->params['breadcrumbs'][] = ['label' => 'User Social Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-social-account-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
