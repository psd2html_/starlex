<?php

namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use app\modules\user\Module;
use yii\base\Model;
use yii\db\ActiveQuery;

class ProfileUpdateForm extends Model
{
    public $email;
    public $firstname;
    public $lastname;

    /**
     * @var User
     */
    private $_user;

    /**
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        $this->email = $user->email;
        $this->firstname = $user->firstname;
        $this->lastname = $user->lastname;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => User::class,
                'message' => "Такой Email уже существует",
                'filter' => function (ActiveQuery $query) {
                    $query->andWhere(['<>', 'id', $this->_user->id]);
                },
            ],
            ['email', 'string', 'max' => 255],
            ['firstname', 'string', 'max' => 255],
            ['lastname', 'string', 'max' => 255],
        ];
    }


    /**
     * @return bool
     */
    public function update()
    {
        if ($this->validate())
        {
            $user = $this->_user;
            $user->email = $this->email;
            $user->firstname = $this->firstname;
            $user->lastname = $this->lastname;
            return $user->save(false);
        }
        else
        {
            return false;
        }
    }
}