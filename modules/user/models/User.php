<?php

namespace app\modules\user\models;

use nodge\eauth\ErrorException;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer             $id
 * @property string              $username
 * @property string              $firstname
 * @property string              $lastname
 * @property string              $email
 * @property string              $password_hash
 * @property integer             $status
 * @property string              $auth_key
 * @property string              $email_confirm_token
 * @property string              $password_reset_token
 * @property integer             $created_at
 * @property \yii\db\ActiveQuery $userSocialAccounts
 * @property mixed               $statusName
 * @property \yii\db\ActiveQuery $userSettings
 * @property string              $password
 * @property integer             $updated_at
 * @property int                 $rating [int(11)]
 * @property int                 $type   [int(11)]  Тип: обычный пользователь, админ, менеджер
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE  = 1;
    const STATUS_WAIT    = 2;

    const TYPE_ADMIN = 9;

    const SCENARIO_PROFILE = 'profile'; // Заблокирован
    /**
     * @var array EAuth attributes
     */
    public $profile; // Активен
    public $authKey; // Ожидает подтверждения
    public $f_password;

    public static function tableName()
    {
        return 'user';
    }

    /**
     * @param      $type
     * @param null $code
     *
     * @return array
     */
    public static function itemAlias($type, $code = NULL)
    {
        $_items = [
            'Status' => [
                self::STATUS_BLOCKED => 'Заблокирован',
                self::STATUS_ACTIVE  => 'Активен',
                self::STATUS_WAIT    => 'Ожидает подтверждения',
            ],
        ];
        if (isset($code)){
            return $_items[$type][$code] ?: [];
        } else {
            return $_items[$type] ?: [];
        }
    }

    public static function findIdentity($id)
    {
        if (Yii::$app->getSession()
            ->has('user-' . $id)){
            return new self(Yii::$app->getSession()
                ->get('user-' . $id));
        } else {
            //return isset(self::$users[$id]) ? new self(self::$users[$id]) : null;
            return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
        }
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        //throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)){
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status'               => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)){
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $expire >= time();
    }

    /**
     * @param string $email_confirm_token
     *
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_confirm_token' => $email_confirm_token, 'status' => self::STATUS_WAIT]);
    }

    /**
     * @param \nodge\eauth\ServiceBase $service
     *
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service)
    {
        if (!$service->getIsAuthenticated()){
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName() . '-' . $service->getId();
        $attributes = [
            'id'       => $id,
            'username' => $service->getAttribute('name'),
            'authKey'  => md5($id),
            'profile'  => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()
            ->set('user-' . $id, $attributes);

        return new self($attributes);
    }

    public static function collection()
    {
        return ArrayHelper::map(self::find()
            ->asArray()
            ->all(), 'id', 'username');
    }

    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email]);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['username', 'firstname', 'lastname', 'email', 'status', 'f_password'],
            self::SCENARIO_PROFILE => ['email', 'firstname', 'lastname'],
            'rest-profile' => ['firstname', 'lastname']
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'required'],
//            ['username', 'match', 'pattern' => '#^[\w_-]+$#is'],
//            [
//                'username',
//                'unique',
//                'message'     => 'This username has already been taken.',
//            ],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['firstname', 'match', 'pattern' => '/^[a-zA-Zа-яА-Я\']+$/', 'message' => 'Значение должно содержать латинские или русские символы и состоять из одного слова'],
            ['firstname', 'string', 'min' => 2, 'max' => 255],

            ['lastname', 'match', 'pattern' => '/^[a-zA-Zа-яА-Я\']+$/', 'message' => 'Значение должно содержать латинские или русские символы и состоять из одного слова'],
            ['lastname', 'string', 'min' => 2, 'max' => 255],

            //['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'message' => 'This email address has already been taken.',
            ],
            ['email', 'string', 'min' => 5,'max' => 255],

            [['status', 'rating', 'type'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesArray())],
        ];
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_BLOCKED => 'Заблокирован',
            self::STATUS_ACTIVE  => 'Активен',
            self::STATUS_WAIT    => 'Ожидает подтверждения',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => 'ID',
            'username'             => 'Логин',
            'firstname'            => 'Имя',
            'lastname'             => 'Фамилия',
            'email'                => 'Email',
            'password_hash'        => 'Password Hash',
            'status'               => 'Статус',
            'auth_key'             => 'Auth Key (Token)',
            'email_confirm_token'  => 'Email Confirm Token',
            'password_reset_token' => 'Password Reset Token',
            'created_at'           => 'Создан',
            'updated_at'           => 'Обновлен',
            'f_password'           => 'Пароль',
        ];
    }

    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if ($insert){
                $this->generateAuthKey();
            }

            return true;
        }

        return false;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettings()
    {
        return $this->hasMany(UserSettings::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSocialAccounts()
    {
        return $this->hasMany(UserSocialAccount::class, ['user_id' => 'id']);
    }
}
