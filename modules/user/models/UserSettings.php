<?php

namespace app\modules\user\models;

use app\models\Language;
use app\models\Tariff;

/**
 * This is the model class for table "user_settings".
 *
 * @property integer  $id
 * @property integer  $user_id
 * @property integer  $tariff_id
 * @property integer  $native_lang_id
 * @property integer  $learn_lang_id
 * @property integer  $target_day
 * @property integer  $notify_day_task
 * @property integer  $notify_words_repeat
 * @property integer  $test_status
 * @property integer  $rewards
 * @property string   $certificates
 * @property integer  $rating
 *
 * @property Language $learnLang
 * @property Language $nativeLang
 * @property Tariff   $tariff
 * @property User     $user
 */
class UserSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [
                [
                    'user_id',
                    'tariff_id',
                    'native_lang_id',
                    'learn_lang_id',
                    'target_day',
                    'notify_day_task',
                    'notify_words_repeat',
                    'test_status',
                    'rewards',
                    'rating',
                ],
                'integer',
            ],
            [['certificates'], 'string', 'max' => 255],
            [
                ['learn_lang_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['learn_lang_id' => 'id'],
            ],
            [
                ['native_lang_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Language::class,
                'targetAttribute' => ['native_lang_id' => 'id'],
            ],
            [
                ['tariff_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tariff::class,
                'targetAttribute' => ['tariff_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                  => 'ID',
            'user_id'             => 'User ID',
            'tariff_id'           => 'Tariff ID',
            'native_lang_id'      => 'Родной язык',
            'learn_lang_id'       => 'Изучаемый язык',
            'target_day'          => 'Дневная цель (0-10 минут;1-20 минут;далее уточнить)',
            'notify_day_task'     => 'Напоминать о задании на день(0-выкл;1-вкл)',
            'notify_words_repeat' => 'Напоминать о словах для повторения(0-выкл;1-вкл)',
            'test_status'         => 'Статус вводного задания(0-не пройдено;1-пройдено)',
            'rewards'             => 'Кол-во полученных наград',
            'certificates'        => 'массив открытых номеров сертификата(1-9)',
            'rating'              => 'Кол-во набранного опыта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearnLang()
    {
        return $this->hasOne(Language::class, ['id' => 'learn_lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNativeLang()
    {
        return $this->hasOne(Language::class, ['id' => 'native_lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::class, ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
