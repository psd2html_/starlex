<?php

namespace app\modules\user\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_social_account".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string  $provider
 * @property integer $client_id
 * @property string  $data
 * @property string  $code
 * @property string  $email
 * @property string  $username
 * @property string  $phone
 * @property string  $avatar
 * @property string  $social_page
 * @property integer $sex
 * @property string  $birthday
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User    $user
 */
class UserSocialAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_social_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'client_id', 'sex', 'created_at', 'updated_at'], 'integer'],
            [['provider', 'created_at'], 'required'],
            [['data'], 'string'],
            [
                ['provider', 'code', 'email', 'username', 'phone', 'avatar', 'social_page', 'birthday'],
                'string',
                'max' => 255,
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'user_id'     => 'User ID',
            'provider'    => 'Provider',
            'client_id'   => 'Social ID',
            'data'        => 'Data',
            'code'        => 'Code',
            'email'       => 'Email',
            'username'    => 'Username',
            'phone'       => 'Phone',
            'avatar'      => 'Avatar',
            'social_page' => 'Ссылка на профиль юзера',
            'sex'         => '1-мужской;2-женский',
            'birthday'    => 'Birthday',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
