<?php

namespace app\controllers;

use app\models\Lesson;
use app\models\LessonWord;
use app\models\LessonWordBadTranslate;
use app\models\search\LessonWordBadTranslateSearch;
use app\models\Theme;
use app\models\WordGroup;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LessonWordBadTranslateController implements the CRUD actions for LessonWordBadTranslate model.
 */
class LessonWordBadTranslateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LessonWordBadTranslate models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LessonWordBadTranslateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new LessonWordBadTranslate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LessonWordBadTranslate();
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()){
            return $this->redirect(['index']);
        }

        $themeId = isset($post['themeId']) ? (int)$post['themeId'] : 0;
        $lessonId = isset($post['lessonId']) ? (int)$post['lessonId'] : 0;

        $themesCollection = Theme::collection();
        $lessonsCollection = Lesson::collection(['theme_id' => $themeId]);
        $lessonWordsCollection = LessonWord::collection(['lesson_id' => $lessonId]);

        return $this->render('create', [
            'model'                 => $model,
            'lessonId'              => $lessonId,
            'themeId'               => $themeId,
            'lessonWordsCollection' => $lessonWordsCollection,
            'themesCollection'      => $themesCollection,
            'lessonsCollection'     => $lessonsCollection,
        ]);
    }

    /**
     * Updates an existing LessonWordBadTranslate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }

        $post = Yii::$app->request->post();

        $themeId = $model->lessonWord->lesson->theme_id;
        if (Yii::$app->request->isPost && isset($post['themeId'])){
            $themeId = (int)$post['themeId'];
        }

        $lessonId = $model->lessonWord->lesson_id;
        if (Yii::$app->request->isPost && isset($post['lessonId'])){
            $lessonId = (int)$post['lessonId'];
        }

        $themesCollection = Theme::collection();
        $lessonsCollection = Lesson::collection(['theme_id' => $themeId]);
        $lessonWordsCollection = LessonWord::collection(['lesson_id' => $lessonId]);

        return $this->render('update', [
            'model'                 => $model,
            'lessonId'              => $lessonId,
            'themeId'               => $themeId,
            'themesCollection'      => $themesCollection,
            'lessonsCollection'     => $lessonsCollection,
            'lessonWordsCollection' => $lessonWordsCollection,
        ]);
    }

    /**
     * Finds the LessonWordBadTranslate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return LessonWordBadTranslate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LessonWordBadTranslate::findOne($id)) !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing LessonWordBadTranslate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)
            ->delete();

        return $this->redirect(['index']);
    }
}
