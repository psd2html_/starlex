<?php

namespace app\controllers;

use app\models\Lesson;
use app\models\LessonWord;
use app\models\search\LessonWordSearch;
use app\models\Theme;
use app\models\WordGroup;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LessonWordController implements the CRUD actions for LessonWord model.
 */
class LessonWordController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LessonWord models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LessonWordSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new LessonWord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LessonWord();
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()){
            return $this->redirect(['index']);
        }

        $themeId = isset($post['themeId']) ? $post['themeId'] : 0;

        $themesCollection = Theme::collection();
        $lessonsCollection = Lesson::collection(['theme_id' => $themeId]);
        $wordsCollection = WordGroup::collection();

        return $this->render('create', [
            'model'             => $model,
            'themeId'           => $themeId,
            'themesCollection'  => $themesCollection,
            'lessonsCollection' => $lessonsCollection,
            'wordsCollection'   => $wordsCollection,
        ]);
    }

    /**
     * Updates an existing LessonWord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }

        $themeId = $model->lesson->theme_id;

        if (Yii::$app->request->isPost && Yii::$app->request->post('themeId')){
            $themeId = (int)Yii::$app->request->post('themeId');
        }

        $themesCollection = Theme::collection();
        $lessonsCollection = Lesson::collection(['theme_id' => $themeId]);
        $wordsCollection = WordGroup::collection();

        return $this->render('update', [
            'model'             => $model,
            'themeId'           => $themeId,
            'themesCollection'  => $themesCollection,
            'lessonsCollection' => $lessonsCollection,
            'wordsCollection'   => $wordsCollection,
        ]);
    }

    /**
     * Finds the LessonWord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return LessonWord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LessonWord::findOne($id)) !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing LessonWord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)
            ->delete();

        return $this->redirect(['index']);
    }
}
