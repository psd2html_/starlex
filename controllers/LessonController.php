<?php

namespace app\controllers;

use app\models\Language;
use app\models\Lesson;
use app\models\LessonTranslate;
use app\models\WordGroup;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * LessonController implements the CRUD actions for Lessons model.
 */
class LessonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lessons models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $langId = Yii::$app->getRequest()
            ->getQueryParam('lang_id');

        if ($langId){
            $lang_name = @(Language::findOne($langId))->name;
        } else {
            $lang_name = 'Русский';
        }

        // todo: берется первый язык если нет параметра
        $langId = $langId ?? 1;
        $query = Lesson::find()
            ->from('lesson')
            ->joinWith('lang')
            ->where(['lesson_translate.lang_id' => $langId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'lang_name'    => $lang_name,
        ]);
    }

    /**
     * Creates a new Lessons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $lesson = new Lesson();

        if (Yii::$app->request->isPost && $lesson->load(Yii::$app->request->post())){
            if ($lesson->save() && isset($lesson['id'])){
                return $this->redirect(['index']);
            } else {
                return $this->redirect('update', [
                    'id' => $lesson->id,
                ]);
            }
        } else {
            if (!$lesson->names){
                foreach (Language::collection() as $langId => $langName){
                    $lesson->names[] = new LessonTranslate([
                        'lang_id' => $langId,
                    ]);
                }
            }

            return $this->render('create', [
                'model' => $lesson,
            ]);
        }
    }

    /**
     * Updates an existing Lessons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $lesson = $this->findModel($id);

        if (Yii::$app->request->isPost && $lesson->load(Yii::$app->request->post()) && $lesson->save()){
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $lesson,
            ]);
        }
    }

    /**
     * Finds the Lessons model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Lesson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lesson::findOne($id)) !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Lessons model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        self::findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionRemoveWordGroup($id)
    {
        Yii::$app->response->format = Yii::$app->response::FORMAT_JSON;

        return [
            'success' => (bool)WordGroup::deleteAll(['id' => $id]),
        ];
    }

    private function uploadImportFile(Lesson &$model)
    {
        $model->archiveFile = UploadedFile::getInstance($model, 'archiveFile');
        if ($model->upload()){

            return true;
        }

        return false;
    }
}
