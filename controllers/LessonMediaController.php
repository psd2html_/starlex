<?php

namespace app\controllers;

use app\models\Lesson;
use app\models\LessonMedia;
use app\models\LessonWord;
use app\models\search\LessonMediaSearch;
use app\models\Theme;
use app\models\UploadForm;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * LessonMediaController implements the CRUD actions for LessonsMedia model.
 */
class LessonMediaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LessonsMedia models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LessonMediaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new LessonsMedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LessonMedia();
        if ($model->load(Yii::$app->request->post())){
            // удаляем файл
            if ($model->del_media){
                $model->deleteFile();
            } else {
                $file = UploadedFile::getInstance($model, 'media');

                if ($file){
                    // Attach file
                    $model->attachFile($file);
                    if ($model->save()){
                        return $this->redirect(['index']);
                    }
                }
            }
        }

        $themeId = (int)Yii::$app->request->post('themeId');
        $lessonId = (int)Yii::$app->request->post('lessonId');

        $themesCollection = Theme::collectionParent();
        $lessonsCollection = Lesson::collection(['theme_id' => $themeId]);
        $lessonWordsCollection = LessonWord::collectionUsa(['lesson_id' => $lessonId]);

        return $this->render('create', [
            'model'                 => $model,
            'lessonId'              => $lessonId,
            'themeId'               => $themeId,
            'themesCollection'      => $themesCollection,
            'lessonsCollection'     => $lessonsCollection,
            'lessonWordsCollection' => $lessonWordsCollection,
        ]);
    }

    /**
     * Updates an existing LessonsMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){
            //Если отмечен чекбокс «удалить файл»
            if ($model->del_media){
                $model->deleteFile();
            }

            $file = UploadedFile::getInstance($model, 'media');
            if ($file)// Attach file
            {
                $model->attachFile($file);
            }

            if ($model->save()){
                return $this->redirect(['index']);
            }
        } else {
            $themeId = $model->themeId;
            $lessonId = $model->lessonId;

            if (Yii::$app->request->isPost && Yii::$app->request->post('themeId')){
                $themeId = (int)Yii::$app->request->post('themeId');
            }
            if (Yii::$app->request->isPost && Yii::$app->request->post('lessonId')){
                $lessonId = (int)Yii::$app->request->post('lessonId');
            }

            $themesCollection = Theme::collectionParent();
            $lessonsCollection = Lesson::collection(['theme_id' => $themeId]);
            $lessonWordsCollection = LessonWord::collectionUsa(['lesson_id' => $lessonId]);

            return $this->render('update', [
                'model'                 => $model,
                'lessonId'              => $lessonId,
                'themeId'               => $themeId,
                'themesCollection'      => $themesCollection,
                'lessonsCollection'     => $lessonsCollection,
                'lessonWordsCollection' => $lessonWordsCollection,
            ]);
        }
    }

    /**
     * Finds the LessonsMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return LessonMedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LessonMedia::findOne($id)) !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing LessonsMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        // удаляем file
        $model->deleteFile();
        $model->delete();

        return $this->redirect(['index']);
    }
}
