<?php

namespace app\controllers;

use app\models\LessonWord;
use app\models\search\LessonWordFinishSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LessonWordFinishController implements the CRUD actions for LessonWord model.
 */
class LessonWordFinishController extends Controller
{
    /**
     * Lists all LessonWord models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LessonWordFinishSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the LessonWord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return LessonWord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LessonWord::findOne($id)) !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
