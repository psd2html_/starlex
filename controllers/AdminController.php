<?php

namespace app\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class AdminController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'index'                => ['get'],
                    'git-pull'             => ['post'],
                    'git-status'           => ['post'],
                    'git-reset-hard'       => ['post'],
                    'composer-install'     => ['post'],
                    'composer-update'      => ['post'],
                    'composer-self-update' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGitPull()
    {
        return $this->commandExec('git pull');
    }

    private function commandExec($command)
    {
        $projectPath = \Yii::getAlias('@app');
        $exec = "cd {$projectPath} && {$command}";

        exec($exec, $resultArray);

        if ($resultArray){
            \Yii::$app->getSession()
                ->setFlash('info', implode("<br>", $resultArray));
        }

        return $this->redirect('index');
    }

    public function actionGitStatus()
    {
        return $this->commandExec('git status');
    }

    public function actionGitResetHard()
    {
        return $this->commandExec('git reset --hard HEAD');
    }

    public function actionComposerUpdate()
    {
        return $this->commandExec('composer update');
    }

    public function actionComposerInstall()
    {
        return $this->commandExec('composer install');
    }

    public function actionComposerSelfUpdate()
    {
        return $this->commandExec('composer self-update');
    }
}
