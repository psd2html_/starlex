<?php

namespace app\controllers;

use app\models\Lesson;
use app\models\LessonImage;
use app\models\LessonWord;
use app\models\search\LessonImageSearch;
use app\models\Theme;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * LessonImageController implements the CRUD actions for LessonsMedia model.
 */
class LessonImageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LessonsMedia models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LessonImageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new LessonsMedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LessonImage();
        if ($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model, 'image');

            if ($file){
                $model->image = ''; // временно присваиваем путое значение
                if ( $model->save(false) ){
                    // Attach image
                    $model->attachImage($file);

                    return $this->redirect(['index']);
                }
            }
        }

        $themeId = (int)Yii::$app->request->post('themeId');
        $lessonId = (int)Yii::$app->request->post('lessonId');

        $themesCollection = Theme::collectionParent();
        $lessonsCollection = Lesson::collection(['theme_id' => $themeId]);
        $lessonWordsCollection = LessonWord::collectionUsa(['lesson_id' => $lessonId]);

        return $this->render('create', [
            'model'                 => $model,
            'themesCollection'      => $themesCollection,
            'lessonsCollection'     => $lessonsCollection,
            'themeId'               => $themeId,
            'lessonId'              => $lessonId,
            'lessonWordsCollection' => $lessonWordsCollection,
        ]);
    }

    /**
     * Updates an existing LessonsMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){
            if ($model->save()){
                // Attach image
                $model->attachImage();

                return $this->redirect(['index']);
            }
        }

        $themeId = $model->themeId;
        $lessonId = $model->lessonId;

        if (Yii::$app->request->isPost && Yii::$app->request->post('themeId')){
            $themeId = (int)Yii::$app->request->post('themeId');
        }
        if (Yii::$app->request->isPost && Yii::$app->request->post('lessonId')){
            $lessonId = (int)Yii::$app->request->post('lessonId');
        }

        $themesCollection = Theme::collectionParent();
        $lessonsCollection = Lesson::collection(['theme_id' => $themeId]);
        $lessonWordsCollection = LessonWord::collectionUsa(['lesson_id' => $lessonId]);

        return $this->render('update', [
            'model'                 => $model,
            'themesCollection'      => $themesCollection,
            'lessonsCollection'     => $lessonsCollection,
            'themeId'               => $themeId,
            'lessonId'              => $lessonId,
            'lessonWordsCollection' => $lessonWordsCollection,
        ]);
    }

    /**
     * Finds the LessonsMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return LessonImage
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LessonImage::findOne($id)) !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing LessonsMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        // удаляем картинки
        $model->deleteAttach();
        $model->delete();

        return $this->redirect(['index']);
    }
}
