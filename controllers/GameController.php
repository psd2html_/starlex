<?php

namespace app\controllers;

use app\models\Game;
use app\models\GameTranslate;
use app\models\Language;
use app\models\search\GameSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * GameController implements the CRUD actions for Game model.
 */
class GameController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Game models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Game model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Game();

        if ($this->gameData($model)){
            return $this->redirect(['index']);
        } else {
            if ( !$model->names ){
                foreach (Language::collection() as $langId => $langName){
                    $model->names[] = new GameTranslate([
                        'language_id' => $langId,
                    ]);
                }
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function gameData(Game $game)
    {
        if ( Yii::$app->request->isPost && $game->load(Yii::$app->request->post()) && $game->save()){
            foreach ($game->names as $langId => $gameName){
                $condition = [
                    'game_id' => $game->id,
                    'language_id' => $langId,
                ];

                if ( !($gameName instanceof GameTranslate) ){
                    $gameTranslate = GameTranslate::findOne($condition);

                    if ( !$gameTranslate ){
                        $gameTranslate = new GameTranslate($condition);
                    }
                    $gameTranslate->translate = $gameName;

                    if ( $gameTranslate->save() ){
                        $game->names[$langId] = $gameTranslate;
                    } else {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Updates an existing Game model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Game::findOne($id)) !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Game model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)
            ->delete();

        return $this->redirect(['index']);
    }
}
