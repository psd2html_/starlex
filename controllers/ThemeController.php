<?php

namespace app\controllers;

use app\models\Language;
use app\models\Theme;
use app\models\ThemeTranslate;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ThemeController implements the CRUD actions for Themes model.
 */
class ThemeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Themes models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $lang_id = Yii::$app->getRequest()
            ->getQueryParam('lang_id');

//        Yii::$app->language = 'en';

        if ($lang_id){
            $lang_name = Language::findOne($lang_id)->name;
        } else {
            $lang_name = 'Русский';
        }

        // todo: берется первый язык если нет параметра
        $lang_id = ($lang_id) ? $lang_id : 1;
        $query = Theme::find()
            ->joinWith('lang l')
            ->where(['l.language_id' => $lang_id]);
//var_dump( $query->createCommand()->queryAll() );exit;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Themes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Theme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Theme::findOne($id)) !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Themes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Theme();
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()){
            // пакетная вставка переводов в таблицу
            $insert = [];
            foreach ($post['Theme']['languages'] as $key => $value){
                $insert[] = [$key, $model->id, $value];
            }
            $ThemesLangModel = new ThemeTranslate();
            Yii::$app->db->createCommand()
                ->batchInsert($ThemesLangModel::tableName(), ['language_id', 'theme_id', 'translate'], $insert)
                ->execute();

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Themes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()){
            $theme_pos = [];
            if($row_poss = $post["theme_position"]){
                Yii::$app->db->createCommand()
                    ->update(Theme::tableName(), ['pos_row' => null, 'pos_col' => null], ['parent_id' => $id])
                    ->execute();
                $i = 0;
                foreach ($row_poss as $row_pos){
                    $i++;
                    $j = 0;
                    foreach ($row_pos as $pos){
                        $j++;
                        $theme_pos[$pos] = ['y' => $i, 'x' => $j];
                    }
                }

                if(count($theme_pos)>0){
                    foreach ($theme_pos as $id2 => $pos){
                        Yii::$app->db->createCommand()
                            ->update(Theme::tableName(), ['pos_row' => $pos['y'], 'pos_col' => $pos['x']], ['id' => $id2])
                            ->execute();
                    }
                }
            }

            foreach ($post['Theme']['languages'] as $key => $value){
                Yii::$app->db->createCommand()
                    ->update(ThemeTranslate::tableName(), ['translate' => $value], ['theme_id' => $id, 'language_id' => $key])
                    ->execute();
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Themes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)
            ->delete();

        return $this->redirect(['index']);
    }
}
