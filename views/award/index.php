<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AwardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Награды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="award-index">
    <p>
        <?= Html::a('Создать награду', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            'attainment_name',
//            'success_json:ntext',
            'name',
            'description',
//            'img',
            [
                'attribute' => 'img',
                'value'     => function ($model){
                    return Html::img($model->img);
                },
                'format'    => 'raw',
            ],
            // 'logic:ntext',
            'experience',
            'tariff_id',
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
