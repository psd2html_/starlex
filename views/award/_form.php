<?php

use app\models\Tariff;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Award */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="award-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'attainment_name')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'success_json')
        ->widget('trntv\aceeditor\AceEditor', [
            'mode'  => 'json',
            'theme' => 'github',
        ]); ?>

    <?= $form->field($model, 'name')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'img')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'logic')
        ->widget('trntv\aceeditor\AceEditor', [
            'mode'  => 'json',
            'theme' => 'github',
        ]); ?>

    <?= $form->field($model, 'experience')
        ->textInput() ?>

    <?= $form->field($model, 'tariff_id')
        ->dropDownList(['' => '- Не задано -'] + Tariff::collection()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
