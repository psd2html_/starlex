<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserCertificateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Certificates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-certificate-index">
    <p>
        <?= Html::a('Create User Certificate', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'user_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'user_name',
                'value'     => function ($model){
                    return $model->user->username;
                },
            ],
            [
                'attribute' => 'certificate_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'certificate_name',
                'value'     => function ($model){
                    return ($model->certificate) ? $model->certificate->name : null;
                },
            ],
            'count_parts_collect',
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
