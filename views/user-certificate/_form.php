<?php

use app\models\Certificate;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserCertificate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-certificate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')
        ->dropDownList(User::collection()) ?>

    <?= $form->field($model, 'certificate_id')
        ->dropDownList(Certificate::collection()) ?>

    <?= $form->field($model, 'count_parts_collect')
        ->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
