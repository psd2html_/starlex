<?php


/* @var $this yii\web\View */
/* @var $model app\models\UserCertificate */

$this->title = 'Create User Certificate';
$this->params['breadcrumbs'][] = ['label' => 'User Certificates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-certificate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
