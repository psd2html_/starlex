<?php

/* @var $this yii\web\View */
/* @var $model app\models\UserCertificate */

$this->title = 'Update User Certificate: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Certificates', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-certificate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
