<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Администрирование');
$this->params['breadcrumbs'][] = $this->title;

?>

<div>
    <h3>GIT</h3>
    <?= Html::a('GIT pull', '/admin/git-pull', [
        'data-confirm' => Yii::t('yii', 'Отправить команду "git pull"?'),
        'data-method'  => 'post',
        'class'        => 'btn btn-success',
    ]); ?>
    &nbsp;
    <?= Html::a('GIT status', '/admin/git-status', [
        'data-confirm' => Yii::t('yii', 'Отправить команду "git status"?'),
        'data-method'  => 'post',
        'class'        => 'btn btn-info',
    ]); ?>
    &nbsp;
    <?= Html::a('GIT reset --hard HEAD', '/admin/git-reset-hard', [
        'data-confirm' => Yii::t('yii', 'Отправить команду "git reset --hard HEAD"?'),
        'data-method'  => 'post',
        'class'        => 'btn btn-danger',
    ]); ?>
</div>
<div>
    <h3>Composer</h3>
    <?= Html::a('COMPOSER install', '/admin/composer-install', [
        'data-confirm' => Yii::t('yii', 'Отправить команду "composer install"?'),
        'data-method'  => 'post',
        'class'        => 'btn btn-info',
    ]); ?>
    &nbsp;
    <?= Html::a('COMPOSER update', '/admin/composer-update', [
        'data-confirm' => Yii::t('yii', 'Отправить команду "composer update"?'),
        'data-method'  => 'post',
        'class'        => 'btn btn-success',
    ]); ?>
    &nbsp;
    <?= Html::a('COMPOSER self-update', '/admin/composer-self-update', [
        'data-confirm' => Yii::t('yii', 'Отправить команду "composer self-update"?'),
        'data-method'  => 'post',
        'class'        => 'btn btn-danger',
    ]); ?>
</div>
<div>
    <h3>Другое</h3>
    <?= Html::a('phpMyAdmin', '/phpmyadmin', [
        'class'  => 'btn btn-warning',
        'target' => '_blank',
    ]); ?>
</div>