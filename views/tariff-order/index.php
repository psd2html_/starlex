<?php

use app\models\Tariff;
use app\modules\user\models\User;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TariffOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tariffs Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-order-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Tariffs Order'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'options' => ['width' => 80],
            ],
            [
                'attribute' => 'user_id',
                'options' => ['width' => 80],
                'filter' => User::collection(),
            ],
            'userName',
            [
                'attribute' => 'tariff_id',
                'options' => ['width' => 80],
                'filter' => Tariff::collection(),
            ],
            'tariffName',
            'sum',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
