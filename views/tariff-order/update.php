<?php

/* @var $this yii\web\View */
/* @var $model app\models\TariffOrder */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Tariffs Order',
    ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tariffs Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tariffs-order-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
