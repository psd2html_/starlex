<?php


/* @var $this yii\web\View */
/* @var $model app\models\TariffOrder */

$this->title = Yii::t('app', 'Create Tariffs Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tariffs Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-order-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
