<?php

use app\models\Tariff;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TariffOrder */
/* @var $form yii\widgets\ActiveForm */

$listOptions = [
    'prompt' => 'Выберите элемент'
]
?>

<div class="tariffs-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')
        ->dropDownList(User::collection(), $listOptions) ?>

    <?= $form->field($model, 'tariff_id')
        ->dropDownList(Tariff::collection(), $listOptions) ?>

    <?= $form->field($model, 'sum')
        ->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create')
            : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
