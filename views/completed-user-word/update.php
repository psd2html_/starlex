<?php

/* @var $this yii\web\View */
/* @var $model app\models\CompletedUserWord */

$this->title = 'Update Completed User Word: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Completed User Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="completed-user-word-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
