<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CompletedUserWordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Completed User Words';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="completed-user-word-index">
    <p>
        <?= Html::a('Create Completed User Word', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'user_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'userName',
                'value'     => function ($model){
                    /** @var $model \app\models\CompletedUserTheme */
                    return $model->user->email;
                },
            ],
            [
                'attribute' => 'lesson_word_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'lessonWordName',
                'value'     => function ($model){
                    /** @var $model \app\models\CompletedUserWord */
                    return $model->lessonWord->word->name;
                },
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
