<?php


/* @var $this yii\web\View */
/* @var $model app\models\CompletedUserWord */

$this->title = 'Create Completed User Word';
$this->params['breadcrumbs'][] = ['label' => 'Completed User Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="completed-user-word-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
