<?php

use app\models\Language;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\LessonMedia */
/* @var $form yii\widgets\ActiveForm */
/* @var $themeId integer */
/* @var $lessonId integer */
/* @var $themesCollection array */
/* @var $lessonsCollection array */
/* @var $lessonWordsCollection array */

$dropdownParams = ['prompt' => 'Пожалуйста, выберите элемент'];

?>

<?php Pjax::begin(); ?>

    <div class="lessons-media-form">
        <div class="row">

            <?php ActiveForm::begin(); ?>

            <div class="col-md-4">
                <div class="form-group">
                    <?= Html::label('Theme', 'themeId'); ?>
                    <?= Html::dropDownList('themeId', $themeId, $themesCollection, $dropdownParams + [
                            'onChange' => "$(this).closest('form').submit();",
                            'class'    => 'form-control',
                            'id'       => 'themeId',
                        ]) ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <?= Html::label('Lesson', 'lessonId'); ?>
                    <?= Html::dropDownList('lessonId', $lessonId, $lessonsCollection, $dropdownParams + [
                            'onChange' => "$(this).closest('form').submit();",
                            'class'    => 'form-control',
                            'id'       => 'lessonId',
                        ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

            <?php $form = ActiveForm::begin(); ?>

            <div class="col-md-4">
                <?= $form->field($model, 'lesson_word_id')
                    ->dropDownList($lessonWordsCollection, $dropdownParams); ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'language_id')
                    ->dropDownList(Language::collection(), $dropdownParams); ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'media')
                    ->fileInput(); ?>
            </div>

            <?php if (!$model->isNewRecord): ?>
                <div class="col-md-4">
                    <?= Html::tag('audio', '', [
                        'src'      => $model->getMediaPath(),
                        'controls' => 'controls',
                    ]); ?>
                </div>
            <?php endif; ?>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                    ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

<?php Pjax::end(); ?>