<?php

use app\models\Language;
use app\models\Lesson;
use app\models\LessonMedia;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\models\search\LessonMediaSearch */

$this->title = Yii::t('app', 'Lessons Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lessons-media-index">


    <p>
        <?= Html::a(Yii::t('app', 'Create Lessons Media'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => 80],
            ],

            [
                'attribute' => 'lessonId',
                'value'     => function ($model){
                    return $model->lessonName;
                },
            ],
            [
                'attribute' => 'lesson_word_id',
                'label'     => Yii::t('app', 'Word'),
                'value'     => function ($model){
                    /** @var $model LessonMedia */
                    return isset($model->lessonWord->word->name) ? $model->lessonWord->word->name : '';
                },
                'filter' => false,
            ],
            [
                'attribute' => 'language_id',
                'filter' => Language::collection(),
                'value' => 'language.name',
            ],
            [
                'attribute' => 'media',
                'format' => 'raw',
                'value' => function ($model){
                    /** @var $model LessonMedia */
                    return Html::tag('audio', '', [
                        'src' => $model->getMediaPath(),
                        'controls' => 'controls'
                    ]);
                }
            ],

            [
                'attribute' => 'created_at',
                'format'    => 'datetime',
                'options'   => ['width' => 170],
            ],
            [
                'attribute' => 'updated_at',
                'format'    => 'datetime',
                'options'   => ['width' => 170],
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
