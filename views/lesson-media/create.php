<?php


/* @var $this yii\web\View */
/* @var $model app\models\LessonMedia */
/* @var $themeId integer */
/* @var $lessonId integer */
/* @var $themesCollection array */
/* @var $lessonsCollection array */
/* @var $lessonWordsCollection array */

$this->title = Yii::t('app', 'Create Lessons Media');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lessons Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lessons-media-create">
    <?= $this->render('_form', [
        'model'                 => $model,
        'themeId'               => $themeId,
        'lessonId'              => $lessonId,
        'themesCollection'      => $themesCollection,
        'lessonsCollection'     => $lessonsCollection,
        'lessonWordsCollection' => $lessonWordsCollection,
    ]) ?>
</div>
