<?php


/* @var $this yii\web\View */
/* @var $model app\models\LessonWordBadTranslate */
/* @var $themeId integer */
/* @var $lessonId integer */
/* @var $themesCollection array */
/* @var $lessonsCollection array */
/* @var $lessonWordsCollection array */

$this->title = 'Create Lesson Word Bad Translate';
$this->params['breadcrumbs'][] = ['label' => 'Lesson Word Bad Translates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-word-bad-translate-create">
    <?= $this->render('_form', [
        'model'                 => $model,
        'lessonId'              => $lessonId,
        'themeId'               => $themeId,
        'themesCollection'      => $themesCollection,
        'lessonsCollection'     => $lessonsCollection,
        'lessonWordsCollection' => $lessonWordsCollection,
    ]) ?>
</div>
