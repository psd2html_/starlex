<?php

/* @var $this yii\web\View */
/* @var $model app\models\LessonWordBadTranslate */
/* @var $themeId integer */
/* @var $lessonId integer */
/* @var $themesCollection array */
/* @var $lessonsCollection array */
/* @var $lessonWordsCollection array */

$this->title = 'Update Lesson Word Bad Translate: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lesson Word Bad Translates', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lesson-word-bad-translate-update">
    <?= $this->render('_form', [
        'model'                 => $model,
        'lessonId'              => $lessonId,
        'themeId'               => $themeId,
        'themesCollection'      => $themesCollection,
        'lessonsCollection'     => $lessonsCollection,
        'lessonWordsCollection' => $lessonWordsCollection,
    ]) ?>
</div>
