<?php

use app\models\LessonWordBadTranslate;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LessonWordBadTranslateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lesson Word Bad Translates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-word-bad-translate-index">
    <p>
        <?= Html::a('Create Lesson Word Bad Translate', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'lesson_word_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'word',
                'value'     => function ($model){
                    /** @var $model LessonWordBadTranslate */
                    return $model->lessonWord->word->name;
                },
            ],
            [
                'attribute' => 'bad_translate_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'bad_translate',
                'value'     => function ($model){
                    /** @var $model LessonWordBadTranslate */
                    return $model->badTranslate->name;
                },
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
