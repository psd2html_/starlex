<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?php foreach ($model->getNames() as $gameName) : ?>
                <?php /** @var $gameName app\models\GameTranslate */ ?>

                <?= $form->field($model, "names[{$gameName->language_id}]")
                    ->textInput(['maxlength' => true, 'value' => $gameName->translate])
                    ->label($gameName->language->name); ?>
            <?php endforeach; ?>
        </div>
<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'banner')
//                ->textInput(['maxlength' => true]) ?>
<!--        </div>-->
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create')
            : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
