<?php

/* @var $this yii\web\View */
/* @var $model app\models\Game */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Game',
    ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Games'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="game-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
