<?php

/* @var $this yii\web\View */
/* @var $model app\models\LessonTranslate */

$this->title = 'Update Lesson Translate: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lesson Translates', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lesson-translate-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
