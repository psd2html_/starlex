<?php


/* @var $this yii\web\View */
/* @var $model app\models\LessonTranslate */

$this->title = 'Create Lesson Translate';
$this->params['breadcrumbs'][] = ['label' => 'Lesson Translates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-translate-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
