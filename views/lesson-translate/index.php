<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LessonTranslateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lesson Translates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-translate-index">
    <p>
        <?= Html::a('Create Lesson Translate', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'lesson_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'lang_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            'translate',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
