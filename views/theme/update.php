<?php

/* @var $this yii\web\View */
/* @var $model app\models\Theme */

$this->title = 'Update Themes: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="themes-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
