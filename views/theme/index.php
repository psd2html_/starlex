<?php

use app\models\Lesson;
use app\models\Theme;
use app\widgets\TranslateMenuWidget;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Темы';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="themes-index">
    <p><?= Html::a('Переводы', ['/theme-translate'], ['class' => 'btn btn-default']) ?></p>
    <p><?= Html::a('Создать тему', ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'id',
            'parent_id',
            [
                'attribute' => 'parent_name',
                'value' => function ($model){
                    /**
                     * @var $model Theme
                     */
                    return @$model->parent->name;
                }
            ],
            [
                'attribute' => 'name',
                'value' => function ($model){
                    /**
                     * @var $model Theme
                     */
                    return $model->themeTranslateByLanguage->translate;
                }
            ],
            [
                'attribute' => 'status',
                'value'     => function ($data){
                    return Lesson::statuses($data->status);
                },
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
