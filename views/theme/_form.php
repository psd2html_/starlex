<?php

use app\models\Theme;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Theme */
/* @var $form yii\widgets\ActiveForm */
$theme_select_view = [];

if (!$model->isNewRecord){
    $select = [];
    $parents = \app\models\Theme::find()
        ->where(['parent_id' => $model->id])
        ->joinWith('lang')
        ->asArray()
        ->all();

    foreach ($parents as $p) {
        /*$select[] = [
            "id"=> $p["id"],
            "translate"=> $p["lang"][0]["translate"],
        ];*/
        $select[$p["id"]] = $p["lang"][0]["translate"];
        if(isset($p["pos_row"]) && isset($p["pos_col"])){
            $theme_select_view[$p["pos_row"]][$p["pos_col"]] = $p["id"];
        }
    }
    //echo "<pre>";
    //var_dump($select);
    //echo "</pre>";
    $theme_select_view_count = count($theme_select_view);

    $params = ['empty' => 'Выберите подтему'];
    $theme_select = Html::dropDownList('theme_position[{row}][]',  null, $select, $params);
    $theme_select = preg_replace( "/\r|\n/", "", $theme_select );

    $script = <<< JS
        var i = $theme_select_view_count;
        var row = '<div class="row" row="{count}"><div class="col-md-auto"><button type="button" class="btn btn-info add_col">+</button> <button type="button" class="btn btn-warning del_row">-</button></div></div>';
        var themes = '<div class="col-md-auto">$theme_select<button type="button" class="btn btn-danger del_col">-</button></div>';

        $( '.struct' ).on( 'click', '.del_col', function () {
             $(this).parent().remove();
        });

        $( '.struct' ).on( 'click', '.del_row', function () {
             $(this).parent().parent().remove();
        });
        
        $( '.struct' ).on( 'click', '.add_col', function () {
             var row = $(this).parent().parent();
             var row_count = row.attr("row");
             themes2 = themes;
             themes2 = themes2.replace('{row}', row_count);
             row.append(themes2);
        });
        
        $('.add_row').bind('click', function() {
             i = i + 1;
             row2 = row;
             row2 = row2.replace('{count}', i);
             $(".struct").append(row2);
        });
        
JS;

    Yii::$app->view->registerJs($script, yii\web\View::POS_READY);
}

?>

<div class="themes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $params = [
        'prompt' => 'Укажите родителя темы если это необходимо',
    ];
    echo $form->field($model, 'parent_id')
        ->dropDownList(Theme::collectionOneLevel(), $params);
    ?>

    <?php
    $params = ['empty' => 'Выберите статус'];
    echo $form->field($model, 'status')
        ->dropDownList(\app\models\Theme::statuses(), $params);
    ?>

    <h3>Переводы:</h3>

    <?php if ($model->isNewRecord): ?>

        <?php $languages = \app\models\Language::find()
            ->asArray()
            ->all(); ?>
        <?php foreach ($languages as $_lang_value) : ?>
            <?= $form->field($model, 'languages[' . $_lang_value['id'] . ']')
                ->textInput(['maxlength' => true, 'value' => ''])
                ->label($_lang_value['name']); ?>
        <?php endforeach; ?>

    <?php else : ?>

        <?php $t = \app\models\ThemeTranslate::find()
            ->where(['theme_id' => $model->id])
            ->joinWith('lang')
            ->asArray()
            ->all(); ?>
        <?php foreach ($t as $_lang_value) : ?>
            <?= $form->field($model, 'languages[' . $_lang_value['language_id'] . ']')
                ->textInput(['maxlength' => true, 'value' => $_lang_value['translate']])
                ->label($_lang_value['lang']['name']); ?>
        <?php endforeach; ?>

        <?php if (!$model->parent_id){ ?>

            <div class="form-group">
                <h3>Структура темы:</h3>
                <div class="container">
                    <div class="row">
                        <button type="button" class="btn btn-primary add_row">Добавить строку</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="container struct">
                    <?php
                    if(count($theme_select_view)>0){
                        foreach ($theme_select_view as $row => $themes_row){
                    ?>
                    <div class="row" row="<?=$row?>">
                        <div class="col-md-auto">
                            <button type="button" class="btn btn-info add_col">+</button>
                            <button type="button" class="btn btn-warning del_row">-</button>
                        </div>
                    <?php
                            foreach ($themes_row as $col => $themes_col){
                                $theme_select2 = Html::dropDownList('theme_position['.$row.'][]', $themes_col, $select, $params);
                    ?>
                         <div class="col-md-auto"><?=$theme_select2?><button type="button" class="btn btn-danger del_col">-</button></div>
                    <?php
                            }
                    ?>
                    </div>
                    <?php
                        }
                    } else {
                    ?>
                    <div class="row" row="1">
                        <div class="col-md-auto">
                            <button type="button" class="btn btn-info add_col">+</button>
                            <button type="button" class="btn btn-warning del_row">-</button>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        <?php } else { ?>
            <?= $form->field($model, 'pos_row')
                ->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

            <?= $form->field($model, 'pos_col')
                ->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
        <?php } ?>
    <?php endif; ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
