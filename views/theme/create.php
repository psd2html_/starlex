<?php


/* @var $this yii\web\View */
/* @var $model app\models\Theme */

$this->title = 'Create Themes';
$this->params['breadcrumbs'][] = ['label' => 'Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="themes-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
