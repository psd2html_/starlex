<?php

/* @var $this yii\web\View */
/* @var $model app\models\Language */

$this->title = 'Update Language: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="language-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
