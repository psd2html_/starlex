<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Languages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-index">


    <p>
        <?= Html::a('Create Language', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'id',
            'name',
            'locale',
            [
                'attribute' => 'is_default',
                'format'    => 'text',
                'value'     => function ($model){
                    if($model->is_default == 1){
                        return "Да";
                    }else{
                        return "";
                    }
                },
            ],
            [
                'attribute' => 'is_learn',
                'format'    => 'text',
                'value'     => function ($model){
                    if($model->is_learn == 1){
                        return "Да";
                    }else{
                        return "";
                    }
                },
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
