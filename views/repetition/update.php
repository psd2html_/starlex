<?php

/* @var $this yii\web\View */
/* @var $model app\models\Repetition */

$this->title = 'Update Repetition: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Repetitions', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="repetition-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
