<?php


/* @var $this yii\web\View */
/* @var $model app\models\Repetition */

$this->title = 'Create Repetition';
$this->params['breadcrumbs'][] = ['label' => 'Repetitions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repetition-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
