<?php

use app\models\LessonWord;
use app\models\Repetition;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Repetition */
/* @var $form yii\widgets\ActiveForm */

$listOptions = [
    'prompt' => 'Выберите элемент',
];
?>

<div class="repetition-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lesson_word_id')
        ->dropDownList(LessonWord::collection(), $listOptions) ?>

    <?= $form->field($model, 'status')
        ->dropDownList(Repetition::statuses()) ?>

    <?= $form->field($model, 'count_success')
        ->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
