<?php

use app\models\Repetition;
use app\models\Word;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RepetitionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Repetitions';
$this->params['breadcrumbs'][] = $this->title;

$wordList = Word::collection();
?>
<div class="repetition-index">
    <p>
        <?= Html::a('Create Repetition', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'lesson_word_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            'wordName',
            [
                'attribute' => 'status',
                'filter'    => Repetition::statuses(),
                'value'     => function ($model){
                    /** @var $model Repetition */
                    return Repetition::statuses()[$model->status];
                },
            ],
            'count_success',
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
