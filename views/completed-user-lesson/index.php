<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CompletedUserLessonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Completed User Lessons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="completed-user-lesson-index">
    <p>
        <?= Html::a('Create Completed User Lesson', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'user_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'userName',
                'value'     => function ($model){
                    /** @var $model \app\models\CompletedUserTheme */
                    return $model->user->email;
                },
            ],
            [
                'attribute' => 'lesson_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'lessonName',
                'value'     => function ($model){
                    /** @var $model \app\models\CompletedUserLesson */
                    return $model->lesson->name;
                },
            ],
            'stars_total',
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
