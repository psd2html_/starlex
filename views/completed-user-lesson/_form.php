<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Language;

/* @var $this yii\web\View */
/* @var $model app\models\CompletedUserLesson */
/* @var $form yii\widgets\ActiveForm */
$dropdownParams = ['prompt' => 'Пожалуйста, выберите элемент'];
?>

<div class="completed-user-lesson-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')
        ->textInput() ?>

    <?= $form->field($model, 'lesson_id')
        ->textInput() ?>

    <?= $form->field($model, 'lang_id')
        ->dropDownList(Language::collection(), $dropdownParams) ?>

    <?= $form->field($model, 'translate_id')
        ->dropDownList(Language::collection(), $dropdownParams) ?>

    <?= $form->field($model, 'stars_total')
        ->textInput() ?>

    <?= $form->field($model, 'points')
        ->textInput() ?>

    <?= $form->field($model, 'flag_help')
        ->textInput() ?>

    <?= $form->field($model, 'flag_time')
        ->textInput() ?>

    <?= $form->field($model, 'answers_count')
        ->textInput() ?>

    <?= null/* $form->field($model, 'created_at')->textInput() */ ?>

    <?= null/* $form->field($model, 'updated_at')->textInput() */ ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
