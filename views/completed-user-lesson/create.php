<?php


/* @var $this yii\web\View */
/* @var $model app\models\CompletedUserLesson */

$this->title = 'Create Completed User Lesson';
$this->params['breadcrumbs'][] = ['label' => 'Completed User Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="completed-user-lesson-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
