<?php

/* @var $this yii\web\View */
/* @var $model app\models\CompletedUserLesson */

$this->title = 'Update Completed User Lesson: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Completed User Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="completed-user-lesson-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
