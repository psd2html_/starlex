<?php

/* @var $this yii\web\View */
/* @var $model app\models\Tariff */

$this->title = 'Update Tariffs: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tariffs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tariffs-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
