<?php


/* @var $this yii\web\View */
/* @var $model app\models\Tariff */

$this->title = 'Create Tariffs';
$this->params['breadcrumbs'][] = ['label' => 'Tariffs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariffs-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
