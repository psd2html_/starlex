<?php


/* @var $this yii\web\View */
/* @var $model app\models\LessonImage */
/* @var $themeId integer */
/* @var $lessonId integer */
/* @var $themesCollection array */
/* @var $lessonsCollection array */
/* @var $lessonWordsCollection array */

$this->title = Yii::t('app', 'Create Lessons Images');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lessons Images'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-image-create">
    <?= $this->render('_form', [
        'model'                 => $model,
        'themesCollection'      => $themesCollection,
        'lessonsCollection'     => $lessonsCollection,
        'themeId'               => $themeId,
        'lessonId'              => $lessonId,
        'lessonWordsCollection' => $lessonWordsCollection,
    ]) ?>
</div>
