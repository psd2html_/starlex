<?php

use app\models\Language;
use app\models\LessonImage;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lessons Images');
$this->params['breadcrumbs'][] = $this->title;

echo newerton\fancybox\FancyBox::widget([
    'target'  => 'a[rel=fancybox]',
    'helpers' => true,
    'mouse'   => true,
    'config'  => [
        'maxWidth'    => '90%',
        'maxHeight'   => '90%',
        'playSpeed'   => 7000,
        'padding'     => 0,
        'fitToView'   => false,
        'width'       => '70%',
        'height'      => '70%',
        'autoSize'    => false,
        'closeClick'  => false,
        'openEffect'  => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect'  => 'elastic',
        'nextEffect'  => 'elastic',
        'closeBtn'    => false,
        'openOpacity' => true,
        'helpers'     => [
            'title'   => ['type' => 'float'],
            'buttons' => [],
            'thumbs'  => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)',
                ],
            ],
        ],
    ],
]);
?>
<div class="lesson-image-index">


    <p>
        <?= Html::a(Yii::t('app', 'Create Lessons Images'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => 80],
            ],

            [
                'attribute' => 'lesson_word_id',
                'label'     => Yii::t('app', 'Word'),
                'value'     => function ($model){
                    /** @var $model LessonImage */
                    return @$model->lessonWord->word->name;
                },
                //'filter' => false,
            ],
            /*[
                'attribute' => 'language_id',
                'filter' => Language::collection(),
                'value' => 'language.name',
            ],*/
            [
                'attribute' => 'image',
                'format'    => 'raw',
                'label'     => Yii::t('app', 'Картинка'),
                'value'     => function ($data){
                    if (!empty($data->image)){
                        $thumbnail = $data->getImageThumbUrl();
                        if (file_exists($thumbnail)){
                            return Html::img('/' . $thumbnail);
                        } else {
                            $mediaUrl = '/' . $data->getBaseImagePath() . '/' . $data->image;
                            return Html::a(Html::img($mediaUrl, ['width' => 80,]), $mediaUrl, [
                                'rel'   => 'fancybox',
                            ]);
                        }
                    }
                },
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'datetime',
                'options'   => ['width' => '170'],
            ],
            [
                'attribute' => 'updated_at',
                'format'    => 'datetime',
                'options'   => ['width' => '170'],
            ],


            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
