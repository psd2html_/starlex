<?php


/* @var $this yii\web\View */
/* @var $model app\models\Word */

$this->title = 'Create Word';
$this->params['breadcrumbs'][] = ['label' => 'Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="word-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
