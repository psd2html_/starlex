<?php

/* @var $this yii\web\View */
/* @var $model app\models\Word */

$this->title = 'Update Word: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="word-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
