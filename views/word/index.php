<?php

use app\models\Language;
use app\models\Word;
use app\models\WordGroup;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\WordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Words';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="word-index">
    <p>
        <?= Html::a('Create Word', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::a('Проставить метки "Словосочетание"', ['', 'collocation-words' => 'true'], ['class' => 'btn btn-info']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'word_group_id',
                'value' => function ($model){
                    /** @var $model Word */
                    if ( isset($model->wordGroup->name) ){
                        return Html::a($model->wordGroup->name, ['word-group/update', 'id' => $model->word_group_id]);
                    }
                    return null;
                },
                'format' => 'html',
                'filter' => WordGroup::collection(),
            ],
            [
                'attribute' => 'lang_id',
                'value' => function ($model ){
                    /** @var $model Word */
                    return $model->langName;
                },
                'filter' => Language::collection()
            ],
            'name',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => function ( $model){
                    /** @var $model Word */
                    return $model::statuses($model->status);
                },
                'filter' => Word::statuses(),
            ],
            [
                'attribute' => 'is_word_collocation',
                'value' => function (Word $model){
                    return @Word::collocationWordStatuses()[$model->is_word_collocation];
                },
                'filter' => Word::collocationWordStatuses(),
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
