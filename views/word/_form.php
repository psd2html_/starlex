<?php

use app\models\Language;
use app\models\Word;
use app\models\WordGroup;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Word */
/* @var $form yii\widgets\ActiveForm */

$dropDownParams = ['prompt' => 'Пожалуйста, выберите элемент'];
?>

<div class="word-form">
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'   => false,
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->field($model, 'lang_id')
        ->dropDownList(Language::collection(), $dropDownParams) ?>

    <?= $form->field($model, 'word_group_id')
        ->dropDownList(WordGroup::collection(), $dropDownParams); ?>

    <?= $form->field($model, 'article')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')
        ->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_word_collocation')
        ->dropDownList(Word::collocationWordStatuses()) ?>

    <?= $form->field($model, 'status')
        ->dropDownList(Word::statuses()) ?>

    <?= null/* $form->field($model, 'created_at')->textInput() */ ?>

    <?= null/* $form->field($model, 'updated_at')->textInput() */ ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
