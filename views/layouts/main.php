<?php
/**
 * @var $this yii\web\View
 * @var $content
 */
?>
<?php $this->beginContent('@app/views/layouts/common.php'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?php echo $content ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>