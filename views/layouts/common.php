<?php

use app\assets\AppAsset;
use app\helpers\FlashHelper;
use app\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\widgets\Breadcrumbs;

/**
 * @var $this yii\web\View
 */

$bundle = AppAsset::register($this);
?>
<?php $this->beginContent('@app/views/layouts/base.php'); ?>
<div class="wrapper">
    <!-- header logo: style can be found in header.less -->
    <header class="main-header">
        <a href="/" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <?= Yii::$app->name ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/img/anonymous.jpg" class="user-image">
                            <span><?= Yii::$app->user->identity->email ?> <i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header light-blue">
                                <img src="/img/anonymous.jpg" class="img-circle" alt="User Image" />
                                <p>
                                    <?= Yii::$app->user->identity->email ?>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?= Html::a('Profile', ['/user/profile'], ['class' => 'btn btn-default btn-flat']) ?>
                                </div>
                                <div class="pull-right">
                                    <?= Html::a('Logout', ['/user/sign-in/logout'], [
                                        'class'       => 'btn btn-default btn-flat',
                                        'data-method' => 'post',
                                    ]) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/anonymous.jpg" class="img-circle" />
                </div>
                <div class="pull-left info">
                    <p><?= 'Hello, '.Yii::$app->user->identity->username ?></p>
                    <a href="<?= Url::to(['/user/profile']) ?>">
                        <i class="fa fa-circle text-success"></i>
                        <?= Yii::$app->formatter->asDatetime(time()) ?>
                    </a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?= Menu::widget([
                'options'         => ['class' => 'sidebar-menu'],
//				'linkTemplate'    => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
//				'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                'activateParents' => true,
                'items'           => [
                    [
                        'label'   => 'Main',
                        'options' => ['class' => 'header'],
                    ],
                    [
                        'label' => 'Главная',
                        'url'   => ['/site/index'],
                        'icon'  => 'fa fa-edit',
                    ],
                    [
                        'label' => 'Темы',
                        'icon'  => 'fa fa-edit',
                        'url'   => '#',
                        'items' => [
                            [
                                'label' => 'Список',
                                'url'   => ['/theme'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Переводы названий',
                                'url'   => ['/theme-translate'],
                                'icon'  => 'fa fa-edit',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Уроки',
                        'icon'  => 'fa fa-edit',
                        'url'   => '#',
                        'items' => [
                            [
                                'label' => 'Список',
                                'url'   => ['/lesson'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Список переводов названий',
                                'url'   => ['/lesson-translate'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Слова уроков',
                                'url'   => ['/lesson-word'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Неверные переводы слов',
                                'url'   => ['/lesson-word-bad-translate'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Картинки',
                                'url'   => ['/lesson-image'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Медиа',
                                'url'   => ['/lesson-media'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Слова уроков итог',
                                'url'   => ['/lesson-word-finish'],
                                'icon'  => 'fa fa-edit',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Слова',
                        'url'   => '#',
                        'icon'  => 'fa fa-edit',
                        'items' => [
                            [
                                'label' => 'Группы слов',
                                'url'   => ['/word-group'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Список',
                                'url'   => ['/word'],
                                'icon'  => 'fa fa-edit',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Игры',
                        'icon'  => 'fa fa-edit',
                        'url'   => '#',
                        'items' => [
                            [
                                'label' => 'Список',
                                'url'   => ['/game'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Переводы названий',
                                'url'   => ['/game-translate'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Сохранения пользователей',
                                'url'   => ['/game-user-data'],
                                'icon'  => 'fa fa-edit',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Достижения',
                        'url'   => '#',
                        'icon'  => 'fa fa-edit',
                        'items' => [
                            [
                                'label' => 'Награды',
                                'url'   => ['/award'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Сертификаты',
                                'url'   => ['/certificate'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Награды пользователей',
                                'url'   => ['/user-award'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Сертификаты пользователей',
                                'url'   => ['/user-certificate'],
                                'icon'  => 'fa fa-edit',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Завершённые',
                        'url'   => '#',
                        'icon'  => 'fa fa-edit',
                        'items' => [
                            [
                                'label' => 'Темы',
                                'url'   => ['/completed-user-theme'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Уроки',
                                'url'   => ['/completed-user-lesson'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Слова',
                                'url'   => ['/completed-user-word'],
                                'icon'  => 'fa fa-edit',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Повторения',
                        'url'   => ['/repetition'],
                        'icon'  => 'fa fa-edit',
                    ],
                    [
                        'label' => 'Система',
                        'icon'  => 'fa fa-edit',
                        'url'   => '#',
                        'items' => [
                            [
                                'label' => 'Админ',
                                'url'   => ['/admin'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'API',
                                'url'   => ['/api'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Пользователи',
                                'icon'  => 'fa fa-edit',
                                'url'   => ['/users'],
                            ],
                            [
                                'label' => 'Тарифы',
                                'url'   => ['/tariff'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Заказы',
                                'url'   => ['/tariff-order'],
                                'icon'  => 'fa fa-edit',
                            ],
                            [
                                'label' => 'Языки',
                                'url'   => ['/language'],
                                'icon'  => 'fa fa-edit',
                            ],
                        ],
                    ],
                ],
            ]) ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $this->title ?>
                <?php if (isset($this->params['subtitle'])): ?>
                    <small><?= $this->params['subtitle'] ?></small>
                <?php endif; ?>
            </h1>

            <?= Breadcrumbs::widget([
                'tag'   => 'ol',
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if (Yii::$app->session->hasFlash('alert')): ?>
                <div class="row">
                    <div class="col-md-4">
                        <?= \yii\bootstrap\Alert::widget([
                            'body'    => ArrayHelper::getValue(FlashHelper::getAlert(), 'body'),
                            'options' => ArrayHelper::getValue(FlashHelper::getAlert(), 'options'),
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>
            <?= $content ?>
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<?php $this->endContent(); ?>
