<?php

/* @var $this yii\web\View */
/* @var $model app\models\GameUserData */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Game User Data',
    ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game User Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="game-user-data-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
