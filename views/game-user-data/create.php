<?php


/* @var $this yii\web\View */
/* @var $model app\models\GameUserData */

$this->title = Yii::t('app', 'Create Game User Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game User Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-user-data-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
