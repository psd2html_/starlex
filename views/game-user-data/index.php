<?php

use app\models\GameUserData;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GameUserDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Game User Data');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-user-data-index">


    <p>
        <?= Html::a(Yii::t('app', 'Create Game User Data'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'game_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'game_name',
                'content'   => function ($model){
                    /**
                     * @var $model GameUserData
                     */
                    return $model->game->name;
                },
            ],
            [
                'attribute' => 'user_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'user_name',
                'content'   => function ($model){
                    /**
                     * @var $model GameUserData
                     */
                    return $model->user->username;
                },
            ],
//            'data:ntext',
            [
                'attribute' => 'rating',
                'options'   => [
                    'width' => 80,
                ],
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
