<?php

use app\models\Game;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GameUserData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-user-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'game_id')
        ->dropDownList(Game::collection()) ?>

    <?= $form->field($model, 'user_id')
        ->dropDownList(User::collection()) ?>

    <?= $form->field($model, 'data')
        ->widget(\trntv\aceeditor\AceEditor::class, [
            'mode'  => 'json', // programing language mode. Default "html"
            'theme' => 'github' // editor theme. Default "github"
        ]) ?>

    <?= $form->field($model, 'rating')
        ->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create')
            : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
