<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <p>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'id',
            'username',
            'firstname',
            'lastname',
            'email:email',
            // 'password_hash',

            [
                'attribute' => 'status',
                'label'     => Yii::t('app', 'Статус'),
                'format'    => 'text',
                'value'     => function ($data){
                    return app\modules\user\models\User::itemAlias("Status", $data->status);
                },
            ],
            'auth_key',
            // 'email_confirm_token:email',
            // 'password_reset_token',

            [
                'attribute' => 'created_at',
                'label'     => 'Создано',
                'format'    => 'text',
                'value'     => function ($model){
                    return date('d.m.Y', $model->created_at);
                },
            ],
            [
                'attribute' => 'updated_at',
                'label'     => 'Обновлено',
                'format'    => 'text',
                'value'     => function ($model){
                    return date('d.m.Y', $model->updated_at);
                },
            ],


            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {b_Settings}',  // the default buttons + your custom button
                'buttons'  => [
                    'b_Settings' => function ($url, $model){     // render your custom button
                        $userSettings = $model->userSettings;
                        if ($model->userSettings){
                            return Html::a('<span class="glyphicon glyphicon-cog">Настройки</span>', [
                                '/user/settings/view',
                                'id' => $userSettings[0]->id,
                            ]);
                        }
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
