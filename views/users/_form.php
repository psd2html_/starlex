<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')
        ->textInput(['maxlength' => true]) ?>

    <?php
    $items = $model::itemAlias('Status');

    $params = ['empty' => 'Выберите Статус'];
    echo $form->field($model, 'status')
        ->dropDownList($items, $params);
    ?>

    <?= $form->field($model, 'f_password')
        ->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
