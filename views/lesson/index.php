<?php

use app\models\Lesson;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Уроки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lessons-index">

    <h3>Перевод: <?php echo $lang_name; ?></h3>

    <div class="row">
        <div class="col-md-3">
            <?= Html::a('Create Lessons', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'id',
            [
                'attribute' => 'theme_id',
                'label'     => Yii::t('app', 'Theme'),
                'format'    => 'text',
                'value'     => function ($model){
                    /* @var $model Lesson */
                    return $model->theme->name;
                },
            ],
            [
                'attribute' => 'name',
                'value' => function ($model){
                    /* @var $model Lesson */
                    return $model->name;
                }
            ],
            [
                'attribute' => 'status',
                'label'     => Yii::t('app', 'Status'),
                'format'    => 'text',
                'value'     => function ($model){
                    /* @var $model Lesson */
                    return @\app\models\Theme::statuses($model->status, true);
                },
            ],
            [
                'attribute' => 'wordsCount',
                'value' => function ($model){
                    /* @var $model Lesson */
                    return count($model->lessonWords);
                }
            ],
            'time_passage',
            'lives_count',
            'points_count',
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
