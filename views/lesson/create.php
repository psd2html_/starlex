<?php


/* @var $this yii\web\View */
/* @var $model app\models\Lesson */
/* @var $languages array */

$this->title = 'Create Lessons';
$this->params['breadcrumbs'][] = ['label' => 'Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lessons-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
