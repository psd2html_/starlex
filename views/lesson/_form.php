<?php

use app\models\Language;
use app\models\LessonTranslate;
use app\models\Theme;
use app\widgets\LessonWordWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lesson */
/* @var $form yii\widgets\ActiveForm */
/* @var $importFormModel app\models\forms\LessonsImport */

?>

<div class="lessons-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <h3>Параметры лекции:</h3>
            <div class="row">
                <div class="col-md-4">
                    <?php
                    echo $form->field($model, 'theme_id')
                        ->dropDownList(Theme::collectionParent(), ['prompt' => 'Укажите тему']);
                    ?>
                </div>

                <div class="col-md-4">
                    <?php

                    echo $form->field($model, 'status')
                        ->dropDownList(Theme::statuses(false, true));
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'time_passage')
                        ->textInput() ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'lives_count')
                        ->textInput() ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'points_count')
                        ->textInput() ?>
                </div>
            </div>
            <!--            <div class="row">-->
            <!--                <div class="col-md-6">-->
            <!--                    --><? //= $form->field($model, 'archiveFile')
            //                        ->fileInput(); ?>
            <!--                </div>-->
            <!--            </div>-->
        </div>
        <div class="col-md-3 col-md-offset-1">
            <h3>Переводы названия:</h3>

            <?php foreach ($model->getNames() as $lessonName) : ?>
                <?php /** @var $lessonName LessonTranslate */ ?>
                <?= $form->field($model, "names[{$lessonName->lang_id}]")
                    ->textInput(['maxlength' => true, 'value' => $lessonName->translate])
                    ->label($lessonName->language->name); ?>
            <?php endforeach; ?>

        </div>
    </div>

    <div id="words-list" class="row">
        <div class="col-md-12">
            <h3>Слова:</h3>
            <p>
                <?= Html::button('Добавить слово', [
                    'class' => 'btn btn-info',
                    'id'    => 'create-new-word',
                ]); ?>
                &nbsp;
                Количество слов на странице:&nbsp;&nbsp;<strong><span id="count-words"></span></strong>
            </p>
        </div>
        <?= LessonWordWidget::widget([
            'model' => $model,
            'form'  => $form,
        ]); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?= LessonWordWidget::widget([
        'isEmpty' => true,
        'model'   => $model,
        'form'    => $form,
    ]); ?>
</div>

