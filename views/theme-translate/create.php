<?php


/* @var $this yii\web\View */
/* @var $model app\models\ThemeTranslate */

$this->title = 'Create Themes Language';
$this->params['breadcrumbs'][] = ['label' => 'Themes Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="themes-language-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
