<?php

use app\models\Language;
use app\models\Theme;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ThemeTranslate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="themes-language-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'language_id')
        ->dropDownList(Language::collection()) ?>

    <?= $form->field($model, 'theme_id')
        ->dropDownList(Theme::collection()) ?>

    <?= $form->field($model, 'translate')
        ->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
