<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Переводы тем';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="themes-language-index">
    <p><?= Html::a('Назад к темам', ['/theme'], ['class' => 'btn btn-default']) ?></p>
    <p><?= Html::a('Создать перевод темы', ['create'], ['class' => 'btn btn-success']) ?></p>
    
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'language_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'theme_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            'translate',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'options'  => [
                    'width' => 80,
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
