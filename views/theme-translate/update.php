<?php

/* @var $this yii\web\View */
/* @var $model app\models\ThemeTranslate */

$this->title = 'Update Themes Language: ' . $model->translate;
$this->params['breadcrumbs'][] = ['label' => 'Themes Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->translate, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="themes-language-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
