<?php

/* @var $this yii\web\View */
/* @var $model app\models\WordGroup */

$this->title = 'Update Word Group: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Word Group', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="word-translate-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
