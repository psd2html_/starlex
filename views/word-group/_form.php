<?php

/* @var $this yii\web\View */

use app\models\Language;
use app\models\Word;
use app\models\WordGroup;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model app\models\WordGroup */
/* @var $form yii\widgets\ActiveForm */

$selectOptions = [
    'prompt' => 'Please select...',
];
$languageId = Yii::$app->request->get('languageId');

if ($model->isNewRecord){
    $jsOnChangeLang = <<<JS
$(this).closest('form').submit()
JS;

} else {
    $jsOnChangeLang = <<<JS
document.location.href += '&languageId=' + $(this, 'option:selected').val();
JS;

}

?>

<div class="word-translate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')
        ->textInput() ?>
    <?= $form->field($model, 'description')
        ->textarea() ?>

    <?= $form->field($model, 'status')
        ->dropDownList(WordGroup::statuses()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
