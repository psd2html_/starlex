<?php

use app\models\WordGroup;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\WordGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Word Group';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="word-translate-index">
    <p>
        <?= Html::a('Create Word Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::a('Filter by duplicate', ['/word-group', 'find-duplicate' => 'true'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Reset filter', ['/word-group'], ['class' => 'btn btn-warning']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'name',
                'value' => function ($model){
                    /** @var $model \app\models\Word */
                    return Html::a($model->name, ['word/index', 'WordSearch[word_group_id]' => $model->id]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'description',
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model){
                    /** @var $model \app\models\WordGroup */
                    return $model::statuses($model->status);
                },
                'options'   => [
                    'width' => 120,
                ],
                'filter' => WordGroup::statuses()
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'options'   => [
                    'width' => 80,
                ],
            ],
        ],
    ]); ?>
</div>
