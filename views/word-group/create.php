<?php


/* @var $this yii\web\View */
/* @var $model app\models\WordGroup */

$this->title = 'Create Word Group';
$this->params['breadcrumbs'][] = ['label' => 'Word Group', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="word-translate-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
