<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\LessonWord */
/* @var $form yii\widgets\ActiveForm */
/* @var $themeId integer */
/* @var $lessonId integer */
/* @var $themesCollection array */
/* @var $lessonsCollection array */
/* @var $wordsCollection array */

$dropdownParams = ['prompt' => 'Пожалуйста, выберите элемент'];

if (!$model->isNewRecord){
    $theme_id = $model->lesson->theme_id;
}
?>
<?php Pjax::begin(); ?>

    <div class="lesson-word-form">
        <div class="row">
            <?php ActiveForm::begin(); ?>

            <div class="col-md-4">
                <div class="form-group">
                    <?= Html::label('Theme', 'themeId'); ?>
                    <?= Html::dropDownList('themeId', $themeId, $themesCollection, $dropdownParams + [
                            'onChange' => "$(this).closest('form').submit();",
                            'class'    => 'form-control',
                            'id'       => 'themeId',
                        ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

            <?php $form = ActiveForm::begin(); ?>
            <?php if ($themeId || !($model->isNewRecord)): ?>
                <div class="col-md-4">
                    <?= $form->field($model, 'lesson_id')
                        ->dropDownList($lessonsCollection, $dropdownParams) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'word_group_id')
                        ->dropDownList($wordsCollection, $dropdownParams) ?>
                </div>
            <?php endif; ?>
            <div class="col-md-4">
                <?= $form->field($model, 'sync_id')
                    ->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                    ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

<?php Pjax::end(); ?>