<?php

/* @var $this yii\web\View */
/* @var $model app\models\LessonWord */
/* @var $themeId integer */
/* @var $lessonId integer */
/* @var $themesCollection array */
/* @var $lessonsCollection array */
/* @var $wordsCollection array */

$this->title = 'Create Lesson Word';
$this->params['breadcrumbs'][] = ['label' => 'Lesson Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-word-create">
    <?= $this->render('_form', [
        'model'             => $model,
        'themesCollection'  => $themesCollection,
        'lessonsCollection' => $lessonsCollection,
        'themeId'           => $themeId,
        'wordsCollection'   => $wordsCollection,
    ]) ?>
</div>
