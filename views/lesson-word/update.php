<?php

/* @var $this yii\web\View */
/* @var $model app\models\LessonWord */
/* @var $themeId integer */
/* @var $lessonId integer */
/* @var $themesCollection array */
/* @var $lessonsCollection array */
/* @var $wordsCollection array */

$this->title = 'Update Lesson Word: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lesson Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lesson-word-update">
    <?= $this->render('_form', [
        'model'             => $model,
        'themesCollection'  => $themesCollection,
        'lessonsCollection' => $lessonsCollection,
        'themeId'           => $themeId,
        'wordsCollection'   => $wordsCollection,
    ]) ?>
</div>
