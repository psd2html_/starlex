<?php

use app\models\Lesson;
use app\models\Theme;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LessonWordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lesson Words';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lesson-word-index">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'theme_id',
                'value'     => 'theme.name',
                'filter'    => Theme::collection(),
            ],
            [
                'attribute' => 'lesson_id',
                'options'   => [
                    'width' => 80,
                ],
                'value'     => 'lesson.name',
                'filter'    => Lesson::collection(),
            ],
            [
                'attribute' => 'word_group_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'word_group_name',
                'value'     => 'wordGroup.name',
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'datetime',
                'filter' => false,
            ],
            [
                'attribute' => 'updated_at',
                'format'    => 'datetime',
                'filter' => false,
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
