<?php


/* @var $this yii\web\View */
/* @var $model app\models\GameTranslate */

$this->title = Yii::t('app', 'Create Game Translate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game Translates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-translate-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
