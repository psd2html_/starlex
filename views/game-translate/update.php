<?php

/* @var $this yii\web\View */
/* @var $model app\models\GameTranslate */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Game Translate',
    ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Game Translates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="game-translate-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
