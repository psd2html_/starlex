<?php

use app\models\Game;
use app\models\Language;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GameTranslate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-translate-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'game_id')
                ->dropDownList(Game::collection()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'language_id')
                ->dropDownList(Language::collection()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'translate')
                ->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create')
            : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
