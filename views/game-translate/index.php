<?php

use app\models\GameTranslate;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GameTranslateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Game Translates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-translate-index">


    <p>
        <?= Html::a(Yii::t('app', 'Create Game Translate'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'game_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'language_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'language_name',
                'content'   => function ($model){
                    /**
                     * @var $model GameTranslate
                     */
                    return $model->language->name;
                },
            ],
            'translate',
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
