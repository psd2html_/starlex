<?php

use app\models\Award;
use app\models\UserAward;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserAward */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-award-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')
        ->dropDownList(User::collection()) ?>

    <?= $form->field($model, 'award_id')
        ->dropDownList(Award::collection()) ?>

    <?= $form->field($model, 'status')
        ->dropDownList(UserAward::getStatuses()) ?>

    <?= null/* $form->field($model, 'created_at')->textInput() */ ?>

    <?= null/* $form->field($model, 'updated_at')->textInput() */ ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
