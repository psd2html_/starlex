<?php


/* @var $this yii\web\View */
/* @var $model app\models\UserAward */

$this->title = 'Create User Award';
$this->params['breadcrumbs'][] = ['label' => 'User Awards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-award-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
