<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserAwardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Awards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-award-index">
    <p>
        <?= Html::a('Create User Award', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'user_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'award_id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model){
                    return $model->statusName;
                },
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
