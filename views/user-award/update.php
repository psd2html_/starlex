<?php

/* @var $this yii\web\View */
/* @var $model app\models\UserAward */

$this->title = 'Update User Award: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Awards', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-award-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
