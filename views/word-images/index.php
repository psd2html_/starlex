<?php

use app\models\Language;
use app\models\Word;
use app\models\WordGroup;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\WordImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слова без картинок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="word-index">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'name',
                'value' => function ($model){
                    /** @var $model \app\models\WordGroup */
                    return Html::a($model->name, ['word/index', 'WordSearch[word_group_id]' => $model->id]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'description',
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model){
                    /** @var $model \app\models\WordGroup */
                    return $model::statuses($model->status);
                },
                'options'   => [
                    'width' => 120,
                ],
                'filter' => WordGroup::statuses()
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
