<?php

use app\models\LessonWord;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LessonWordFinishSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lesson Words');
$this->params['breadcrumbs'][] = $this->title;

echo newerton\fancybox\FancyBox::widget([
    'target'  => 'a[rel=fancybox]',
    'helpers' => true,
    'mouse'   => true,
    'config'  => [
        'maxWidth'    => '90%',
        'maxHeight'   => '90%',
        'playSpeed'   => 7000,
        'padding'     => 0,
        'fitToView'   => false,
        'width'       => '70%',
        'height'      => '70%',
        'autoSize'    => false,
        'closeClick'  => false,
        'openEffect'  => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect'  => 'elastic',
        'nextEffect'  => 'elastic',
        'closeBtn'    => false,
        'openOpacity' => true,
        'helpers'     => [
            'title'   => ['type' => 'float'],
            'buttons' => [],
            'thumbs'  => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)',
                ],
            ],
        ],
    ],
]);
?>
<div class="lesson-word-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create Lesson Word'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => [
                    'width' => 80,
                ],
            ],
            [
                'attribute' => 'lesson_id',
                'options'   => [
                    'width' => 80,
                ],
                'value'   => function (LessonWord $model){
                    $name = $model->lesson->name;

                    return Html::a($name, ['lesson/update', 'id' => $model->lesson->id]);
                },
                'format'  => 'raw',
            ],
            [
                'label'   => 'Слово',
                'options' => [
                    'width' => 80,
                ],
                'value'   => function (LessonWord $model){
                    $name = $model->wordGroup->name;

                    return Html::a($name, ['word-group/update', 'id' => $model->wordGroup->id]);
                },
                'format'  => 'raw',
            ],
            [
                'label'   => 'Слово (описание)',
                'options' => [
                    'width' => 80,
                ],
                'value'   => function (LessonWord $model){
                    return $model->wordGroup->description;
                },
                'format'  => 'raw',
            ],
            [
                'label'   => 'Перевод',
                'options' => [
                    'width' => 80,
                ],
                'value'   => function (LessonWord $model){
                    $translate = $model->wordGroup->currentTranslate;
                    $name = $translate->name;

                    return Html::a($name, ['word/update', 'id' => $translate->id]);
                },
                'format'  => 'raw',
            ],
            [
                'label'   => 'Перевод (описание)',
                'options' => [
                    'width' => 80,
                ],
                'value'   => function (LessonWord $model){
                    return $model->wordGroup->description;
                },
                'format'  => 'raw',
            ],
            [
                'label'   => 'Аудио',
                'value'  => function (LessonWord $model){
                    $result = [];
                    foreach ($model->lessonMedia as $media){
                        $result[] = Html::tag('audio', '', [
                            'src'      => $media->getMediaPath(),
                            'controls' => 'controls',
                        ]);
                    }

                    $result = implode("<br>", $result);

                    return $result;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'image',
                'format'    => 'raw',
                'label'     => Yii::t('app', 'Картинка'),
                'value'     => function (LessonWord $model){
                    $result = [];
                    foreach ($model->lessonImages as $lessonImage){
                        if (!empty($lessonImage->image)){
                            $thumbnail = $lessonImage->getImageThumbUrl();
                            if (file_exists($thumbnail)){
                                $result[] = Html::img('/' . $thumbnail);
                            } else {
                                $mediaUrl = '/' . $lessonImage->getBaseImagePath() . '/' . $lessonImage->image;
                                $result[] = Html::a(Html::img($mediaUrl, ['width' => 80,]), $mediaUrl, [
                                    'rel' => 'fancybox',
                                ]);
                            }
                        }
                    }

                    $result = implode("<br>", $result);

                    return $result;
                },
            ],

            'created_at:datetime',
            'updated_at:datetime',
            'sync_id',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
