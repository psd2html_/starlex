<?php

/* @var $this yii\web\View */
/* @var $model app\models\CompletedUserTheme */

$this->title = 'Update Completed User Theme: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Completed User Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="completed-user-theme-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
