<?php


/* @var $this yii\web\View */
/* @var $model app\models\CompletedUserTheme */

$this->title = 'Create Completed User Theme';
$this->params['breadcrumbs'][] = ['label' => 'Completed User Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="completed-user-theme-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
