<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompletedUserTheme */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="completed-user-theme-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')
        ->textInput() ?>

    <?= $form->field($model, 'theme_id')
        ->textInput() ?>

    <?= null/* $form->field($model, 'created_at')->textInput() */ ?>

    <?= null/* $form->field($model, 'updated_at')->textInput() */ ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
